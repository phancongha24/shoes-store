cleanup:
	docker-compose up -d ogas-database
	docker exec -i $(shell docker-compose ps -q ogas-database ) psql -Upostgres -c "DROP DATABASE \"shoes-storedb\""
up:
	docker-compose up -d
stop:
	docker-compose stop
rebuild:
	docker-compose up -d ogas-database
	docker exec -i $(shell docker-compose ps -q ogas-database ) psql -Upostgres < ./version/sagoshoesdb_final.sql
backup:
	make stop
	docker exec -it $(shell docker-compose ps -q ogas-database ) pg_dumpall -Upostgres > ./version/sagoshoesdb_final.sql
remove:
	docker-compose down
