package com.sago.shoes.Security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTokenProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTokenProvider.class);

    @Value(value = "${app.jwtSecret}")
    private String jwtSecret;

    @Value(value = "${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    public String createUserDetailToken(Authentication authentication) {
        UserDetail userPrincipal = (UserDetail) authentication.getPrincipal();
        Date expiredDate = new Date(new Date().getTime() + jwtExpirationInMs);
        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiredDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    public String createStaffDetailToken(Authentication auth){
        StaffDetail staffPrincipal = (StaffDetail) auth.getPrincipal();
        Date expiredDate = new Date(new Date().getTime() + jwtExpirationInMs);
        return Jwts.builder()
                .setSubject(Long.toString(staffPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiredDate)
                .setHeaderParam("isAdmin",true)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    public Jws<Claims> parseClaimsJws(String Token){
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(Token);
    }
    public Long getIDByJWT(String Token) {
        Claims claims = parseClaimsJws(Token).getBody();
        return Long.valueOf(claims.getSubject());
    }

    public boolean isAdmin(String Token){
        return  parseClaimsJws(Token).getHeader().get("isAdmin") != null;
    }
    public boolean validateToken(String Token) {
        try {
            parseClaimsJws(Token);
            return true;
        } catch (SignatureException ex) {
            LOGGER.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            LOGGER.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("JWT claims string is empty");
        }
        return false;
    }
}
