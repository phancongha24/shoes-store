package com.sago.shoes.Security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sago.shoes.Models.Staff;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
public class StaffDetail  implements UserDetails {
    private Long id;
    private String fullname;
    private String username;
    @JsonIgnore
    private String password;
    private Long type;
    private Collection<? extends GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public static StaffDetail create(Staff Staff){
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority(Staff.getRole().getId().toString()));
        return new StaffDetail(Staff.getId(), Staff.getFullName(),Staff.getUsername(),Staff.getPassword(),0L,list);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
