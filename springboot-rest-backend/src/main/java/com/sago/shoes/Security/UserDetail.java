package com.sago.shoes.Security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sago.shoes.Models.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Data
@AllArgsConstructor
public class UserDetail implements UserDetails {
    private Long id;
    private String fullname;
    private String email;
    @JsonIgnore
    private String password;
    private Long type;
    private Collection<? extends GrantedAuthority> authorities;


    public static UserDetail create(User User){
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("CUSTOMER"));
        return new UserDetail(User.getId(), User.getFullName(), User.getEmail(), User.getPassword(),1L,list);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getUsername() {
        return email;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
