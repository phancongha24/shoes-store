package com.sago.shoes.Services.Impl;

import com.sago.shoes.Models.User;
import com.sago.shoes.Repositories.UserRepository;
import com.sago.shoes.Security.JWTokenProvider;
import com.sago.shoes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl  implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JWTokenProvider jwTokenProvider;
    public String generateRecoveryPassword(int length)
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
    @Override
    public void save(User user) {
        userRepository.save(user);
    }



    @Override
    public User findByEmail(String email){
        User user = userRepository.findByEmail(email);
        return user;
    }
    @Override
    public boolean existsByEmail(String Email){
        return userRepository.existsByEmail(Email);
    }

    @Override
    public void updateProfile(Long id, String phoneNumber) {
        User user = findById(id);
        user.setPhoneNumber(phoneNumber);
        userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new UsernameNotFoundException(String.format("%s not found",id)));
        return user;
    }

    @Override
    public User JWTokenToUser(HttpServletRequest httpServletRequest) {
        String Token = httpServletRequest.getHeader("Authorization").substring(7);
        return userRepository.findById(jwTokenProvider.getIDByJWT(Token)).orElseThrow(()-> new UsernameNotFoundException("not found"));
    }

    public List<User> getAllUser()
    {
        return userRepository.findAll();
    }



}
