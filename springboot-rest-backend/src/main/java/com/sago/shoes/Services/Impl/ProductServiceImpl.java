package com.sago.shoes.Services.Impl;

import com.sago.shoes.Configs.FileConfig;
import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.*;
import com.sago.shoes.Payload.ProductDTO.*;
import com.sago.shoes.Repositories.ProductImageRepository;
import com.sago.shoes.Repositories.ProductRepository;
import com.sago.shoes.Repositories.ProductVariantRepository;
import com.sago.shoes.Repositories.ReviewRepository;
import com.sago.shoes.Services.ColorService;
import com.sago.shoes.Services.OrderService;
import com.sago.shoes.Services.ProductService;
import com.sago.shoes.Services.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductVariantRepository productVariantRepository;
    @Autowired
    private FileServiceImpl fileServiceImpl;
    @Autowired
    private BrandServiceImpl brandService;
    @Autowired
    private ColorService colorService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private SizeService sizeService;
    @Autowired
    private ProductImageRepository productImageRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    @Override
    @Transactional(rollbackOn = Exception.class)
    public Product updateProduct(Long id,ProductRequest productRequest, MultipartFile[] images) {
        Product product = this.convertToProduct(productRequest);
        List<ProductImage> productImages = new ArrayList<>();
        product.setId(id);
        for(MultipartFile image: images){
            ProductImage productImage = new ProductImage();
            productImage.setImage(fileServiceImpl.storeFile(image));
            productImage.setProduct(product);
            productImages.add(productImage);
        }
        for(ProductImage imageUrl: product.getProductImages()){
            System.out.println(fileServiceImpl.fileStorageLocation.resolve(imageUrl.getImage() + " image file"));
            System.out.println(new File(fileServiceImpl.fileStorageLocation.resolve(imageUrl.getImage()).toString()).delete());
        }
        product.setProductImages(productImages);
        productImageRepository.deleteAllByProduct(product);
        productImageRepository.saveAll(productImages);
        return this.save(product);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Product createProduct(ProductRequest productRequest, MultipartFile[] images) {
        Product product = this.convertToProduct(productRequest);
        List<ProductImage> productImages = new ArrayList<>();
        for(MultipartFile image: images){
            ProductImage productImage = new ProductImage();
            productImage.setImage(fileServiceImpl.storeFile(image));
            productImage.setProduct(product);
            productImages.add(productImage);
        }
        product.setProductImages(productImages);
        product = this.save(product);
        productImageRepository.saveAll(productImages);
        return product;
    }

    public List<ProductResponse> getAllProducts(Pageable pageable, List<Specification<Product>> conditions) {
        Specification result = null;
        if(conditions.size() !=0) {
            for (Specification<Product> condition : conditions) {
                result = Specification.where(result).and(condition);
            }
            Set<Product> products =  productRepository.findAll(result,pageable).toSet();
            return products.stream().map(this::convertToProductResponse).toList();
        }
            return productRepository.findAll(pageable).stream().map(this::convertToProductResponse).toList();
    }

    public Object test(Pageable pageable, Specification conditions){
        return productRepository.findAll(conditions,pageable).getContent();
    }

    @Override
    public ProductDetailResponse getById(Long id) {

        return productRepository.findById(id).map(this::convertToProductDetailResponse).orElseThrow(() ->new ResourceNotFoundException("Product"));
    }

    @Override
    public ProductDetailResponse getBySlug(String slug) {
        return productRepository.findBySlug(slug).map(this::convertToProductDetailResponse).orElseThrow(() ->new ResourceNotFoundException("Product"));
    }


    @Override
    public void updateQuantity(ProductVariant productVariant, Integer quantity) {

            productVariant.setQuantity(quantity);
            productVariantRepository.save(productVariant);
    }

    @Override
    public ProductVariant findProductVariant(Long id) {
        return productVariantRepository.getById(id);
    }

    @Override
    public ProductVariant findProductVariant(String productName) {
        // 0 -> slug
        // 1 -> Color Name
        // 2 -> Size Name
        String[] productComp = productName.split(" ");
        Product product = productRepository.findBySlug(productComp[0]).get();
        Color color = colorService.getByName(productComp[1]);
        Size size = sizeService.getByName(productComp[2]);
        ProductVariant productVariant = productVariantRepository.findBySizeAndColor(product.getId(), size.getId(), color.getId());
        if(productVariant == null){
            productVariant = new ProductVariant();
            productVariant.setProduct(product);
            productVariant.setColor(color);
            productVariant.setSize(size);
            productVariant.setQuantity(0);
            return productVariantRepository.save(productVariant);
        }
        return productVariant;
    }

    @Override
    public String getFullProductName(ProductVariant productVariant) {
        return productVariant.getProduct().getName() + " " + productVariant.getColor().getName() + " " + productVariant.getSize().getName();
    }

    @Override
    public Review createReviewBySlug(String slug, ReviewRequest reviewRequest) {
        Review review = this.convertToReview(reviewRequest);
        review.setProduct(productRepository.findBySlug(slug).orElseThrow(() ->new ResourceNotFoundException("Product")));
        return reviewRepository.save(review);
    }

    @Override
    public Review createReviewById(Long id, ReviewRequest reviewRequest) {
        Review review = this.convertToReview(reviewRequest);
        review.setProduct(productRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException("Product")));
        return reviewRepository.save(review);
    }

    @Override
    public Product save(Product entity) {
        if(entity.getSlug()==null){
            String slug = entity.getName().replaceAll(" ","-").replaceAll("[^\\w-]","").toLowerCase(Locale.ROOT);
            entity.setSlug(slug);
        }
        return productRepository.save(entity);
    }

    private Product convertToProduct(ProductRequest productRequest){
        Product product = new Product();
        product.setBrand(brandService.getByName(productRequest.getBrand()));
        product.setProductPrice(productRequest.getPrice());
        product.setName(productRequest.getName());
        product.setGender(productRequest.getGender());
        product.setDescription(productRequest.getDescription());
        return product;
    }
    private Review convertToReview(ReviewRequest reviewRequest){
        Review review = new Review();
        review.setContent(reviewRequest.getContent());
        review.setRating(reviewRequest.getRate());
        review.setTitle(reviewRequest.getTitle());
        review.setName(reviewRequest.getName());
        return review;
    }
    private ProductResponse convertToProductResponse(Product product){
        Map<String, Map<String,String>> colorsEachSize = new HashMap<>();
        List<ProductVariant> pVs = product.getProductVariants();
        //will remove it
        Integer sold = orderService.getSoldProduct(product);
        Long inStock = 0L;
        for(ProductVariant pV : pVs){
            inStock += pV.getQuantity();
            String key = pV.getSize().getName();
            String color = pV.getColor().getName();
            if(!colorsEachSize.containsKey(key))
                colorsEachSize.put(key,new HashMap<>());
            colorsEachSize.get(key).put(color,pV.getQuantity()+"");
        }
        List<ProductVariantResponse> pVRs = new ArrayList<>();
        product.setInStock(inStock);
        for(String key : colorsEachSize.keySet()){
            pVRs.add(convertToProductVariantResponse(key,colorsEachSize.get(key)));
        }
        return new ProductResponse(product,pVRs,sold);
    }

    private ProductDetailResponse convertToProductDetailResponse(Product product){
        Map<String, Map<String,String>> colorsEachSize = new HashMap<>();
        List<ProductVariant> pVs = product.getProductVariants();
        //will remove it
        Integer sold = orderService.getSoldProduct(product);
        Long inStock = 0L;
        for(ProductVariant pV : pVs){
            inStock += pV.getQuantity();
            String key = pV.getSize().getName();
            String color = pV.getColor().getName();
            if(!colorsEachSize.containsKey(key))
                colorsEachSize.put(key,new HashMap<>());
            colorsEachSize.get(key).put(color,pV.getQuantity()+"");
        }
        List<ProductVariantResponse> pVRs = new ArrayList<>();
        product.setInStock(inStock);
        for(String key : colorsEachSize.keySet()){
            pVRs.add(convertToProductVariantResponse(key,colorsEachSize.get(key)));
        }

        return new ProductDetailResponse(product,pVRs,sold);
    }

    private ProductVariantResponse convertToProductVariantResponse(String sizeName, Map<String,String> colors){
        return new ProductVariantResponse(sizeName,colors);
    }
}
