package com.sago.shoes.Services;


import com.sago.shoes.Models.Review;
import com.sago.shoes.Payload.StaffDTO.ReviewResponse;

import java.util.List;

public interface ReviewService {
    List<ReviewResponse> getAllReviews();
}
