package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Supplier;
import com.sago.shoes.Models.Supplier;
import com.sago.shoes.Repositories.SupplierRepository;
import com.sago.shoes.Services.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository SupplierRepository;

    @Override
    public Supplier save(Supplier Supplier) {
        return SupplierRepository.save(Supplier);
    }


    public Supplier createSupplier(Supplier Supplier) {
        return SupplierRepository.save(Supplier);
    }
    public Supplier updateSupplier(Long id, Supplier Supplier) {
        Supplier.setId(id);
        return SupplierRepository.save(Supplier);
    }

    @Override
    public List<Supplier> getAllSupplier() {
        return SupplierRepository.findAll();
    }

    public Supplier getById(Long id) {
        return SupplierRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Supplier")
        );
    }

    @Override
    public Supplier getByName(String name) {
        return SupplierRepository.findSupplierByName(name).orElseThrow(()-> new ResourceNotFoundException("Supplier"));
    }



}
