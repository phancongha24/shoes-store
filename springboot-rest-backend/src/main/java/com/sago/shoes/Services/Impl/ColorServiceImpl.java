package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Color;
import com.sago.shoes.Repositories.ColorRepository;
import com.sago.shoes.Services.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorRepository colorRepository;

    @Override
    public Color save(Color Color) {
        if(Color.getSlug()==null){
            String slug = Color.getName().toLowerCase(Locale.ROOT).replaceAll(" ","-");
            Color.setSlug(slug);
        }
        return colorRepository.save(Color);
    }

    @Override
    public Color update(Long id, Color Color) {
        return colorRepository.save(Color);
    }

    public Color getById(Long id) {
        return colorRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Color")
        );
    }

    @Override
    public Color getByName(String name) {
        return colorRepository.findByName(name).orElseThrow(() -> new ResourceNotFoundException("Color"));
    }


    @Override
    public List<Color> getAllColors() {
        return colorRepository.findAll();
    }


}
