package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Role;
import com.sago.shoes.Repositories.RoleRepository;
import com.sago.shoes.Services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository RoleRepository;

    @Override
    public Role save(Role Role) {
        return RoleRepository.save(Role);
    }

    @Override
    public Role update(Long id, Role Role) {
        return RoleRepository.save(Role);
    }

    public Role getById(Long id) {
        return RoleRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Role")
        );
    }

    @Override
    public Role getByName(String name) {
        return RoleRepository.findByTenQuyen(name).orElseThrow(() -> new ResourceNotFoundException("Role"));
    }


    @Override
    public List<Role> getAllRoles() {
        return RoleRepository.findAll();
    }


}
