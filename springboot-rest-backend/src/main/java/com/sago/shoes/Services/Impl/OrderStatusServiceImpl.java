package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.OrderStatus;
import com.sago.shoes.Repositories.OrderStatusRepository;
import com.sago.shoes.Services.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class OrderStatusServiceImpl implements OrderStatusService {

    @Autowired
    private OrderStatusRepository OrderStatusRepository;

    public OrderStatus save(OrderStatus OrderStatus) {
        return OrderStatusRepository.save(OrderStatus);
    }


    public OrderStatus createOrderStatus(OrderStatus OrderStatus) {
        return OrderStatusRepository.save(OrderStatus);
    }
    public OrderStatus updateOrderStatus(Long id, OrderStatus OrderStatus) {
        OrderStatus.setId(id);
        return OrderStatusRepository.save(OrderStatus);
    }

    public OrderStatus getById(Long id) {
        return OrderStatusRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("OrderStatus")
        );
    }

    @Override
    public OrderStatus getByName(String name) {
        return OrderStatusRepository.findOrderStatusByName(name).orElseThrow(()-> new ResourceNotFoundException("OrderStatus"));
    }


    @Override
    public List<OrderStatus> getAllOrderStatuses() {
        return OrderStatusRepository.findAll();
    }


}
