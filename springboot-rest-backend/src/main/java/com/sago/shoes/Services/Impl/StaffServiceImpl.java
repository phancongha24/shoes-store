package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Staff;
import com.sago.shoes.Payload.AuthDTO.StaffSignUpRequest;
import com.sago.shoes.Payload.StaffDTO.StaffResponse;
import com.sago.shoes.Repositories.StaffRepository;
import com.sago.shoes.Security.JWTokenProvider;
import com.sago.shoes.Services.RoleService;
import com.sago.shoes.Services.StaffService;
import com.sago.shoes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleService roleService;
    @Override
    public Staff save(Staff Staff) {
        return staffRepository.save(Staff);
    }

    @Override
    public StaffResponse createStaff(StaffSignUpRequest staffSignUpRequest) {
        Staff staff = this.convertToStaff(staffSignUpRequest);
        staff = this.save(staff);
        return this.convertToStaffResponse(staff);
    }

    @Override
    public StaffResponse updateStaff(Long id, StaffSignUpRequest staffSignUpRequest) {
        Staff staff = this.convertToStaff(staffSignUpRequest);
        staff.setId(id);
        staff = this.save(staff);
        return this.convertToStaffResponse(staff);
    }

    @Override
    public Staff getStaff(String token) {
        return null;
    }

    @Override
    public boolean existsByUsername(String username) {
        return staffRepository.existsByUsername(username);
    }

    @Override
    public Staff findByUsername(String username) {
        return staffRepository.findByUsername(username);
    }


    @Override
    public Staff findById(Long id) {
        return staffRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Staff"));
    }

    @Override
    public StaffResponse getById(Long id){
        return this.convertToStaffResponse(this.findById(id));
    }
    @Override
    public List<StaffResponse> getAllStaff()
    {
        return staffRepository.findAll().stream().map(this::convertToStaffResponse).toList();
    }

    private Staff convertToStaff(StaffSignUpRequest staffSignUpRequest){
        Staff staff = new Staff();
        staff.setFullName(staffSignUpRequest.getFullname());
        staff.setPassword(passwordEncoder.encode(staffSignUpRequest.getPassword()));
        staff.setRole(roleService.getByName(staffSignUpRequest.getRole()));
        staff.setUsername(staffSignUpRequest.getUsername());
        return staff;
    }
    private StaffResponse convertToStaffResponse(Staff staff){
        return new StaffResponse(staff);
    }
}
