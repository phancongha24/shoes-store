package com.sago.shoes.Services.Impl;
import com.sago.shoes.Models.Staff;
import com.sago.shoes.Models.User;
import com.sago.shoes.Repositories.StaffRepository;
import com.sago.shoes.Repositories.UserRepository;
import com.sago.shoes.Security.StaffDetail;
import com.sago.shoes.Security.UserDetail;
import com.sago.shoes.Services.StaffDetailService;
import com.sago.shoes.Services.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements StaffDetailService, UserDetailService,UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StaffRepository staffRepository;

    public UserDetail loadUserById(Long ID){
        User user = userRepository.findById(ID).orElseThrow(()->new UsernameNotFoundException(String.format("User %s not found",ID)));
        return UserDetail.create(user);
    }

    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email);
        Staff staff = staffRepository.findByUsername(email);
        if(user==null && staff !=null) return StaffDetail.create(staff);
        if(user!=null && staff ==null) return UserDetail.create(user);
        return null;
    }

    public StaffDetail loadStaffById(Long ID){
        Staff staff = staffRepository.findById(ID).orElseThrow(()->new UsernameNotFoundException(String.format("Staff %s not found",ID)));
        return StaffDetail.create(staff);
    }

}
