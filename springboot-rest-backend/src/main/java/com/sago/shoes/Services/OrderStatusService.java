package com.sago.shoes.Services;


import com.sago.shoes.Models.Order;
import com.sago.shoes.Models.OrderStatus;

import java.util.List;

public interface OrderStatusService{
    OrderStatus save(OrderStatus orderStatus);
    OrderStatus createOrderStatus(OrderStatus OrderStatus);
    OrderStatus updateOrderStatus(Long id, OrderStatus OrderStatus);
    List<OrderStatus> getAllOrderStatuses();
    OrderStatus getById(Long id);
    OrderStatus getByName(String name);
}
