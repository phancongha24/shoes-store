package com.sago.shoes.Services;


import com.sago.shoes.Models.Brand;
import com.sago.shoes.Models.Supplier;

import java.util.List;

public interface SupplierService{
    Supplier  save(Supplier supplier);
    Supplier createSupplier(Supplier supplier);
    Supplier updateSupplier(Long id, Supplier supplier);
    List<Supplier> getAllSupplier();
    Supplier getById(Long id);
    Supplier getByName(String name);
}
