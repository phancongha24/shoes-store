package com.sago.shoes.Services;

import com.sago.shoes.Security.StaffDetail;

public interface StaffDetailService {
    StaffDetail loadStaffById(Long ID);
}
