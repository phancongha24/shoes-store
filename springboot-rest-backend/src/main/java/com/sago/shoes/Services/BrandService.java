package com.sago.shoes.Services;


import com.sago.shoes.Models.Brand;

import java.util.List;

public interface BrandService{
    Brand save(Brand brand);
    Brand createBrand(Brand brand);
    Brand updateBrand(Long id, Brand brand);
    List<Brand> getAllBrands();
    Brand getById(Long id);
    Brand getByName(String name);
}
