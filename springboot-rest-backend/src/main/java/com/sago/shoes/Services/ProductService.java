package com.sago.shoes.Services;

import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductVariant;
import com.sago.shoes.Models.Review;
import com.sago.shoes.Payload.ProductDTO.ProductRequest;
import com.sago.shoes.Payload.ProductDTO.ProductResponse;
import com.sago.shoes.Payload.ProductDTO.ReviewRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;


public interface ProductService{
    Product save(Product product);
    Product updateProduct(Long id,ProductRequest product, MultipartFile[] images);
    Product createProduct(ProductRequest product, MultipartFile[] images);
    List<ProductResponse> getAllProducts(Pageable pageable, @Nullable List<Specification<Product>> productCriteria);
    ProductResponse getById(Long id);
    ProductResponse getBySlug(String slug);
    void updateQuantity(ProductVariant productVariant, Integer quantity);
    ProductVariant findProductVariant(Long id);
    ProductVariant findProductVariant(String productName);
    String getFullProductName(ProductVariant productVariant);
    Review createReviewBySlug(String slug, ReviewRequest reviewRequest);
    Review createReviewById(Long id, ReviewRequest reviewRequest);
    Object test(Pageable pageable, @Nullable Specification<Product> productCriteria);
}
