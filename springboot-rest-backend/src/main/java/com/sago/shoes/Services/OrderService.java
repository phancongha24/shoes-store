package com.sago.shoes.Services;


import com.sago.shoes.Models.*;
import com.sago.shoes.Payload.OrderDTO.*;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface OrderService{
    Order save(Order order);
    Object createOrder(OrderRequest Order, Long userId);
    OrderResponse updateOrder(Order order, String Order, String reason, String username);
    List<OrderResponse> getAllOrders(Map<String,String> filterParam, Pageable pageable);
    List<OrderResponse> getAllOrdersOfUser(String username, Map<String,String> filterParam, Pageable pageable);
    OrderDetailResponse getById(Long id);
    OrderResponse changeOrderStatus(Long id, String orderStatus, String reason, String username);
    OrderResponse cancelOrder(Long id, String reason, String username);
    String createPayment(PaymentRequest paymentRequest);
    String callbackPayment(Map<String, String> vnpay);
    Map<String, Object> statisticOrder();
    List<OrderTrackingResponse> tracking(String orderFilter);
    Integer getSoldProduct(Product product);
    Map<String,Integer> getSoldProducts();
}
