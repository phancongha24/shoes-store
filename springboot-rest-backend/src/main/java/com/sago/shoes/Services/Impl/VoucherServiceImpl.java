package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.*;
import com.sago.shoes.Models.Order;
import com.sago.shoes.Models.UsedVoucher;
import com.sago.shoes.Models.User;
import com.sago.shoes.Models.Voucher;
import com.sago.shoes.Payload.VoucherDTO.VoucherRequest;
import com.sago.shoes.Payload.VoucherDTO.VoucherResponse;
import com.sago.shoes.Repositories.UsedVoucherRepository;
import com.sago.shoes.Repositories.VoucherRepository;
import com.sago.shoes.Services.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class VoucherServiceImpl implements VoucherService {
    @Autowired
    private VoucherRepository voucherRepository;
    @Autowired
    private UsedVoucherRepository usedVoucherRepository;

    @Override
    public Voucher save(Voucher voucher) {
        return this.voucherRepository.save(voucher);
    }

    @Override
    public Voucher createVoucher(VoucherRequest voucherRequest) {
        Voucher voucher = convertToVoucher(voucherRequest);
        voucher.setQuantity(voucher.getTotal());
        if(voucherRepository.existsByCode(voucher.getCode()))
            throw new ResourceExistsException("Code");
        return this.save(voucher);
    }

    public Voucher updateVoucher(Long id, VoucherRequest voucherRequest){
        Voucher voucher = convertToVoucher(voucherRequest);
        voucher.setQuantity(voucherRepository.findById(id).get().getQuantity());
        voucher.setId(id);
        return this.save(voucher);
    }

    @Override
    public List<VoucherResponse> getAllVouchers(Map<String, String> filterParam, Pageable pageable) {
        if(filterParam.get("available")!= null && filterParam.get("available")=="true")
        return voucherRepository.findAll(pageable).filter(voucher -> voucher.getQuantity()>0).map(this::convertToVoucherResponse).toList();
        return voucherRepository.findAll(pageable).map(this::convertToVoucherResponse).toList();
    }

    @Override
    public Voucher findById(Long id) {
        return (voucherRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Voucher")));
    }

    @Override
    public Voucher findByCode(String code) {
        return voucherRepository.findVoucherByCode(code).orElseThrow(()-> new ResourceNotFoundException("Voucher"));
    }

    @Override
    public VoucherResponse getById(Long id) {
        return this.convertToVoucherResponse(voucherRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Voucher")));
    }


    @Override
    public VoucherResponse getByCode(String code) {
        return this.convertToVoucherResponse(voucherRepository.findVoucherByCode(code).orElseThrow(()-> new ResourceNotFoundException("Voucher")));
    }

    @Override
    public boolean isValid(Voucher voucher,Long userId) {
        if(voucher.getQuantity()<1)   throw new OutOfStockException("Voucher "+ voucher.getCode());
        if(voucher.getExpiredDate().before(java.util.Calendar.getInstance().getTime())) throw new InvalidVoucherException("voucher "+ voucher.getCode() + " is expired");
        for(UsedVoucher usedVoucher: voucher.getUsedVouchers()){
            if(usedVoucher.getUser().getId().equals(userId)) throw new InvalidVoucherException("Voucher "+ voucher.getCode()+ " has been used");
        }
        return true;
    }

    @Override
    public void applyVoucher(Voucher voucher, Order order, User user) {
        UsedVoucher usedVoucher = createUsedVoucher(user);
        voucher.setQuantity(voucher.getQuantity()-1);
        List<UsedVoucher> usedVouchers = voucher.getUsedVouchers();
        if(usedVoucher==null)
            usedVouchers = new ArrayList<>();
        usedVoucher.setVoucher(voucher);
        usedVouchers.add(usedVoucher);
        usedVoucherRepository.save(usedVoucher);
        voucherRepository.save(voucher);
    }

    @Override
    public Object validateVoucher(String code,Long userId) {
        try{
            Voucher voucher = this.findByCode(code);
            isValid(voucher,userId);
            return this.convertToVoucherResponse(voucher);
        }catch(Exception ex){
            return ex.getMessage();
        }
    }

    private UsedVoucher createUsedVoucher(User user){
        UsedVoucher usedVoucher = new UsedVoucher();
        usedVoucher.setUser(user);
        return usedVoucher;
    }
    private VoucherResponse convertToVoucherResponse(Voucher voucher){
        return new VoucherResponse(voucher);
    }
    private Voucher convertToVoucher(VoucherRequest voucherRequest){
        Voucher voucher = new Voucher();
        voucher.setCode(voucherRequest.getCode());
        voucher.setDescription(voucherRequest.getDescription());
        voucher.setDiscountAmount(voucherRequest.getDiscountAmount().doubleValue());
        voucher.setExpiredDate(voucherRequest.getExpiredDate());
        voucher.setName(voucherRequest.getName());
        voucher.setTotal(voucherRequest.getTotal());
        voucher.setMaxDiscountAmount(voucherRequest.getMaxDiscountAmount().doubleValue());
        return voucher;
    }
}
