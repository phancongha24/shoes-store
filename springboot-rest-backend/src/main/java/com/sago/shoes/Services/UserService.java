package com.sago.shoes.Services;

import com.sago.shoes.Models.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {
    void save(User user);
    boolean existsByEmail(String email);
    User findByEmail(String email);
    void updateProfile(Long id, String phoneNumber);
    User findById(Long id);
    User JWTokenToUser(HttpServletRequest httpServletRequest);
    String generateRecoveryPassword(int length);
    List<User> getAllUser();

}
