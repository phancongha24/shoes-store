package com.sago.shoes.Services.Impl;

import com.sago.shoes.Models.Review;
import com.sago.shoes.Payload.StaffDTO.ReviewResponse;
import com.sago.shoes.Repositories.ReviewRepository;
import com.sago.shoes.Services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository ReviewRepository;

    @Override
    public List<ReviewResponse> getAllReviews() {
        return ReviewRepository.findAll().stream().map(this::convertToReviewResponse).toList();
    }

    private ReviewResponse convertToReviewResponse(Review review){
        return new ReviewResponse(review);
    }

}
