package com.sago.shoes.Specifications;

import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductVariant;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

public class ProductSpecification {

    public static Specification<Product> betweenPrice(Long gte, Long lte){
        return ((root, query, cb) -> cb.between(root.get("productPrice"),gte,lte));
    }

    public static Specification<Product> hasGenderIn(String gender){
        return (((root, query, criteriaBuilder) ->{
            return root.get("gender").in(gender.split(","));
        }
        ));
    }

    public static Specification<Product> hasSizeIn(String size){
        return (((root, query, criteriaBuilder) ->{
            Join<Product, ProductVariant> productVariantJoin   = root.join("productVariants", JoinType.LEFT);

            return productVariantJoin.get("size").get("name").in(size.split(","));
        }
        ));
    }
    public static Specification<Product> hasBrandIn(String brand){
        return (((root, query, criteriaBuilder) ->
            root.get("brand").get("slug").in(brand.split(","))
        ));
    }

    public static Specification<Product> hasColorIn(String color){
        return (((root, query, criteriaBuilder) ->{
          Join<Product, ProductVariant> productVariantJoin   = root.join("productVariants", JoinType.LEFT);
          return productVariantJoin.get("color").get("slug").in(color.split(","));
        }
        ));
    }
}
