package com.sago.shoes.Payload.AuthDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserSignInRequest {

    @NotBlank
    private String email;

    @NotBlank
    private String password;
}
