package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.OrderItem;
import lombok.Data;

@Data
public class OrderItemResponse {
    @JsonProperty
    private String productName;
    @JsonProperty
    private int quantity;
    @JsonProperty
    private Long price;

    public OrderItemResponse(String productName, int quantity, Long price) {
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
    }
    public OrderItemResponse(OrderItem orderItem){
        //Name Color Size
        this(orderItem.getProductVariant().getProduct().getName()  + " " +orderItem.getProductVariant().getColor().getName() + " " +orderItem.getProductVariant().getSize().getName(), orderItem.getQuantity(), Double.valueOf(orderItem.getPrice()).longValue());
    }
}
