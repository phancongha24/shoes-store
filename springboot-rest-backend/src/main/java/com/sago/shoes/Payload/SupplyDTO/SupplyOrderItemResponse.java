package com.sago.shoes.Payload.SupplyDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.SupplyOrderItem;
import lombok.Data;

@Data
public class SupplyOrderItemResponse {
    @JsonProperty
    private String productName;
    @JsonProperty
    private int quantity;
    @JsonProperty
    private double price;

    public SupplyOrderItemResponse(String productName, int quantity, double price) {
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
    }
    public SupplyOrderItemResponse(SupplyOrderItem SupplyOrderItem){
        //Name Color Size
        this(SupplyOrderItem.getProductVariant().getProduct().getName()  + " " +SupplyOrderItem.getProductVariant().getColor().getName() + " " +SupplyOrderItem.getProductVariant().getSize().getName(), SupplyOrderItem.getQuantity(), SupplyOrderItem.getPrice());
    }
}
