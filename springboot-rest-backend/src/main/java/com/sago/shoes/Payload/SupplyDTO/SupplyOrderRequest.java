package com.sago.shoes.Payload.SupplyDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.util.List;

@Data

public class SupplyOrderRequest {

    @JsonProperty
    private String note;

    @JsonProperty
    private List<SupplyOrderItemRequest> SupplyOrderItems;

    @JsonProperty
    private Long supplyName;


}
