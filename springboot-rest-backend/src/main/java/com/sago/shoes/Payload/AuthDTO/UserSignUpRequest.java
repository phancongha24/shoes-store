package com.sago.shoes.Payload.AuthDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserSignUpRequest {
    @NotBlank
    private String fullName;

    @NotBlank
    private String email;

    @NotBlank
    private String phoneNumber;

    @NotBlank
    private String password;
}
