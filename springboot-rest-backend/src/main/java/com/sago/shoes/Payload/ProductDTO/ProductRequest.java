package com.sago.shoes.Payload.ProductDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Brand;
import com.sago.shoes.Models.ProductVariant;
import lombok.Data;

import java.util.List;

@Data
public class ProductRequest {
    @JsonProperty(required = false)
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private double price;
    @JsonProperty
    private String gender;
    @JsonProperty
    private String brand;
    @JsonProperty
    private String description;
}
