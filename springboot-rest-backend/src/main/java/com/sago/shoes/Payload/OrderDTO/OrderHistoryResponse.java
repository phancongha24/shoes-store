package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.OrderHistory;
import com.sago.shoes.Payload.DateResponse;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderHistoryResponse extends DateResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String status;

    @JsonProperty
    private String reason;

    @JsonProperty
    private String editedBy = null;


    public OrderHistoryResponse(OrderHistory orderHistory){
        this.id = orderHistory.getId();
        this.status = orderHistory.getOrderStatus().getName();
        this.reason = orderHistory.getReason();
        if(orderHistory.getStaff() != null)
            this.editedBy = orderHistory.getStaff().getUsername();
        super.setCreatedAt(orderHistory.getCreatedAt());
    }
}
