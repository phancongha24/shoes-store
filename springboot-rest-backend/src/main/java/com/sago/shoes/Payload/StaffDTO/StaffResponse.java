package com.sago.shoes.Payload.StaffDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Staff;
import lombok.Data;

@Data
public class StaffResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String username;
    @JsonProperty
    private String fullName;
    @JsonProperty
    private String roleName;

    public StaffResponse(Long id,String username, String fullName, String roleName) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.roleName = roleName;
    }

    public StaffResponse(Staff staff){
        this(staff.getId(),staff.getUsername(),staff.getFullName(),staff.getRole().getTenQuyen());
    }
}
