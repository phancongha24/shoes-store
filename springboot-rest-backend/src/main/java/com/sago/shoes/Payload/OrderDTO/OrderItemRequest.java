package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.ProductVariant;
import lombok.Data;

@Data
public class OrderItemRequest {
    @JsonProperty
    private String productName;

    @JsonProperty
    private int quantity;


}
