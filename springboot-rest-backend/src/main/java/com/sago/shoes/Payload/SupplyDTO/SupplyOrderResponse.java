package com.sago.shoes.Payload.SupplyDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.SupplyOrder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data

public class SupplyOrderResponse {

    @JsonProperty
    private Long id;
    @JsonProperty
    private Double total;
    @JsonProperty
    private String supplier;
    @JsonProperty
    private String username;
    @JsonProperty
    private Date createdAt;
    @JsonProperty
    private Date updatedAt;
    @JsonProperty
    private List<SupplyOrderItemResponse> SupplyOrderItems;


    public SupplyOrderResponse(Long id,Double total, String supplier, String username, Date createdAt, Date updatedAt, List<SupplyOrderItemResponse> supplyOrderItems) {
        this.id =id;
        this.total = total;
        this.supplier = supplier;
        this.username = username;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        SupplyOrderItems = supplyOrderItems;
    }

    public SupplyOrderResponse(SupplyOrder supplyOrder, List<SupplyOrderItemResponse> supplyOrderItemResponses){

        this(supplyOrder.getId(),supplyOrder.getTotal(),
        supplyOrder.getSupplier().getName(),
        supplyOrder.getStaff().getUsername(),
        supplyOrder.getCreatedAt(),
        supplyOrder.getUpdatedAt(),supplyOrderItemResponses);

    }

}
