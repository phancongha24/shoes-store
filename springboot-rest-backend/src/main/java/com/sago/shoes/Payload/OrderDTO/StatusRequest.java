package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StatusRequest {
    @JsonProperty
    private String status;
    @JsonProperty
    private String reason;
}
