package com.sago.shoes.Payload.VoucherDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Voucher;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class VoucherRequest {
    @JsonProperty
    private String code;

    @JsonProperty
    private Integer total;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private Long discountAmount;

    @JsonProperty
    private Long maxDiscountAmount;

    @JsonProperty
    @NotNull
    private Date expiredDate;

    public VoucherRequest(String code, Integer quantity, String name, String description, Long discountAmount, Long maxDiscountAmount, Date expiredDate) {
        this.code = code;
        this.total = quantity;
        this.name = name;
        this.description = description;
        this.discountAmount = discountAmount;
        this.maxDiscountAmount = maxDiscountAmount;
        this.expiredDate = expiredDate;
    }

    public VoucherRequest(Voucher voucher){
        this(voucher.getCode(),
                voucher.getQuantity(),
        voucher.getName(),
        voucher.getDescription(),
        voucher.getDiscountAmount().longValue(),voucher.getMaxDiscountAmount().longValue(),voucher.getExpiredDate());

    }

}
