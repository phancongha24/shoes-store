package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Null;
import java.util.List;

@Data

public class OrderRequest {

    @JsonProperty
    private String address;

    @JsonProperty
    private String note;

    private List<OrderItemRequest> orderItems;

    @JsonProperty
    private String paymentMethod;

    @JsonProperty
    @JsonIgnore
    @Nullable
    private String ipAddress;

    @JsonProperty
    private String customer;

    @JsonProperty
    private String phoneNumber;

    @JsonProperty
    @Nullable
    private String code;
}
