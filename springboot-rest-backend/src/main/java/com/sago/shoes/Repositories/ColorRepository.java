package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Color;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ColorRepository extends JpaRepository<Color,Long> {
    List<Color> findAll();
    Optional<Color> findByName(String name);

}
