package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StaffRepository extends JpaRepository<Staff,Long> {
    List<Staff> findAll();
    boolean existsByUsername(String username);
    Staff findByUsername(String username);
}
