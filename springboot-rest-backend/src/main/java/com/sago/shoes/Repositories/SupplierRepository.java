package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier,Long> {
    List<Supplier> findAll();
    Optional<Supplier> findSupplierByName(String name);
}
