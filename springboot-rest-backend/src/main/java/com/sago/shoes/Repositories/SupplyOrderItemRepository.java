package com.sago.shoes.Repositories;

import com.sago.shoes.Models.OrderItem;
import com.sago.shoes.Models.SupplyOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplyOrderItemRepository extends JpaRepository<SupplyOrderItem,Long> {

}
