package com.sago.shoes.Repositories;

import com.sago.shoes.Models.UsedVoucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsedVoucherRepository extends JpaRepository<UsedVoucher,Long> {

}
