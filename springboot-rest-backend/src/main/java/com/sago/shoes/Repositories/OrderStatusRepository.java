package com.sago.shoes.Repositories;

import com.sago.shoes.Models.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderStatusRepository extends JpaRepository<OrderStatus,Long> {
    List<OrderStatus> findAll();
    Optional<OrderStatus> findOrderStatusByName(String name);
}
