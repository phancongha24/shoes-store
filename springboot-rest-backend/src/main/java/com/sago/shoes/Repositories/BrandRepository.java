package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BrandRepository extends JpaRepository<Brand,Long> {
    List<Brand> findAll();
    Optional<Brand> findBrandBySlug(String name);
}
