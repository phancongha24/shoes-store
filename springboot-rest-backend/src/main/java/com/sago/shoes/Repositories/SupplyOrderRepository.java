package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Order;
import com.sago.shoes.Models.SupplyOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplyOrderRepository extends JpaRepository<SupplyOrder,Long>, JpaSpecificationExecutor<SupplyOrder> {

}
