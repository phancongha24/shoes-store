package com.sago.shoes.Repositories;

import com.sago.shoes.Models.OrderItem;
import com.sago.shoes.Models.ProductVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem,Long> {
    List<OrderItem> findAll();
    @Query(value = "SELECT p.productVariant.id, SUM(p.quantity) as T\n" +
            "FROM OrderItem p \n" +
            "group by p.productVariant.id \n" +
            "order by T desc")
    List<Long> top5BestSeller();

    @Query("SELECT A.productVariant.product.brand.name,sum(A.quantity) as T\n" +
            "FROM OrderItem A\n"+
            "GROUP BY A.productVariant.product.brand.name")
    List<Object[]> salesByBrand();
    @Query("SELECT COUNT(A.productVariant.id)" +
            "FROM OrderItem A\n" +
            "WHERE A.productVariant= :productVariant\n" +
            "GROUP BY A.productVariant.id")
    Integer getSold(ProductVariant productVariant);
}
