package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductVariant;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductVariantRepository extends JpaRepository<ProductVariant,Long> , JpaSpecificationExecutor<Product> {
    List<ProductVariant> findAll();
    @Query(value = "SELECT p FROM ProductVariant p WHERE p.product.id = :product and p.size.id = :size and p.color.id = :color")
    ProductVariant findBySizeAndColor(@Param("product") Long product, @Param("size") Long sizeID,@Param("color") Long colorID);

}
