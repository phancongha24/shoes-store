package com.sago.shoes.Models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sago.shoes.Models.ProductVariant;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name="api_color")
public class Color implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "slug")
    private String slug;

    @OneToMany(mappedBy = "color")
    @JsonIgnore
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ProductVariant> productVariants;
}