package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="api_product")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "name",length=10485760)
    private String name;

    @Column(name ="description", length=10485760)
    private String description;

    @Column(name = "slug", unique = true)
    private String slug;

    @Column(name ="price")
    @Min(value = 0, message = "Product price must no be less then zero.")
    private double productPrice;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="brand_id")
    private Brand brand;

    @Column(columnDefinition = "varchar(255) default 'men'")
    private String gender;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonIgnore
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ProductVariant> productVariants = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ProductImage> productImages = new ArrayList<>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private List<Review> reviews = new ArrayList<>();

    @Transient
    private Long inStock;


}