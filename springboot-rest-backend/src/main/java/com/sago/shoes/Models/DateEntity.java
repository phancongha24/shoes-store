package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Data
@MappedSuperclass
@JsonIgnoreProperties(
        value = { "createdAt", "updatedAt" },
        allowGetters = true
)
@EntityListeners(AuditingEntityListener.class)
public abstract class DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @CreatedDate
    @Column(name="created", nullable = false, updatable = false)
    private Date createdAt;

    @LastModifiedDate
    @Column(name="updated", nullable = false)
    private Date updatedAt;

}
