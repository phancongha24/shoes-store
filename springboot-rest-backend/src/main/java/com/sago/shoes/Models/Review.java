package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="api_review")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Review extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "rating")
    private Long rating;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="product_id")
    @JsonIgnore
    private Product product;
}