package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="api_voucher")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Voucher extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column
    private String code;

    @Column
    private Integer quantity;

    @Column
    private Integer total;

    @Column
    private String name;

    @Column
    private String description;

    @Column(name="discount_amount")
    private Double discountAmount;

    @Column(name="max_discount_amount")
    private Double maxDiscountAmount;
    @Column(name="expired_date")
    private Date expiredDate;

    @OneToMany(mappedBy = "voucher")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<UsedVoucher> usedVouchers;



}
