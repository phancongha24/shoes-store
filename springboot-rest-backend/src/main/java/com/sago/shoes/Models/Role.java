package com.sago.shoes.Models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="api_role")
@NoArgsConstructor
@RequiredArgsConstructor
public class Role extends DateEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotBlank
    @Column(name="tenquyen")
    @NonNull
    private String tenQuyen;
    @OneToMany(fetch = FetchType.LAZY,mappedBy="role")
    private List<Staff> listStaff = new ArrayList<>();
}
