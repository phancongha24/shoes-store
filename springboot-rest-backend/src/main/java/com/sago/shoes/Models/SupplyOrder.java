package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="api_supply")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SupplyOrder extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="staff_id")
    private Staff staff;

    @ManyToOne
    @JoinColumn(name="supplier_id")
    private Supplier supplier;

    @Column(name = "phonenumber")
    private Double total;

    @OneToMany(mappedBy = "supplyOrder")
    private List<SupplyOrderItem> supplyOrderItemList;
}