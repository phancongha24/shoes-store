package com.sago.shoes.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class ForbiddenException extends RuntimeException{

    public ForbiddenException(){
        super("Forbidden");
    }
}
