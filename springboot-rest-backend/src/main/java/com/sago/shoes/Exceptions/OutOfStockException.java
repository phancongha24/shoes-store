package com.sago.shoes.Exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class OutOfStockException extends RuntimeException{
    private String resource;
    public OutOfStockException(String resource){
        super(resource + " out of stock!!");
        this.resource = resource;
    }
}
