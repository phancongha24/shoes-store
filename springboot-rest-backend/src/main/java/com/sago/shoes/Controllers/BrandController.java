package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Brand;
import com.sago.shoes.Services.BrandService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/brands")
@Api(value = "Brand Resources")
public class BrandController {
    @Autowired
    private BrandService brandService;
    @GetMapping
    @ApiOperation(value="get all Brands")
    public List<Brand> getAllBrands(){
        return brandService.getAllBrands();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one Brand")
    public Brand getBrandById(@PathVariable(value="id")Long id){
        return brandService.getById(id);
    }

    @PostMapping
    @ApiOperation(value="create a new Brand")
    public ResponseEntity createBrand(@Valid @RequestBody Brand brand){
        return ResponseEntity.status(HttpStatus.CREATED).body(brandService.createBrand(brand));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a Brand")
    public ResponseEntity updateBrand(
            @ApiParam(value = "id of a brand")
            @PathVariable(value="id")Long id, @Valid @RequestBody Brand brand){

        return ResponseEntity.status(HttpStatus.OK).body(brandService.updateBrand(id,brand));
    }

    @PatchMapping("/{id}")
    @ApiOperation(value="partial update a Brand")
    public ResponseEntity partialUpdateBrand(int id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Partial updated" +id);
    }
}
