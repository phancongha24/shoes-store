package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.Review;
import com.sago.shoes.Payload.ProductDTO.ProductRequest;
import com.sago.shoes.Payload.ProductDTO.ProductResponse;
import com.sago.shoes.Payload.ProductDTO.ReviewRequest;
import com.sago.shoes.Services.Impl.FileServiceImpl;
import com.sago.shoes.Services.ProductService;
import com.sago.shoes.Specifications.ProductSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/products")
@Api(value = "Product Resources")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private FileServiceImpl fileStorageService;
    @GetMapping
    @ApiOperation(value="get all products")
    //brands=Nike,Converse,Adidas
    // &gender=Women
    // &colors=red,blue,yellow
    // &page=1
    // &salePrice_gte=200
    // &salePrice_lte=500
    // &sizes=24,25,26
    // &sort=asc
    public Object getAllProducts(
            @RequestParam(value="page", defaultValue = "1") Integer page,
            @RequestParam(value="limit", defaultValue = "30") Integer limit,
            @RequestParam(value="brands",required = false) String brands,
            @RequestParam(value="colors",required = false) String colors,
            @RequestParam(value="genders", required = false) String genders,
            @RequestParam(value="price_gte", required = false) Long priceGTE,
            @RequestParam(value="price_lte", required = false) Long priceLTE,
            @RequestParam(value="sizes", required = false) String sizes,
            @RequestParam(value="sort", defaultValue = "desc") String sort
                                                ){
        List<Specification<Product>> conditions = new ArrayList<>();
        Sort sortT = sort.equals("desc") ? Sort.by(Sort.Direction.DESC,"productPrice"): Sort.by(Sort.Direction.ASC,"productPrice");
        Pageable pageRequest = PageRequest.of(page-1,limit, sortT);
        Long lte=Long.MAX_VALUE,gte=0L;
        if(brands != null) conditions.add(ProductSpecification.hasBrandIn(brands));
        if(colors != null) conditions.add(ProductSpecification.hasColorIn(colors));
        if(sizes  != null) conditions.add(ProductSpecification.hasSizeIn(sizes));
        if(genders != null) conditions.add(ProductSpecification.hasGenderIn(genders));
        if(priceGTE != null || priceLTE != null) {
            if (priceGTE != null) gte = priceGTE;
            if (priceLTE != null) lte = priceLTE;
            conditions.add(ProductSpecification.betweenPrice(gte, lte));
        }
        return productService.getAllProducts(pageRequest, conditions);
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one product")
    public ProductResponse getProductBySlugOrId(@PathVariable(value="id")String param){
        if (param.matches("\\d+"))
        return productService.getById(Long.parseLong(param));
        return productService.getBySlug(param);
    }

    @PostMapping
    @ApiOperation(value="create a new product")
    public ResponseEntity createProduct(@Valid @RequestParam(name="product") ProductRequest product, @Valid @RequestParam(name="images") MultipartFile[] images){

        return ResponseEntity.status(HttpStatus.CREATED).body(productService.createProduct(product,images));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a product")
    public ResponseEntity updateProduct(@PathVariable(name="id") Long id, @Valid @RequestParam(name="product") ProductRequest productRequest, @Valid @RequestParam(name="images") MultipartFile[] images){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(productService.updateProduct(id,productRequest,images));
    }

//    @PatchMapping("/{id}")
//    @ApiOperation(value="partial update a product")
//    public ResponseEntity partialUpdateProduct(@PathVariable(name="id") Long id){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(productService.updatePrice(id));
//    }

    @PostMapping("/{id}/reviews")
    @ApiOperation(value="write a review")
    public Review createReview(@PathVariable(value="id")String param, @Valid @RequestBody ReviewRequest reviewRequest){
        if (param.matches("\\d+"))
            return productService.createReviewById(Long.parseLong(param),reviewRequest);
        return productService.createReviewBySlug(param,reviewRequest);
    }
    @GetMapping("/images/{fileName}")
    public ResponseEntity getImage(@PathVariable String fileName){
        try {
            Resource resource = fileStorageService.loadFileAsResource(fileName);
            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(resource.getFile().toPath()));
        }catch(Exception ex){
            return ResponseEntity.noContent().build();
        }
    }

}
