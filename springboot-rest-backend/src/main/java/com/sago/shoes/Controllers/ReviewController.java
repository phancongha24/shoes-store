package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Review;
import com.sago.shoes.Payload.StaffDTO.ReviewResponse;
import com.sago.shoes.Services.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/reviews")
@Api(value = "review Resources")
public class ReviewController {
    @Autowired
    private ReviewService reviewService;
    @GetMapping
    @ApiOperation(value="get all reviews")
    public List<ReviewResponse> getAllreviews(){
        return reviewService.getAllReviews();
    }


}
