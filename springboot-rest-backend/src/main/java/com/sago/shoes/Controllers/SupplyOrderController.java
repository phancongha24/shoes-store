package com.sago.shoes.Controllers;
import com.sago.shoes.Models.SupplyOrder;
import com.sago.shoes.Payload.OrderDTO.OrderResponse;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderRequest;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderResponse;
import com.sago.shoes.Security.StaffDetail;
import com.sago.shoes.Security.UserDetail;
import com.sago.shoes.Services.SupplyOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/supply-orders")
@Api(value = "Order Resources")
public class SupplyOrderController {
    @Autowired
    private SupplyOrderService SupplyOrderService;

    @GetMapping
    @ApiOperation(value = "get all SupplyOrder")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public List<SupplyOrderResponse> getAllOrders(
            @RequestParam(value="page", defaultValue = "1") Integer page,
            @RequestParam(value="limit", defaultValue = "10") Integer limit,
            @RequestParam(value="suppliers",required = false) String suppliers,
            @RequestParam(value="from", required = false) String fromDate,
            @RequestParam(value="to", required = false) String toDate,
            @AuthenticationPrincipal UserDetails userDetail) {
        Map<String, String> filterParam = new HashMap<>();
        filterParam.put("suppliers",suppliers);
        filterParam.put("from",fromDate);
        filterParam.put("to",toDate);
        Pageable pageable = PageRequest.of(page-1,limit);
        return SupplyOrderService.getAllSupplyOrders(filterParam,pageable);

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('1') or hasAuthority('2')")
    public SupplyOrderResponse getOrderById(@PathVariable(value = "id") Long id) {
        return SupplyOrderService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    @ApiOperation(value = "create a new SupplyOrder (only for Staff)")
    public ResponseEntity createOrder(@Valid @RequestBody SupplyOrderRequest supplyOrderRequest, @AuthenticationPrincipal StaffDetail staffDetail) {
        return ResponseEntity.status(HttpStatus.CREATED).body(SupplyOrderService.createSupplyOrder(supplyOrderRequest,staffDetail.getId()));
    }


    }


