package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Order;
import com.sago.shoes.Payload.OrderDTO.*;
import com.sago.shoes.Security.UserDetail;
import com.sago.shoes.Services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/orders")
@Api(value = "Order Resources")
public class OrderController {
    @Autowired
    private OrderService OrderService;

    @GetMapping
    @ApiOperation(value = "get all Orders")
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('1') or hasAuthority('2')")
    public List<OrderResponse> getAllOrders(
            @RequestParam(value="page", defaultValue = "1") Integer page,
            @RequestParam(value="limit", defaultValue = "10") Integer limit,
            @RequestParam(value="statuses",required = false) String statuses,
            @RequestParam(value="usernames",required = false) String usernames,
            @RequestParam(value="phone",required = false) String phone,
            @RequestParam(value="from", required = false) String fromDate,
            @RequestParam(value="to", required = false) String toDate,
            @AuthenticationPrincipal UserDetails userDetail) {
        Map<String, String> filterParam = new HashMap<>();
        filterParam.put("statuses",statuses);
        filterParam.put("from",fromDate);
        filterParam.put("to",toDate);
        filterParam.put("phone",phone);
        Pageable pageable = PageRequest.of(page-1,limit, Sort.by("id").descending());
        if (userDetail.getUsername().contains("@"))
            return OrderService.getAllOrdersOfUser(userDetail.getUsername(),filterParam,pageable);
        filterParam.put("usernames", usernames);
        return OrderService.getAllOrders(filterParam,pageable);

    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get one Order")
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('1') or hasAuthority('2')")
    public OrderDetailResponse getOrderById(@PathVariable(value = "id") Long id) {
        return OrderService.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    @ApiOperation(value = "create a new Order (only for Customer)")
    public ResponseEntity createOrder(@Valid @RequestBody OrderRequest orderRequest, @AuthenticationPrincipal UserDetail userDetail, HttpServletRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(OrderService.createOrder(orderRequest,userDetail.getId()));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "update a Order")
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('1') or hasAuthority('2')")
    public ResponseEntity updateOrder(
            @ApiParam(value = "id of a Order")
            @PathVariable(value = "id") Long id, @Valid @RequestBody StatusRequest statusRequest, @AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.changeOrderStatus(id, statusRequest.getStatus(), statusRequest.getReason(),userDetails.getUsername()));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Cancel a order")
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('1') or hasAuthority('2')")
    public ResponseEntity cancelOrder(Long id, @Valid @RequestParam(name = "reason") String reason, @AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.cancelOrder(id,reason,userDetails.getUsername()));
    }

    @PostMapping("/payment")
    @ApiOperation(value=" For VNPay")
    public ResponseEntity payment(@Valid @RequestBody PaymentRequest paymentRequest, HttpServletRequest request){
        paymentRequest.setIpAddress(request.getRemoteAddr());
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.createPayment(paymentRequest));
    }
    @GetMapping("/vnpay-return")
    @ApiOperation(value=" For VNPay")
    public ResponseEntity callbackPayment(@Valid @RequestParam Map<String, String> vnpay){
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.callbackPayment(vnpay));
    }
    @GetMapping("/statistic")
    public ResponseEntity statisticOrder(){
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.statisticOrder());
    }
    @GetMapping("/tracking")
    public ResponseEntity trackingOrder(
            @RequestParam(value="tracked_by",required = false) String trackingKeyword
                                        ){
        return ResponseEntity.status(HttpStatus.OK).body(OrderService.tracking(trackingKeyword));
    }
    }


