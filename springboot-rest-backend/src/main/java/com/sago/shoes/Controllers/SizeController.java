package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Size;
import com.sago.shoes.Services.SizeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/sizes")
@Api(value = "Size Resources")
public class SizeController {
    @Autowired
    private SizeService sizeService;
    @GetMapping
    @ApiOperation(value="get all Sizes")
    public List<Size> getAllSizes(){
        return sizeService.getAllSizes();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one Size")
    public Size getSizeById(@PathVariable(value="id")long id){
        return sizeService.getById(id);
    }

    @PostMapping
    @ApiOperation(value="create a new Size")
    public ResponseEntity createSize(@Valid @RequestBody Size size){
        return ResponseEntity.status(HttpStatus.CREATED).body(sizeService.save(size));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a Size")
    public ResponseEntity updateSize(@PathVariable(name = "id") Long id, @Valid @RequestBody Size size){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(sizeService.update(id,size));
    }

    @PatchMapping("/{id}")
    @ApiOperation(value="partial update a Size")
    public ResponseEntity partialUpdateSize(int id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Partial updated" +id);
    }
}
