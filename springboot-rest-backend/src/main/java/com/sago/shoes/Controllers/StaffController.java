package com.sago.shoes.Controllers;

import com.sago.shoes.Payload.AuthDTO.StaffSignUpRequest;
import com.sago.shoes.Services.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("api/staffs")
@RestController
public class StaffController {
    @Autowired
    private StaffService staffService;


    @PostMapping
    @PreAuthorize("hasAuthority('1')")
    public ResponseEntity createStaff(@Valid @RequestBody StaffSignUpRequest staffSignUpRequest)
    {
            if(staffService.existsByUsername(staffSignUpRequest.getUsername()))
                 return ResponseEntity.status(HttpStatus.OK).body("username exists");
            return ResponseEntity.status(HttpStatus.CREATED).body(staffService.createStaff(staffSignUpRequest));
    }


    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('1')")
    public ResponseEntity updateStaff(@PathVariable(name = "id") Long id,@Valid @RequestBody StaffSignUpRequest staffSignUpRequest)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(staffService.updateStaff(id,staffSignUpRequest));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('1')")
    public ResponseEntity getAllStaff()
    {
        return ResponseEntity.status(HttpStatus.OK).body(staffService.getAllStaff());

    }
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('1')")
    public ResponseEntity getStaffById(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.status(HttpStatus.OK).body(staffService.getById(id));

    }


}