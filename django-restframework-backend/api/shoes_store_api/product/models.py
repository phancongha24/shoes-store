import random

from django.db import models
from django.template.defaultfilters import slugify

# Create your models here.

def get_upload_path(instance, filename):
    return f'{instance.name}//{filename}'
class Size(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Brand(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(null=True,unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f"/{self.slug}/"

    def save(self,*args,**kwargs):
        self.slug = slugify(self.name)
        super(Brand, self).save(*args,**kwargs)


class Product(models.Model):
    name = models.CharField(max_length=500)
    slug = models.SlugField(null=True,unique=True)
    brand = models.ForeignKey(
        Brand,
        on_delete=models.CASCADE,
        related_name="products",
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)
    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.name

    def save(self,*args,**kwargs):
        self.slug = slugify(self.name)
        super(Product, self).save(*args,**kwargs)

class ProductVariant(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name="productvariants",
    )
    size = models.ForeignKey(
        Size,
        on_delete=models.CASCADE,
        related_name="products",
    )
    quantity = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        ordering = ('-quantity',)

    def __str__(self):
        return f"{self.product} | {self.size}"

class ProductImage(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name="productimages",
    )

    image = models.ImageField(upload_to='images/', null=True, blank=True)

