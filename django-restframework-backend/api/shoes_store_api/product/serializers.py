from rest_framework import serializers
from rest_framework.parsers import MultiPartParser, FormParser
from .models import Brand, Size, Product, ProductImage, ProductVariant, ProductImage


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = '__all__'


class ProductVariantSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductVariant
        fields = ('id', 'product', 'size', 'quantity', 'updated',)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        # product = Product.objects.get(pk=representation['product'])
        size = Size.objects.get(pk=representation['size'])
        # representation['product'] = product.name
        representation['size'] = size.name
        is_full_format = True if self._context.get('is_full_format') == None else self._context['is_full_format']
        #self._context to get context from a nested serializer
        if is_full_format is False:
            del representation['product']

        return representation

class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    productimages = ProductImageSerializer(many=True, required=False)
    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['brand'] = instance.brand.name
        return representation

class ProductDetailSerializer(serializers.ModelSerializer):
    productvariants = ProductVariantSerializer(many=True, read_only=False,context={'is_full_format':False})

    class Meta:
        model = Product
        fields = '__all__'


# class UserSerializer(serializers.Serializer):
# username = serializers.CharField()
# email = serializers.EmailField(required=False)
# first_name = serializers.CharField(max_length=20,
# required=False)
# last_name = serializers.CharField(max_length=20,
# required=False)
# password = serializers.CharField(write_only=True,
# required=False)
# join_date = serializers.DateTimeField(read_only=True)
# def create(self, validated_data):
# return User(**validated_data)
# def update(self, instance, validated_data):
# for key, value in validated_data.items():
# setattr(instance, key, value)
# return instance