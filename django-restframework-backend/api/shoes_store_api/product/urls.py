from django.urls import path
from .views import BrandList,SizeList,ProductList,ProductVariantList,ProductImageList
from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register('brands',BrandList, basename='brand')
router.register('sizes',SizeList, basename='size')
router.register('products',ProductList, basename='size')
router.register('product-variants',ProductVariantList,basename='order')
router.register('product-images',ProductImageList)
urlpatterns = router.urls