# from django.db import models
# from product.models import Product
# # Create your models here.
# class Supplier(models.Model):
#     name = models.CharField(max_length=255)
#     email = models.CharField(max_length=128)
#     phone = models.CharField(null=True,
#                                     blank=True,
#                                     max_length=128
#                                 )
# class GoodReceipt(models.Model):
#     supplier = models.ForeignKey(Supplier,
#                                  on_delete = models.CASCADE)
#     subtotal = models.DecimalField(max_digits=8, decimal_places=2)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
# class GoodReceiptItem(models.Model):
#     name = models.ForeignKey(Product,
#                              on_delete = models.CASCADE,
#                              related_name='product_items')
#
#     goodReceipt = models.ForeignKey(GoodReceipt,
#                                     on_delete= models.CASCADE,
#                                     related_name='order_items')
#     quantity = models.PositiveIntegerField()
#     subtotal = models.DecimalField(max_digits=8, decimal_places=2)