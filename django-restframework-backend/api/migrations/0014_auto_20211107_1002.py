# Generated by Django 3.2.9 on 2021-11-07 10:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0013_alter_productimage_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='description',
        ),
        migrations.RemoveField(
            model_name='productimage',
            name='default',
        ),
    ]
