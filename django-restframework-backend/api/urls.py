from os import listdir
from os.path import join, isdir

from django.conf.urls import include, url

from shoes_store.settings import BASE_DIR

API_DIR = 'api/shoes_store_api/'
app_name = "api"
entities = [directory
            for directory in listdir(join(BASE_DIR, API_DIR))
            if (isdir(join(BASE_DIR, API_DIR, directory))
                and directory != '__pycache__')]

urlpatterns = [
    url(r'^', include('api.shoes_store_api.{}.urls'.format(entity)))
    for entity in entities
]
print(urlpatterns)