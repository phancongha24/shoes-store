--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.23
-- Dumped by pg_dump version 12.6

-- Started on 2021-12-21 13:51:17 +07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "shoes-storedb";
--
-- TOC entry 2649 (class 1262 OID 16393)
-- Name: shoes-storedb; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "shoes-storedb" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE "shoes-storedb" OWNER TO postgres;

\connect -reuse-previous=on "dbname='shoes-storedb'"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 224 (class 1255 OID 34634)
-- Name: top_5_best_seller_products(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.top_5_best_seller_products() RETURNS TABLE(product_variant_id bigint)
    LANGUAGE plpgsql
    AS $$
begin
SELECT api_order_item.product_variant_id, COUNT(api_order_item.product_variant_id) as T
FROM api_order_item
group by api_order_item.product_variant_id
order by T desc
    limit 5;
end
$$;


ALTER FUNCTION public.top_5_best_seller_products() OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 185 (class 1259 OID 34635)
-- Name: api_brand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_brand (
                                  id bigint NOT NULL,
                                  name character varying(255) NOT NULL,
                                  slug character varying(50),
                                  email character varying(255),
                                  phonenumber character varying(255)
);


ALTER TABLE public.api_brand OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 34641)
-- Name: api_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_brand_id_seq OWNER TO postgres;

--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 186
-- Name: api_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_brand_id_seq OWNED BY public.api_brand.id;


--
-- TOC entry 187 (class 1259 OID 34643)
-- Name: api_color; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_color (
                                  id bigint NOT NULL,
                                  name character varying(255),
                                  slug character varying(255)
);


ALTER TABLE public.api_color OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 34649)
-- Name: api_color_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_color_id_seq OWNER TO postgres;

--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 188
-- Name: api_color_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_color_id_seq OWNED BY public.api_color.id;


--
-- TOC entry 189 (class 1259 OID 34651)
-- Name: api_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_order (
                                  id bigint NOT NULL,
                                  created timestamp without time zone NOT NULL,
                                  updated timestamp without time zone NOT NULL,
                                  price double precision,
                                  address character varying(255),
                                  note character varying(255),
                                  user_id bigint,
                                  is_fullfilled boolean,
                                  payment_method character varying(255),
                                  customer character varying(255),
                                  phone_number character varying(255)
);


ALTER TABLE public.api_order OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 34657)
-- Name: api_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_order_id_seq OWNER TO postgres;

--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 190
-- Name: api_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_order_id_seq OWNED BY public.api_order.id;


--
-- TOC entry 191 (class 1259 OID 34659)
-- Name: api_order_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_order_item (
                                       id bigint NOT NULL,
                                       price double precision,
                                       quantity integer,
                                       order_id bigint,
                                       product_variant_id bigint
);


ALTER TABLE public.api_order_item OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 34662)
-- Name: api_order_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_order_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_order_item_id_seq OWNER TO postgres;

--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 192
-- Name: api_order_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_order_item_id_seq OWNED BY public.api_order_item.id;


--
-- TOC entry 193 (class 1259 OID 34664)
-- Name: api_order_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_order_status (
                                         id bigint NOT NULL,
                                         created timestamp without time zone NOT NULL,
                                         updated timestamp without time zone NOT NULL,
                                         description character varying(255),
                                         name character varying(255)
);


ALTER TABLE public.api_order_status OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 34670)
-- Name: api_order_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_order_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_order_status_id_seq OWNER TO postgres;

--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 194
-- Name: api_order_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_order_status_id_seq OWNED BY public.api_order_status.id;


--
-- TOC entry 195 (class 1259 OID 34672)
-- Name: api_orderhistory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_orderhistory (
                                         id bigint NOT NULL,
                                         reason character varying(255),
                                         order_id bigint,
                                         orderstatus_id bigint,
                                         staff_id bigint,
                                         created timestamp without time zone NOT NULL,
                                         updated timestamp without time zone NOT NULL
);


ALTER TABLE public.api_orderhistory OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 34675)
-- Name: api_orderhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_orderhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_orderhistory_id_seq OWNER TO postgres;

--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 196
-- Name: api_orderhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_orderhistory_id_seq OWNED BY public.api_orderhistory.id;


--
-- TOC entry 197 (class 1259 OID 34677)
-- Name: api_payment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_payment (
                                    id character varying(255) NOT NULL,
                                    created timestamp without time zone NOT NULL,
                                    updated timestamp without time zone NOT NULL,
                                    amount double precision,
                                    bank_code character varying(255),
                                    order_info character varying(255),
                                    pay_date character varying(255),
                                    reason character varying(255),
                                    order_id bigint
);


ALTER TABLE public.api_payment OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 34683)
-- Name: api_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_product (
                                    id bigint NOT NULL,
                                    name character varying(500) NOT NULL,
                                    price numeric(10,2) NOT NULL,
                                    brand_id bigint NOT NULL,
                                    slug character varying(500),
                                    gender character varying(255) DEFAULT 'men'::character varying
);


ALTER TABLE public.api_product OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 34690)
-- Name: api_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_product_id_seq OWNER TO postgres;

--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 199
-- Name: api_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_product_id_seq OWNED BY public.api_product.id;


--
-- TOC entry 200 (class 1259 OID 34692)
-- Name: api_productimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_productimage (
                                         id bigint NOT NULL,
                                         image character varying(500),
                                         product_id bigint NOT NULL
);


ALTER TABLE public.api_productimage OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 34695)
-- Name: api_productimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_productimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_productimage_id_seq OWNER TO postgres;

--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 201
-- Name: api_productimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_productimage_id_seq OWNED BY public.api_productimage.id;


--
-- TOC entry 202 (class 1259 OID 34697)
-- Name: api_productvariant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_productvariant (
                                           id bigint NOT NULL,
                                           quantity integer NOT NULL,
                                           created timestamp with time zone NOT NULL,
                                           updated timestamp with time zone NOT NULL,
                                           product_id bigint NOT NULL,
                                           size_id bigint NOT NULL,
                                           color_id bigint,
                                           CONSTRAINT api_productvariant_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.api_productvariant OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 34701)
-- Name: api_productvariant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_productvariant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_productvariant_id_seq OWNER TO postgres;

--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 203
-- Name: api_productvariant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_productvariant_id_seq OWNED BY public.api_productvariant.id;


--
-- TOC entry 204 (class 1259 OID 34703)
-- Name: api_review; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_review (
                                   id bigint NOT NULL,
                                   created timestamp without time zone NOT NULL,
                                   updated timestamp without time zone NOT NULL,
                                   content character varying(255),
                                   name character varying(255),
                                   rating double precision,
                                   title character varying(255),
                                   product_id bigint
);


ALTER TABLE public.api_review OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 34709)
-- Name: api_review_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_review_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_review_id_seq OWNER TO postgres;

--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 205
-- Name: api_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_review_id_seq OWNED BY public.api_review.id;


--
-- TOC entry 206 (class 1259 OID 34711)
-- Name: api_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_role (
                                 id bigint NOT NULL,
                                 created timestamp without time zone NOT NULL,
                                 updated timestamp without time zone NOT NULL,
                                 tenquyen character varying(255)
);


ALTER TABLE public.api_role OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 34714)
-- Name: api_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_role_id_seq OWNER TO postgres;

--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 207
-- Name: api_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_role_id_seq OWNED BY public.api_role.id;


--
-- TOC entry 208 (class 1259 OID 34716)
-- Name: api_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_size (
                                 id bigint NOT NULL,
                                 name character varying(255) NOT NULL,
                                 slug character varying(255)
);


ALTER TABLE public.api_size OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 34722)
-- Name: api_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_size_id_seq OWNER TO postgres;

--
-- TOC entry 2661 (class 0 OID 0)
-- Dependencies: 209
-- Name: api_size_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_size_id_seq OWNED BY public.api_size.id;


--
-- TOC entry 210 (class 1259 OID 34724)
-- Name: api_staff; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_staff (
                                  id bigint NOT NULL,
                                  created timestamp without time zone NOT NULL,
                                  updated timestamp without time zone NOT NULL,
                                  full_name character varying(255),
                                  password character varying(255),
                                  username character varying(255),
                                  role_id bigint NOT NULL
);


ALTER TABLE public.api_staff OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 34730)
-- Name: api_staff_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_staff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_staff_id_seq OWNER TO postgres;

--
-- TOC entry 2662 (class 0 OID 0)
-- Dependencies: 211
-- Name: api_staff_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_staff_id_seq OWNED BY public.api_staff.id;


--
-- TOC entry 212 (class 1259 OID 34732)
-- Name: api_supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_supplier (
                                     id bigint NOT NULL,
                                     email character varying(255),
                                     name character varying(255),
                                     phonenumber character varying(255)
);


ALTER TABLE public.api_supplier OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 34738)
-- Name: api_supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_supplier_id_seq OWNER TO postgres;

--
-- TOC entry 2663 (class 0 OID 0)
-- Dependencies: 213
-- Name: api_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_supplier_id_seq OWNED BY public.api_supplier.id;


--
-- TOC entry 214 (class 1259 OID 34740)
-- Name: api_supply; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_supply (
                                   id bigint NOT NULL,
                                   created timestamp without time zone NOT NULL,
                                   updated timestamp without time zone NOT NULL,
                                   phonenumber double precision,
                                   staff_id bigint,
                                   supplier_id bigint
);


ALTER TABLE public.api_supply OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 34743)
-- Name: api_supply_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_supply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_supply_id_seq OWNER TO postgres;

--
-- TOC entry 2664 (class 0 OID 0)
-- Dependencies: 215
-- Name: api_supply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_supply_id_seq OWNED BY public.api_supply.id;


--
-- TOC entry 216 (class 1259 OID 34745)
-- Name: api_supply_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_supply_item (
                                        id bigint NOT NULL,
                                        price double precision,
                                        quantity integer,
                                        product_variant_id bigint,
                                        supply_id bigint
);


ALTER TABLE public.api_supply_item OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 34748)
-- Name: api_supply_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_supply_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_supply_item_id_seq OWNER TO postgres;

--
-- TOC entry 2665 (class 0 OID 0)
-- Dependencies: 217
-- Name: api_supply_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_supply_item_id_seq OWNED BY public.api_supply_item.id;


--
-- TOC entry 221 (class 1259 OID 34935)
-- Name: api_used_voucher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_used_voucher (
                                         id bigint NOT NULL,
                                         created timestamp without time zone NOT NULL,
                                         updated timestamp without time zone NOT NULL,
                                         user_id bigint,
                                         voucher_id bigint
);


ALTER TABLE public.api_used_voucher OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 34933)
-- Name: api_used_voucher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_used_voucher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_used_voucher_id_seq OWNER TO postgres;

--
-- TOC entry 2666 (class 0 OID 0)
-- Dependencies: 220
-- Name: api_used_voucher_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_used_voucher_id_seq OWNED BY public.api_used_voucher.id;


--
-- TOC entry 218 (class 1259 OID 34750)
-- Name: api_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_user (
                                 id bigint NOT NULL,
                                 created timestamp without time zone NOT NULL,
                                 updated timestamp without time zone NOT NULL,
                                 email character varying(255),
                                 fullname character varying(255),
                                 password character varying(255),
                                 phonenumber character varying(255)
);


ALTER TABLE public.api_user OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 34756)
-- Name: api_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_user_id_seq OWNER TO postgres;

--
-- TOC entry 2667 (class 0 OID 0)
-- Dependencies: 219
-- Name: api_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_user_id_seq OWNED BY public.api_user.id;


--
-- TOC entry 223 (class 1259 OID 34943)
-- Name: api_voucher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.api_voucher (
                                    id bigint NOT NULL,
                                    created timestamp without time zone NOT NULL,
                                    updated timestamp without time zone NOT NULL,
                                    code character varying(255),
                                    description character varying(255),
                                    discount_amount double precision,
                                    expired_date timestamp without time zone,
                                    name character varying(255),
                                    quantity integer,
                                    max_discount_amount double precision,
                                    total integer
);


ALTER TABLE public.api_voucher OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 34941)
-- Name: api_voucher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.api_voucher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_voucher_id_seq OWNER TO postgres;

--
-- TOC entry 2668 (class 0 OID 0)
-- Dependencies: 222
-- Name: api_voucher_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.api_voucher_id_seq OWNED BY public.api_voucher.id;


--
-- TOC entry 2393 (class 2604 OID 34758)
-- Name: api_brand id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_brand ALTER COLUMN id SET DEFAULT nextval('public.api_brand_id_seq'::regclass);


--
-- TOC entry 2394 (class 2604 OID 34759)
-- Name: api_color id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_color ALTER COLUMN id SET DEFAULT nextval('public.api_color_id_seq'::regclass);


--
-- TOC entry 2395 (class 2604 OID 34760)
-- Name: api_order id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order ALTER COLUMN id SET DEFAULT nextval('public.api_order_id_seq'::regclass);


--
-- TOC entry 2396 (class 2604 OID 34761)
-- Name: api_order_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_item ALTER COLUMN id SET DEFAULT nextval('public.api_order_item_id_seq'::regclass);


--
-- TOC entry 2397 (class 2604 OID 34762)
-- Name: api_order_status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_status ALTER COLUMN id SET DEFAULT nextval('public.api_order_status_id_seq'::regclass);


--
-- TOC entry 2398 (class 2604 OID 34763)
-- Name: api_orderhistory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_orderhistory ALTER COLUMN id SET DEFAULT nextval('public.api_orderhistory_id_seq'::regclass);


--
-- TOC entry 2400 (class 2604 OID 34764)
-- Name: api_product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_product ALTER COLUMN id SET DEFAULT nextval('public.api_product_id_seq'::regclass);


--
-- TOC entry 2401 (class 2604 OID 34765)
-- Name: api_productimage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productimage ALTER COLUMN id SET DEFAULT nextval('public.api_productimage_id_seq'::regclass);


--
-- TOC entry 2402 (class 2604 OID 34766)
-- Name: api_productvariant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productvariant ALTER COLUMN id SET DEFAULT nextval('public.api_productvariant_id_seq'::regclass);


--
-- TOC entry 2404 (class 2604 OID 34767)
-- Name: api_review id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_review ALTER COLUMN id SET DEFAULT nextval('public.api_review_id_seq'::regclass);


--
-- TOC entry 2405 (class 2604 OID 34768)
-- Name: api_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_role ALTER COLUMN id SET DEFAULT nextval('public.api_role_id_seq'::regclass);


--
-- TOC entry 2406 (class 2604 OID 34769)
-- Name: api_size id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_size ALTER COLUMN id SET DEFAULT nextval('public.api_size_id_seq'::regclass);


--
-- TOC entry 2407 (class 2604 OID 34770)
-- Name: api_staff id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_staff ALTER COLUMN id SET DEFAULT nextval('public.api_staff_id_seq'::regclass);


--
-- TOC entry 2408 (class 2604 OID 34771)
-- Name: api_supplier id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supplier ALTER COLUMN id SET DEFAULT nextval('public.api_supplier_id_seq'::regclass);


--
-- TOC entry 2409 (class 2604 OID 34772)
-- Name: api_supply id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply ALTER COLUMN id SET DEFAULT nextval('public.api_supply_id_seq'::regclass);


--
-- TOC entry 2410 (class 2604 OID 34773)
-- Name: api_supply_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply_item ALTER COLUMN id SET DEFAULT nextval('public.api_supply_item_id_seq'::regclass);


--
-- TOC entry 2412 (class 2604 OID 34938)
-- Name: api_used_voucher id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_used_voucher ALTER COLUMN id SET DEFAULT nextval('public.api_used_voucher_id_seq'::regclass);


--
-- TOC entry 2411 (class 2604 OID 34774)
-- Name: api_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_user ALTER COLUMN id SET DEFAULT nextval('public.api_user_id_seq'::regclass);


--
-- TOC entry 2413 (class 2604 OID 34946)
-- Name: api_voucher id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_voucher ALTER COLUMN id SET DEFAULT nextval('public.api_voucher_id_seq'::regclass);


--
-- TOC entry 2605 (class 0 OID 34635)
-- Dependencies: 185
-- Data for Name: api_brand; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (7, 'Alexander MCQueen', 'alexander-mcqueen', 'supplier@mcqueen.com', '84123456789');
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (6, 'Converse', 'converse', 'supplier@converse.com', '84123456789');
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (1, 'Nike', 'nike', 'nike@gmail.com', '84123456789');
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (2, 'Adidas', 'adidas', 'nike@gmail.com', '84123456789');
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (8, 'Bata', 'bata', NULL, NULL);
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (4, 'Puma', 'puma', 'supplier@puma.com', '84123456789');
INSERT INTO public.api_brand (id, name, slug, email, phonenumber) VALUES (3, 'Other', 'other', 'nike@gmail.com', '84123456789');


--
-- TOC entry 2607 (class 0 OID 34643)
-- Dependencies: 187
-- Data for Name: api_color; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_color (id, name, slug) VALUES (1, 'Red', 'red');
INSERT INTO public.api_color (id, name, slug) VALUES (2, 'Green', 'green');
INSERT INTO public.api_color (id, name, slug) VALUES (3, 'Yellow', 'yellow');
INSERT INTO public.api_color (id, name, slug) VALUES (4, 'Blue', 'blue');
INSERT INTO public.api_color (id, name, slug) VALUES (5, 'Gray', 'gray');
INSERT INTO public.api_color (id, name, slug) VALUES (6, 'Pink', 'pink');
INSERT INTO public.api_color (id, name, slug) VALUES (7, 'Cyan', 'cyan');
INSERT INTO public.api_color (id, name, slug) VALUES (8, 'Purple', 'purple');
INSERT INTO public.api_color (id, name, slug) VALUES (9, 'Fuchsia', 'fuchsia');
INSERT INTO public.api_color (id, name, slug) VALUES (10, 'Aero', 'aero');


--
-- TOC entry 2609 (class 0 OID 34651)
-- Dependencies: 189
-- Data for Name: api_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (54, '2021-11-28 15:35:40.395', '2021-11-28 15:36:03.644', 60480000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Thanh toán online nha', 3, true, 'online', 'Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (56, '2021-11-29 10:12:20.265', '2021-11-29 10:12:20.265', 40320000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Mình cần hàng gấp', 3, false, 'online', 'Sơn', '0929315516');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (55, '2021-11-28 15:36:51.582', '2021-11-28 15:37:09.056', 80640000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Mình cần hàng gấp', 3, true, 'online', 'Hà', '0929315515');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (53, '2021-11-28 15:19:08.821', '2021-11-28 15:32:57.907', 13440000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Thanh toán online nha', 3, true, 'online', 'Hà', '0929315517');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (57, '2021-12-10 19:09:34.173', '2021-12-10 19:10:27.135', 72000000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Chiều khách nè', 2, true, 'online', 'Anh Sơn', '0916735645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (58, '2021-12-10 19:17:16.896', '2021-12-10 19:17:48.55', 24000000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Mua tiếp, order thiếu !!', 2, true, 'online', 'Anh Sơn', '0916735645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (59, '2021-12-10 19:19:32.263', '2021-12-10 19:19:32.263', 24000000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 2, false, 'COD', 'Anh Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (62, '2021-12-11 17:57:56.371', '2021-12-11 17:57:56.371', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (63, '2021-12-11 17:58:53.541', '2021-12-11 17:58:53.541', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (65, '2021-12-11 18:00:14.205', '2021-12-11 18:00:14.205', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, false, '1', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (67, '2021-12-11 18:04:43.465', '2021-12-11 18:04:43.465', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, false, '1', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (68, '2021-12-11 18:27:32.126', '2021-12-11 18:27:32.126', 48000000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'Giao nhanh giup em nha', 3, false, '2', 'Cong Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (70, '2021-12-12 19:55:48.852', '2021-12-12 19:55:48.852', 16674000, '273 an duong vuong, Quận 1, Thành phố Hồ Chí Minh', 'hello', 3, false, 'cod', 'Cong Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (71, '2021-12-12 19:58:32.412', '2021-12-12 20:00:02.317', 16674000, '273 an duong vuong, Quận 1, Thành phố Hồ Chí Minh', 'hello', 3, true, 'online', 'Cong Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (72, '2021-12-13 06:30:33.833', '2021-12-13 06:30:33.833', 7623000, '101 Ho Tung Mau, Huyện Di Linh, Tỉnh Lâm Đồng', 'Giao nhanh giup em nha', 3, false, 'cod', 'Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (73, '2021-12-13 06:48:18.229', '2021-12-13 06:48:42.158', 8127000, '101 ho tung mau, Thành phố Đà Lạt, Tỉnh Lâm Đồng', 'giao hang nhanh giup em', 3, true, 'online', 'Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (74, '2021-12-13 07:59:04.212', '2021-12-13 07:59:40.599', 8379000, '273 an duong vuong, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu', 3, true, 'online', 'Son Cong', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (75, '2021-12-13 08:08:59.686', '2021-12-13 08:09:23.942', 8127000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'hello', 3, true, 'online', 'Phan Công Sơn', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (76, '2021-12-13 13:30:51.909', '2021-12-13 13:30:51.909', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (77, '2021-12-13 13:30:54.742', '2021-12-13 13:30:54.742', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (78, '2021-12-13 13:30:55.971', '2021-12-13 13:30:55.971', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (79, '2021-12-13 13:30:56.169', '2021-12-13 13:30:56.169', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (80, '2021-12-13 13:30:56.405', '2021-12-13 13:30:56.405', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (81, '2021-12-13 13:30:56.442', '2021-12-13 13:30:56.442', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (82, '2021-12-13 13:30:56.749', '2021-12-13 13:30:56.749', 8379000, '273 An Dương Vương, Quận Bình Tân, Thành phố Hồ Chí Minh', '', 3, false, 'online', 'cong son ha', '0978121247');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (83, '2021-12-13 13:33:03.818', '2021-12-13 13:33:03.818', 8379000, '273 An Dương Vương, Quận 1, Thành phố Hồ Chí Minh', 'aa', 3, false, 'cod', 'cong son ha', '0987654321');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (84, '2021-12-15 06:23:41.287', '2021-12-15 06:23:41.287', 2457000, '120 cong chua ngoc han, Quận 1, Thành phố Hồ Chí Minh', 'giao nhanh giup em', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (85, '2021-12-15 06:32:27.587', '2021-12-15 06:32:27.587', 2457000, '273 an duong vuong, Quận 1, Thành phố Hồ Chí Minh', 'day la ghi chu', 3, false, 'cod', 'Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (86, '2021-12-15 06:32:53.276', '2021-12-15 06:32:53.276', 2457000, '273 an duogn vuong, Quận 1, Thành phố Hồ Chí Minh', 'day la ghi chu', 3, false, 'cod', 'Son Cong', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (87, '2021-12-15 06:34:39.146', '2021-12-15 06:36:46.127', 2457000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'giao cham cham', 3, true, 'online', 'On Tuan Huy', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (88, '2021-12-15 06:38:08.453', '2021-12-15 06:38:23.4', 2457000, '271 an duong vuong, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu ne', 4, true, 'online', 'Ho Sy Dat', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (89, '2021-12-15 06:54:07.853', '2021-12-15 06:54:35.486', 10584000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'giao hang 15/12', 3, true, 'online', 'Son', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (64, '2021-12-11 17:59:33.147', '2021-12-15 07:03:57.544', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, true, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (90, '2021-12-15 07:05:23.495', '2021-12-15 07:05:23.495', 648648000, 'Ngã Ba Đồn Canh Nậu Yên Thế Bắc Giang Việt Nam, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu', 3, false, 'cod', 'Sơn Phan Công', '0929315645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (91, '2021-12-15 16:07:35.357', '2021-12-15 16:07:35.357', 420000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'day la ghi chu', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (69, '2021-12-11 18:32:00.089', '2021-12-16 07:52:22.744', 48000000, '120 Ho tung mau, Quận 10, Thành phố Hồ Chí Minh', 'mua choi thoi', 3, true, '2', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (92, '2021-12-16 16:52:20.132', '2021-12-16 16:52:20.273', 18750000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 2, false, 'COD', 'Anh Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (94, '2021-12-16 17:03:22.73', '2021-12-16 17:03:22.774', 18650000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 2, false, 'COD', 'Anh Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (98, '2021-12-16 17:11:00.208', '2021-12-16 17:11:00.225', 7827000, '273 an, Quận 1, Thành phố Hồ Chí Minh', 'giao nhanh', 1, false, 'cod', 'Son Cong', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (66, '2021-12-11 18:03:18.108', '2021-12-19 15:00:45.498', 48000000, '273 An Duong Vuong, Quận 1, Thành phố Hồ Chí Minh', 'Giao cham giup em', 3, true, '1', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (61, '2021-12-11 13:13:50.296', '2021-12-19 15:06:40.383', 18900000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 2, true, 'COD', 'Anh Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (60, '2021-12-11 13:13:36.995', '2021-12-19 15:07:46.966', 12600000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 2, true, 'COD', 'Anh Nam', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (102, '2021-12-17 11:42:51.351', '2021-12-17 11:42:51.402', 18650000, '273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh', 'Giày gì mà rẻ z', 1, false, 'COD', 'Anh 2', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (103, '2021-12-17 17:35:09.418', '2021-12-17 17:36:07.503', 3422000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'Em co ma giam gia ne anh', 1, true, 'online', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (105, '2021-12-17 17:50:04.743', '2021-12-17 17:50:04.743', 8337000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'khong co ma giam gia roi huhu', 1, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (106, '2021-12-17 17:52:41.112', '2021-12-17 17:52:41.145', 7937000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'co ma giam gia ne hihi', 3, false, 'cod', 'Nam Nguyen', '0929315514');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (129, '2021-12-17 18:08:27.192', '2021-12-17 18:08:27.192', 2500000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (131, '2021-12-17 18:09:27.483', '2021-12-17 18:09:27.483', 2500000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu', 3, false, 'cod', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (133, '2021-12-17 18:21:24.495', '2021-12-17 18:21:40.688', 462000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'ghi chu', 3, true, 'online', 'Son Cong', '0916375645');
INSERT INTO public.api_order (id, created, updated, price, address, note, user_id, is_fullfilled, payment_method, customer, phone_number) VALUES (134, '2021-12-20 07:07:09.944', '2021-12-20 07:07:10.006', 62000, '120 Ho Tung Mau, Quận 1, Thành phố Hồ Chí Minh', 'khuye nami', 3, false, 'cod', 'Son Cong', '0916375645');


--
-- TOC entry 2611 (class 0 OID 34659)
-- Dependencies: 191
-- Data for Name: api_order_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (99, 6720000, 1, 53, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (100, 6720000, 1, 53, 7);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (101, 6720000, 4, 54, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (102, 6720000, 5, 54, 7);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (103, 6720000, 6, 55, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (104, 6720000, 6, 55, 7);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (105, 6720000, 6, 56, 5);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (106, 12000000, 6, 57, 12);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (107, 12000000, 2, 58, 12);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (108, 12000000, 2, 59, 12);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (109, 6300000, 2, 60, 11);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (110, 6300000, 3, 61, 10);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (111, 12000000, 4, 62, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (112, 12000000, 4, 63, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (113, 12000000, 4, 64, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (114, 12000000, 4, 65, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (115, 12000000, 4, 66, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (116, 12000000, 4, 67, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (117, 12000000, 4, 68, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (118, 12000000, 4, 69, 6);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (119, 8337000, 2, 70, 16);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (120, 8337000, 2, 71, 16);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (121, 7623000, 1, 72, 26);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (122, 8127000, 1, 73, 18);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (123, 8379000, 1, 74, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (124, 8127000, 1, 75, 20);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (125, 8379000, 1, 76, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (126, 8379000, 1, 77, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (127, 8379000, 1, 78, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (128, 8379000, 1, 79, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (129, 8379000, 1, 81, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (130, 8379000, 1, 82, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (131, 8379000, 1, 80, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (132, 8379000, 1, 83, 13);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (133, 2457000, 1, 84, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (134, 2457000, 1, 85, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (135, 2457000, 1, 86, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (136, 2457000, 1, 87, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (137, 2457000, 1, 88, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (138, 2457000, 1, 89, 104);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (139, 8127000, 1, 89, 17);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (140, 6552000, 99, 90, 141);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (141, 420000, 1, 91, 200);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (142, 6300000, 3, 92, 10);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (144, 6300000, 3, 94, 10);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (148, 8127000, 1, 98, 205);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (152, 6300000, 3, 102, 10);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (153, 462000, 1, 103, 199);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (154, 3360000, 1, 103, 175);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (156, 8337000, 1, 105, 16);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (157, 8337000, 1, 106, 16);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (178, 2500000, 1, 129, 4);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (180, 2500000, 1, 131, 4);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (182, 462000, 1, 133, 199);
INSERT INTO public.api_order_item (id, price, quantity, order_id, product_variant_id) VALUES (183, 462000, 1, 134, 122);


--
-- TOC entry 2613 (class 0 OID 34664)
-- Dependencies: 193
-- Data for Name: api_order_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_order_status (id, created, updated, description, name) VALUES (1, '2021-11-18 04:58:48.463', '2021-11-18 04:58:48.463', 'Customer started the checkout process but did not complete it ', 'Pending');
INSERT INTO public.api_order_status (id, created, updated, description, name) VALUES (2, '2021-11-18 04:59:52.269', '2021-11-18 04:59:52.269', 'Order has been pulled and packaged and is awaiting collection from a shipping provider', 'Awaiting Shipment');
INSERT INTO public.api_order_status (id, created, updated, description, name) VALUES (3, '2021-11-18 05:06:32.839', '2021-11-18 05:06:32.839', 'Order has been shipped', 'Shipping');
INSERT INTO public.api_order_status (id, created, updated, description, name) VALUES (5, '2021-11-18 05:09:26.763', '2021-11-18 05:09:26.763', 'Order has been shipped', 'Completed');
INSERT INTO public.api_order_status (id, created, updated, description, name) VALUES (4, '2021-11-18 05:08:18.397', '2021-11-18 05:08:18.397', 'Customer has cancelled the order', 'Cancelled');


--
-- TOC entry 2615 (class 0 OID 34672)
-- Dependencies: 195
-- Data for Name: api_orderhistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (88, 'Automatically generated by System', 53, 1, 1, '2021-11-28 15:19:08.85', '2021-11-28 15:19:08.85');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (89, 'Giao dịch thành công', 53, 2, 1, '2021-11-28 15:32:57.89', '2021-11-28 15:32:57.89');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (90, 'Automatically generated by System', 54, 1, 1, '2021-11-28 15:35:40.416', '2021-11-28 15:35:40.416');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (91, 'Giao dịch thành công', 54, 2, 1, '2021-11-28 15:36:03.641', '2021-11-28 15:36:03.641');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (92, 'Automatically generated by System', 55, 1, 1, '2021-11-28 15:36:51.585', '2021-11-28 15:36:51.585');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (93, 'Giao dịch thành công', 55, 2, 1, '2021-11-28 15:37:09.052', '2021-11-28 15:37:09.052');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (94, 'Automatically generated by System', 56, 1, 1, '2021-11-29 10:12:20.346', '2021-11-29 10:12:20.346');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (95, 'Giao dịch không thành công do: Khách hàng hủy giao dịch', 56, 4, 1, '2021-11-29 10:13:17.425', '2021-11-29 10:13:17.425');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (96, 'Automatically generated by System', 57, 1, 1, '2021-12-10 19:09:34.686', '2021-12-10 19:09:34.686');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (97, 'Giao dịch thành công', 57, 2, 1, '2021-12-10 19:10:26.888', '2021-12-10 19:10:26.888');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (98, 'Đang được giao', 57, 3, 2, '2021-12-10 19:16:28.719', '2021-12-10 19:16:28.719');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (99, 'Automatically generated by System', 58, 1, 1, '2021-12-10 19:17:17.382', '2021-12-10 19:17:17.382');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (100, 'Giao dịch thành công', 58, 2, 1, '2021-12-10 19:17:48.302', '2021-12-10 19:17:48.302');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (101, 'Đang được giao', 58, 3, 2, '2021-12-10 19:18:12.376', '2021-12-10 19:18:12.376');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (102, 'Đơn hàng hoàn tất', 58, 5, 2, '2021-12-10 19:18:32.719', '2021-12-10 19:18:32.719');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (103, 'Automatically generated by System', 59, 1, 1, '2021-12-10 19:19:32.755', '2021-12-10 19:19:32.755');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (104, 'test change', 54, 3, 1, '2021-12-10 20:05:42.476', '2021-12-10 20:05:42.476');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (105, 'chuyen hang', 55, 3, 1, '2021-12-10 20:10:14.336', '2021-12-10 20:10:14.336');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (106, 'chuyen hang', 53, 3, 1, '2021-12-10 20:11:48.772', '2021-12-10 20:11:48.772');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (107, 'Thanh toan thanh cong', 59, 2, 1, '2021-12-10 20:14:39.487', '2021-12-10 20:14:39.487');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (108, 'bi bom hang', 59, 4, 1, '2021-12-10 20:23:15.971', '2021-12-10 20:23:15.971');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (109, 'Đang giao hàng', 54, 4, 1, '2021-12-10 21:21:38.048', '2021-12-10 21:21:38.048');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (110, 'da nhan duoc hang', 57, 5, 1, '2021-12-10 22:53:46.817', '2021-12-10 22:53:46.817');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (111, 'Giao thành công', 55, 5, 1, '2021-12-11 00:04:54.529', '2021-12-11 00:04:54.529');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (112, 'đã thanh toán thành công', 53, 5, 1, '2021-12-11 12:54:28.593', '2021-12-11 12:54:28.593');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (113, 'Automatically generated by System', 60, 1, 1, '2021-12-11 13:13:37.523', '2021-12-11 13:13:37.523');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (114, 'Automatically generated by System', 61, 1, 1, '2021-12-11 13:13:50.788', '2021-12-11 13:13:50.788');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (115, 'Đang chờ giao hàng', 61, 2, 2, '2021-12-11 13:14:41.238', '2021-12-11 13:14:41.238');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (116, 'Đang được giao', 61, 3, 2, '2021-12-11 13:15:25.227', '2021-12-11 13:15:25.227');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (117, 'Automatically generated by System', 62, 1, 1, '2021-12-11 17:57:56.904', '2021-12-11 17:57:56.904');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (118, 'Automatically generated by System', 63, 1, 1, '2021-12-11 17:58:54.035', '2021-12-11 17:58:54.035');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (119, 'Automatically generated by System', 64, 1, 1, '2021-12-11 17:59:33.634', '2021-12-11 17:59:33.634');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (120, 'Automatically generated by System', 65, 1, 1, '2021-12-11 18:00:14.697', '2021-12-11 18:00:14.697');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (121, 'Automatically generated by System', 66, 1, 1, '2021-12-11 18:03:18.705', '2021-12-11 18:03:18.705');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (122, 'Automatically generated by System', 67, 1, 1, '2021-12-11 18:04:43.958', '2021-12-11 18:04:43.958');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (123, 'Automatically generated by System', 68, 1, 1, '2021-12-11 18:27:32.616', '2021-12-11 18:27:32.616');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (124, 'Automatically generated by System', 69, 1, 1, '2021-12-11 18:32:00.579', '2021-12-11 18:32:00.579');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (125, 'pun` nen huy don by phuongnam2810@gmail.com', 62, 4, 1, '2021-12-12 18:10:28.788', '2021-12-12 18:10:28.788');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (126, 'chu'' pe'' dan^` by phuongnam2810@gmail.com', 63, 4, 1, '2021-12-12 18:12:39.572', '2021-12-12 18:12:39.572');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (127, 'tkjk tkj huy~ tkuj by phuongnam2810@gmail.com', 67, 4, 1, '2021-12-12 18:14:42.275', '2021-12-12 18:14:42.275');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (128, 'Automatically generated by System', 70, 1, 1, '2021-12-12 19:55:49.441', '2021-12-12 19:55:49.441');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (129, 'Automatically generated by System', 71, 1, 1, '2021-12-12 19:58:32.951', '2021-12-12 19:58:32.951');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (130, 'Giao dịch thành công', 71, 2, 1, '2021-12-12 20:00:02.065', '2021-12-12 20:00:02.065');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (131, 'Automatically generated by System', 72, 1, 1, '2021-12-13 06:30:34.457', '2021-12-13 06:30:34.457');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (132, 'Automatically generated by System', 73, 1, 1, '2021-12-13 06:48:18.743', '2021-12-13 06:48:18.743');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (133, 'Giao dịch thành công', 73, 2, 1, '2021-12-13 06:48:41.898', '2021-12-13 06:48:41.898');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (134, 'chuyen trang thai awaiting shipment', 72, 2, 1, '2021-12-13 06:53:02.393', '2021-12-13 06:53:02.393');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (135, 'Automatically generated by System', 74, 1, 1, '2021-12-13 07:59:04.746', '2021-12-13 07:59:04.746');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (136, 'Giao dịch thành công', 74, 2, 1, '2021-12-13 07:59:40.357', '2021-12-13 07:59:40.357');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (137, 'Automatically generated by System', 75, 1, 1, '2021-12-13 08:09:00.28', '2021-12-13 08:09:00.28');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (138, 'Giao dịch thành công', 75, 2, 1, '2021-12-13 08:09:23.696', '2021-12-13 08:09:23.696');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (139, 'Giao dịch thành công', 73, 2, 1, '2021-12-13 08:20:08.177', '2021-12-13 08:20:08.177');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (140, 'chuan bi hang xong roi nek', 74, 3, 1, '2021-12-13 08:30:37.935', '2021-12-13 08:30:37.935');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (141, 'Automatically generated by System', 76, 1, 1, '2021-12-13 13:30:52.589', '2021-12-13 13:30:52.589');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (142, 'Automatically generated by System', 77, 1, 1, '2021-12-13 13:30:56.391', '2021-12-13 13:30:56.391');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (143, 'Automatically generated by System', 78, 1, 1, '2021-12-13 13:30:56.604', '2021-12-13 13:30:56.604');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (144, 'Automatically generated by System', 79, 1, 1, '2021-12-13 13:30:56.783', '2021-12-13 13:30:56.783');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (145, 'Automatically generated by System', 82, 1, 1, '2021-12-13 13:30:57.366', '2021-12-13 13:30:57.366');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (146, 'Automatically generated by System', 80, 1, 1, '2021-12-13 13:30:57.59', '2021-12-13 13:30:57.59');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (147, 'Automatically generated by System', 81, 1, 1, '2021-12-13 13:30:58.087', '2021-12-13 13:30:58.087');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (148, 'Automatically generated by System', 83, 1, 1, '2021-12-13 13:33:04.952', '2021-12-13 13:33:04.952');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (149, 'Automatically generated by System', 84, 1, 1, '2021-12-15 06:23:41.305', '2021-12-15 06:23:41.305');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (150, 'Da nhan hang', 64, 2, 1, '2021-12-15 06:25:54.993', '2021-12-15 06:25:54.993');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (151, 'da nhan hang', 65, 2, 1, '2021-12-15 06:26:30.516', '2021-12-15 06:26:30.516');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (152, 'dang xu ly', 66, 2, 1, '2021-12-15 06:28:03.679', '2021-12-15 06:28:03.679');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (153, 'dang xu ly', 68, 2, 1, '2021-12-15 06:28:55.061', '2021-12-15 06:28:55.061');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (154, 'dang xu', 69, 2, 1, '2021-12-15 06:29:47.525', '2021-12-15 06:29:47.525');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (155, 'dang xu ly', 70, 2, 1, '2021-12-15 06:30:31.825', '2021-12-15 06:30:31.825');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (156, 'dang xu ly', 70, 3, 1, '2021-12-15 06:30:36.709', '2021-12-15 06:30:36.709');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (157, 'dang xu ly', 70, 5, 1, '2021-12-15 06:30:41.198', '2021-12-15 06:30:41.198');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (158, 'Automatically generated by System', 85, 1, 1, '2021-12-15 06:32:27.735', '2021-12-15 06:32:27.735');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (159, 'Automatically generated by System', 86, 1, 1, '2021-12-15 06:32:53.282', '2021-12-15 06:32:53.282');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (160, 'Automatically generated by System', 87, 1, 1, '2021-12-15 06:34:39.154', '2021-12-15 06:34:39.154');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (161, 'Giao dịch thành công', 87, 2, 1, '2021-12-15 06:36:46.125', '2021-12-15 06:36:46.125');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (162, 'Automatically generated by System', 88, 1, 1, '2021-12-15 06:38:08.479', '2021-12-15 06:38:08.479');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (163, 'Giao dịch thành công', 88, 2, 1, '2021-12-15 06:38:23.398', '2021-12-15 06:38:23.398');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (164, 'Automatically generated by System', 89, 1, 1, '2021-12-15 06:54:07.866', '2021-12-15 06:54:07.866');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (165, 'Giao dịch thành công', 89, 2, 1, '2021-12-15 06:54:35.484', '2021-12-15 06:54:35.484');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (166, 'dang xu ly', 89, 3, 1, '2021-12-15 06:56:07.197', '2021-12-15 06:56:07.197');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (167, 'dang xu ly', 64, 3, 7, '2021-12-15 07:03:54.618', '2021-12-15 07:03:54.618');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (168, 'dang xu ly', 64, 5, 7, '2021-12-15 07:03:57.531', '2021-12-15 07:03:57.531');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (169, 'Automatically generated by System', 90, 1, 1, '2021-12-15 07:05:23.502', '2021-12-15 07:05:23.502');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (170, 'dang xu ly', 90, 2, 7, '2021-12-15 07:05:39.157', '2021-12-15 07:05:39.157');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (171, 'Automatically generated by System', 91, 1, 1, '2021-12-15 16:07:35.421', '2021-12-15 16:07:35.421');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (172, 'dang xu ly', 69, 3, 1, '2021-12-16 07:52:19.284', '2021-12-16 07:52:19.284');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (173, 'dang xu ly', 69, 5, 1, '2021-12-16 07:52:22.731', '2021-12-16 07:52:22.731');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (174, 'duyet don', 60, 2, 1, '2021-12-16 07:54:31.321', '2021-12-16 07:54:31.321');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (175, 'dang xu ly', 87, 3, 1, '2021-12-16 07:58:34.545', '2021-12-16 07:58:34.545');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (176, 'dang xu ly', 87, 5, 1, '2021-12-16 07:58:37.632', '2021-12-16 07:58:37.632');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (177, 'Khach huy hang', 90, 4, 2, '2021-12-16 08:05:30.839', '2021-12-16 08:05:30.839');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (178, 'Thich thi cancel by phuongnam2810@gmail.com', 91, 4, 1, '2021-12-16 11:56:38.93', '2021-12-16 11:56:38.93');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (179, 'Automatically generated by System', 92, 1, 1, '2021-12-16 16:52:20.223', '2021-12-16 16:52:20.223');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (181, 'Automatically generated by System', 94, 1, 1, '2021-12-16 17:03:22.735', '2021-12-16 17:03:22.735');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (185, 'Automatically generated by System', 98, 1, 1, '2021-12-16 17:11:00.216', '2021-12-16 17:11:00.216');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (189, 'Automatically generated by System', 102, 1, 1, '2021-12-17 11:42:51.357', '2021-12-17 11:42:51.357');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (190, 'Automatically generated by System', 103, 1, 1, '2021-12-17 17:35:09.465', '2021-12-17 17:35:09.465');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (191, 'Giao dịch thành công', 103, 2, 1, '2021-12-17 17:36:07.501', '2021-12-17 17:36:07.501');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (193, 'Automatically generated by System', 105, 1, 1, '2021-12-17 17:50:04.751', '2021-12-17 17:50:04.751');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (194, 'Automatically generated by System', 106, 1, 1, '2021-12-17 17:52:41.119', '2021-12-17 17:52:41.119');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (217, 'Automatically generated by System', 129, 1, 1, '2021-12-17 18:08:27.198', '2021-12-17 18:08:27.198');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (219, 'Automatically generated by System', 131, 1, 1, '2021-12-17 18:09:27.49', '2021-12-17 18:09:27.49');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (220, 'het xien^` goi` by phuongnam2810@gmail.com', 131, 4, 1, '2021-12-17 18:17:37.02', '2021-12-17 18:17:37.02');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (221, 'a by phuongnam2810@gmail.com', 129, 4, 1, '2021-12-17 18:19:42.14', '2021-12-17 18:19:42.14');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (222, 'b by phuongnam2810@gmail.com', 106, 4, 1, '2021-12-17 18:20:39.565', '2021-12-17 18:20:39.565');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (224, 'Automatically generated by System', 133, 1, 1, '2021-12-17 18:21:24.503', '2021-12-17 18:21:24.503');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (225, 'Giao dịch thành công', 133, 2, 1, '2021-12-17 18:21:40.686', '2021-12-17 18:21:40.686');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (226, 'Khach chua thanh toan', 68, 4, 4, '2021-12-19 15:00:22.516', '2021-12-19 15:00:22.516');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (227, 'test', 66, 3, 4, '2021-12-19 15:00:31.899', '2021-12-19 15:00:31.899');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (228, 'chuyen complete', 66, 5, 4, '2021-12-19 15:00:45.479', '2021-12-19 15:00:45.479');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (229, 'Hàng đã nhận được.', 61, 5, 4, '2021-12-19 15:06:40.359', '2021-12-19 15:06:40.359');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (230, 'Hàng đang được giao', 60, 3, 4, '2021-12-19 15:07:40.208', '2021-12-19 15:07:40.208');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (231, 'Giao hàng thành công', 60, 5, 4, '2021-12-19 15:07:46.949', '2021-12-19 15:07:46.949');
INSERT INTO public.api_orderhistory (id, reason, order_id, orderstatus_id, staff_id, created, updated) VALUES (232, 'Automatically generated by System', 134, 1, 1, '2021-12-20 07:07:09.983', '2021-12-20 07:07:09.983');


--
-- TOC entry 2617 (class 0 OID 34677)
-- Dependencies: 197
-- Data for Name: api_payment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13641622', '2021-11-28 15:32:57.676', '2021-11-28 15:32:57.676', 13440000, 'NCB', 'Thanh toan hoa don53SAGO SHOES', 'Thanh toan hoa don53SAGO SHOES', 'Giao dịch thành công', 53);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13641628', '2021-11-28 15:36:03.604', '2021-11-28 15:36:03.604', 60480000, 'NCB', '[#54]Thanh toan don hang SAGO SHOES', '[#54]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 54);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13641629', '2021-11-28 15:37:09.025', '2021-11-28 15:37:09.025', 80640000, 'NCB', '[#55]Thanh toan don hang SAGO SHOES', '[#55]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 55);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('0', '2021-11-29 10:13:17.373', '2021-11-29 10:13:17.373', 40320000, 'VNPAY', '[#56]Thanh toan don hang SAGO SHOES', '[#56]Thanh toan don hang SAGO SHOES', 'Giao dịch không thành công do: Khách hàng hủy giao dịch', 56);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13651507', '2021-12-10 19:10:25.644', '2021-12-10 19:10:25.644', 72000000, 'NCB', '[#57]Thanh toan don hang SAGO SHOES', '[#57]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 57);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13651508', '2021-12-10 19:17:47.072', '2021-12-10 19:17:47.072', 24000000, 'NCB', '[#58]Thanh toan don hang SAGO SHOES', '[#58]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 58);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13652159', '2021-12-12 20:00:00.82', '2021-12-12 20:00:00.82', 16674000, 'NCB', '[#71]Thanh toan don hang SAGO SHOES', '[#71]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 71);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13652474', '2021-12-13 07:59:39.144', '2021-12-13 07:59:39.144', 8379000, 'NCB', '[#74]Thanh toan don hang SAGO SHOES', '[#74]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 74);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13652485', '2021-12-13 08:09:22.439', '2021-12-13 08:09:22.439', 8127000, 'NCB', '[#75]Thanh toan don hang SAGO SHOES', '[#75]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 75);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13652404', '2021-12-13 06:48:40.582', '2021-12-13 08:20:06.94', 8127000, 'NCB', '[#73]Thanh toan don hang SAGO SHOES', '[#73]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 73);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13653932', '2021-12-15 06:36:46.097', '2021-12-15 06:36:46.097', 2457000, 'NCB', '[#87]Thanh toan don hang SAGO SHOES', '[#87]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 87);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13653937', '2021-12-15 06:38:23.381', '2021-12-15 06:38:23.381', 2457000, 'NCB', '[#88]Thanh toan don hang SAGO SHOES', '[#88]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 88);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13653966', '2021-12-15 06:54:35.469', '2021-12-15 06:54:35.469', 10584000, 'NCB', '[#89]Thanh toan don hang SAGO SHOES', '[#89]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 89);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13656004', '2021-12-17 17:36:07.478', '2021-12-17 17:36:07.478', 3422000, 'NCB', '[#103]Thanh toan don hang SAGO SHOES', '[#103]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 103);
INSERT INTO public.api_payment (id, created, updated, amount, bank_code, order_info, pay_date, reason, order_id) VALUES ('13656027', '2021-12-17 18:21:40.671', '2021-12-17 18:21:40.671', 462000, 'NCB', '[#133]Thanh toan don hang SAGO SHOES', '[#133]Thanh toan don hang SAGO SHOES', 'Giao dịch thành công', 133);


--
-- TOC entry 2618 (class 0 OID 34683)
-- Dependencies: 198
-- Data for Name: api_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (2, 'adidas Trae Young 1 Light Solid Grey Snakeskin', 6300000.00, 2, 'adidas-trae-young-1-light-solid-grey-snakeskin', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (3, 'adidas Trae Young 1 Acid Orange', 6132000.00, 2, 'adidas-trae-young-1-acid-orange', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (4, 'Xbox x Forum Tech Boost ''Halo 20th Anniversary''', 3843000.00, 2, 'xbox-x-forum-tech-boost-halo-20th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (5, 'Nike Air Max 90 Surplus Wolf Grey Pink Salt', 5691000.00, 1, 'nike-air-max-90-surplus-wolf-grey-pink-salt', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (6, 'Nike Blazer Low ''77 Premium Coconut Milk', 6321000.00, 1, 'nike-blazer-low-77-premium-coconut-milk', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (7, 'Nike Free Terra Vista Black Canvas', 1050000.00, 1, 'nike-free-terra-vista-black-canvas', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (8, 'Wmns Air Max 90 ''Lucky Charms''', 1953000.00, 1, 'wmns-air-max-90-lucky-charms', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (9, 'Nike Free Terra Vista Brown Kelp Pink Glaze', 798000.00, 1, 'nike-free-terra-vista-brown-kelp-pink-glaze', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (10, 'Overbreak SP ''Hot Curry''', 7854000.00, 1, 'overbreak-sp-hot-curry', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (11, 'Air Force 1 ''07 LX ''Toasty''', 6195000.00, 1, 'air-force-1-07-lx-toasty', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (12, 'Nike SB Dunk High RX-0 Unicorn Gundam', 8337000.00, 1, 'nike-sb-dunk-high-rx-0-unicorn-gundam', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (13, 'Overbreak SP ''Armory Navy''', 5943000.00, 1, 'overbreak-sp-armory-navy', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (14, 'Dunk Low PS ''Georgetown''', 5628000.00, 1, 'dunk-low-ps-georgetown', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (15, 'Nike Blazer Mid 77 Jumbo White Black', 525000.00, 1, 'nike-blazer-mid-77-jumbo-white-black', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (16, 'Nike BE-DO-WIN SP Around the World', 462000.00, 1, 'nike-be-do-win-sp-around-the-world', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (17, 'Nike Blazer Mid 77 Jumbo Dark Russet', 2079000.00, 1, 'nike-blazer-mid-77-jumbo-dark-russet', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (19, 'Nike Blazer Mid ''77 Premium Dark Chocolate', 840000.00, 1, 'nike-blazer-mid-77-premium-dark-chocolate', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (20, 'Nike Retro GTS 97 Koromogae (W)', 1050000.00, 1, 'nike-retro-gts-97-koromogae-w', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (21, 'Wmns Dunk Low Next Nature ''Pale Coral''', 1932000.00, 1, 'wmns-dunk-low-next-nature-pale-coral', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (22, 'Wmns Air Max Plus ''Sisterhood''', 1092000.00, 1, 'wmns-air-max-plus-sisterhood', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (23, 'Nike Air Huarache Sail Sunset (W)', 2100000.00, 1, 'nike-air-huarache-sail-sunset-w', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (25, 'Nike Dunk Low Siempre Familia', 7413000.00, 1, 'nike-dunk-low-siempre-familia', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (26, 'Nike Air Presto PRM Halloween', 4599000.00, 1, 'nike-air-presto-prm-halloween', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (27, 'Nike SB Dunk Low Mummy', 3843000.00, 1, 'nike-sb-dunk-low-mummy', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (28, 'Wmns SuperRep Cycle ''Gypsy Rose''', 5481000.00, 1, 'wmns-superrep-cycle-gypsy-rose', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (29, 'Nike Air Max 90 Terrascape Black Lime Ice', 6510000.00, 1, 'nike-air-max-90-terrascape-black-lime-ice', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (30, 'Air Max Terrascape Plus ''White Barely Volt''', 6321000.00, 1, 'air-max-terrascape-plus-white-barely-volt', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (31, 'Air Max Terrascape Plus ''Black Barely Volt''', 4767000.00, 1, 'air-max-terrascape-plus-black-barely-volt', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (32, 'Nike Zoom Freak 3 Clear Emerald', 6552000.00, 1, 'nike-zoom-freak-3-clear-emerald', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (33, 'Wmns Air Max 90 Terrascape ''Pomegranate''', 3087000.00, 1, 'wmns-air-max-90-terrascape-pomegranate', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (35, 'Court Borough Low 2 TD ''USA''', 4053000.00, 1, 'court-borough-low-2-td-usa', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (36, 'Wmns Air Max 270 ''Summit White Regal Pink''', 3906000.00, 1, 'wmns-air-max-270-summit-white-regal-pink', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (37, 'Shane Premium SB ''Black Iron Grey''', 2520000.00, 1, 'shane-premium-sb-black-iron-grey', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (38, 'Wmns Zoom Fly 4 ''Football Grey Sapphire''', 7581000.00, 1, 'wmns-zoom-fly-4-football-grey-sapphire', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (39, 'Nike Air Max 90 Terrascape Light Bone (W)', 2562000.00, 1, 'nike-air-max-90-terrascape-light-bone-w', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (43, 'NBA x Air Force 1 High ''07 LV8 ''75th Anniversary', 2520000.00, 1, 'nba-x-air-force-1-high-07-lv8-75th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (58, 'Yeezy Knit Runner Boot ''Sulfur''', 2457000.00, 2, 'yeezy-knit-runner-boot-sulfur', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (60, 'adidas Superturf Adventure Sean Wotherspoon Grey', 4410000.00, 2, 'adidas-superturf-adventure-sean-wotherspoon-grey', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (62, 'adidas Yeezy NSLTD Boot Khaki', 6363000.00, 2, 'adidas-yeezy-nsltd-boot-khaki', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (64, 'adidas Trae Young 1 So So Def Recordings Atlanta', 2625000.00, 2, 'adidas-trae-young-1-so-so-def-recordings-atlanta', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (66, 'adidas Forum Low Midwest Kids Black Gum', 7371000.00, 2, 'adidas-forum-low-midwest-kids-black-gum', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (68, 'adidas Trae Young 1 So So Def Recordings', 3360000.00, 2, 'adidas-trae-young-1-so-so-def-recordings', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (70, 'Copa Sense.3 Laceless TF ''Red''', 6699000.00, 2, 'copa-sense3-laceless-tf-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (72, 'X Speedflow.1 TF ''White Solar Red''', 5586000.00, 2, 'x-speedflow1-tf-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (74, 'Copa Sense.4 IN J ''White Solar Red''', 6888000.00, 2, 'copa-sense4-in-j-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (76, 'X Speedflow.3 Laceless TF ''Red''', 3864000.00, 2, 'x-speedflow3-laceless-tf-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (78, 'X Speedflow.1 TF ''Red''', 5523000.00, 2, 'x-speedflow1-tf-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (82, 'adidas EQT Cushion 91 Consortium 30th Anniversary', 6174000.00, 2, 'adidas-eqt-cushion-91-consortium-30th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (84, 'X Speedflow+ FG J ''Red''', 3990000.00, 2, 'x-speedflow-fg-j-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (86, 'X Speedflow.1 FG J ''White Solar Red''', 7140000.00, 2, 'x-speedflow1-fg-j-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (88, 'adidas EQT Race Walk 30th Anniversary', 4326000.00, 2, 'adidas-eqt-race-walk-30th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (90, 'adidas EQT Prototype 30th Anniversary', 7287000.00, 2, 'adidas-eqt-prototype-30th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (92, 'Copa Sense.3 IN ''White Solar Red''', 2772000.00, 2, 'copa-sense3-in-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (94, 'X Speedflow.1 AG ''White Solar Red''', 8127000.00, 2, 'x-speedflow1-ag-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (96, 'X Speedflow+ FG ''White Solar Red''', 4116000.00, 2, 'x-speedflow-fg-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (98, 'adidas EQT Support Consortium 30th Anniversary', 4830000.00, 2, 'adidas-eqt-support-consortium-30th-anniversary', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (132, 'Chuck Taylor All Star CX High ''Ash Stone Lime Twist''', 4494000.00, 6, 'chuck-taylor-all-star-cx-high-ash-stone-lime-twist', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (34, 'Nike Air Max 90 Terrascape Sail Sea Glass', 8379000.00, 1, 'nike-air-max-90-terrascape-sail-sea-glass', NULL);
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (24, 'Nike Air Force 1 Low Siempre Familia', 7623000.00, 1, 'nike-air-force-1-low-siempre-familia', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (18, 'Nike Air Huarache Koromogae (W)', 882000.00, 1, 'nike-air-huarache-koromogae-w', NULL);
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (80, 'X Speedflow+ FG J ''White Solar Red''', 7980000.00, 2, 'x-speedflow-fg-j-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (100, 'Predator Freak+ FG ''Demonskin - White Solar Red''', 6405000.00, 2, 'predator-freak-fg-demonskin-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (102, 'Copa Sense.3 TF J ''White Solar Red''', 2541000.00, 2, 'copa-sense3-tf-j-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (104, 'Predator Freak.3 IN J ''Demonscale - White Solar Red''', 3885000.00, 2, 'predator-freak3-in-j-demonscale-white-solar-red', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (115, 'Wmns Chuck Taylor All Star Dainty Mule ''Welcome to the Wild''', 6510000.00, 6, 'wmns-chuck-taylor-all-star-dainty-mule-welcome-to-the-wild', 'women');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (117, 'Chuck Taylor All Star All Terrain High ''Wheat''', 1932000.00, 6, 'chuck-taylor-all-star-all-terrain-high-wheat', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (118, 'Chuck Taylor All Star Move High ''Authentic Glam''', 1974000.00, 6, 'chuck-taylor-all-star-move-high-authentic-glam', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (119, 'Chuck Taylor All Star High ''Black Lime Twist''', 5838000.00, 6, 'chuck-taylor-all-star-high-black-lime-twist', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (120, 'Chuck 70 High ''Holiday Sweater - Midnight Navy''', 7140000.00, 6, 'chuck-70-high-holiday-sweater---midnight-navy', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (121, 'Chuck 70 High ''Holiday Sweater - Orange''', 8127000.00, 6, 'chuck-70-high-holiday-sweater---orange', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (122, 'thisisneverthat x Chuck 70 High ''New Vintage''', 5355000.00, 6, 'thisisneverthat-x-chuck-70-high-new-vintage', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (123, 'thisisneverthat x One Star Low ''New Vintage''', 3339000.00, 6, 'thisisneverthat-x-one-star-low-new-vintage', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (124, 'Kith x Chuck 70 High ''10th Anniversary - White''', 693000.00, 6, 'kith-x-chuck-70-high-10th-anniversary---white', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (125, 'Chuck 70 High ''Midnight Hibiscus''', 4704000.00, 6, 'chuck-70-high-midnight-hibiscus', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (126, 'Converse Chuck Taylor All-Star 70 Hi Kith 10 Year Anniversary Black', 588000.00, 6, 'converse-chuck-taylor-all-star-70-hi-kith-10-year-anniversary-black', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (127, 'Run Star Motion High ''Storm Pink''', 1386000.00, 6, 'run-star-motion-high-storm-pink', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (128, 'Chuck 70 High ''Cozy Utility - Egret''', 4704000.00, 6, 'chuck-70-high-cozy-utility---egret', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (129, 'Chuck Taylor All Star CX Low ''White Lime Twist''', 4725000.00, 6, 'chuck-taylor-all-star-cx-low-white-lime-twist', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (130, 'Wmns Chuck Taylor All Star 2X Platform High ''Cozy Sherpa - Vachetta Beige''', 7413000.00, 6, 'wmns-chuck-taylor-all-star-2x-platform-high-cozy-sherpa---vachetta-beige', 'women');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (131, 'Run Star Motion High ''Triple Black''', 5355000.00, 6, 'run-star-motion-high-triple-black', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (133, 'Chuck Taylor All Star CX High ''Black Hyper Pink''', 6846000.00, 6, 'chuck-taylor-all-star-cx-high-black-hyper-pink', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (134, 'Chuck 70 High ''Cozy Utility - Black''', 651000.00, 6, 'chuck-70-high-cozy-utility---black', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (135, 'Chuck Taylor All Star High ''Patchwork''', 420000.00, 6, 'chuck-taylor-all-star-high-patchwork', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (1, 'Chuck Taylor All Star Dainty Mule ''Welcome to the Wild''', 2500000.00, 6, 'chuck-taylor-all-star-dainty-mule-welcome-to-the-wild', NULL);
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (148, 'Nike React Infinity Run Flyknit 2', 3680000.00, 1, 'nike-react-infinity-run-flyknit-2', 'women');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (137, 'Speedcat LS Men''s Motorsport Shoes', 2070000.00, 4, 'speedcat-ls-mens-motorsport-shoes', 'men');
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (149, 'Tread Slick Boot', 18176000.00, 7, 'tread-slick-boot', NULL);
INSERT INTO public.api_product (id, name, price, brand_id, slug, gender) VALUES (150, 'Nike Air Huarache', 2760000.00, 1, 'nike-air-huarache', NULL);


--
-- TOC entry 2620 (class 0 OID 34692)
-- Dependencies: 200
-- Data for Name: api_productimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_productimage (id, image, product_id) VALUES (136, 'ef12e98c-d0bd-43fe-9084-2fa7b18aad47img03.webp', 115);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (137, '66a67dd5-484c-4d73-a018-8d8a23475980img03.webp', 115);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (139, 'd6da55a6-e571-42d7-9d87-912b1dd393a5gfg.png', 117);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (140, '745aee30-1bd2-41a1-aef5-68e538da6730gfg.png', 118);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (141, 'afa6f55a-7289-4156-ade6-cd034c1ce455gfg.png', 119);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (142, 'd1fcd3e4-6792-4e61-b1c5-963c780638a8gfg.png', 120);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (143, 'ee9931cc-6e49-4367-ace4-4117d5cdb036gfg.png', 121);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (144, '169ecf51-8e99-4c32-8efc-58c9855a30f2gfg.png', 122);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (145, '446d3850-a3db-48fa-8de8-6d8c78f9528fgfg.png', 123);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (146, '33631748-1048-43a4-8bf6-1dbbf3f0fcc4gfg.png', 124);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (147, '9f1cba43-02ca-491d-bac9-9db4bf74935cgfg.png', 125);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (148, '1e8f1155-c2d6-41aa-bfb6-117991e1c929gfg.png', 126);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (149, 'faa78d02-6fb8-433d-945e-44146cb8bbd8gfg.png', 127);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (150, 'f9ebdfc6-1383-4ac7-85b0-f1992e4b87bdgfg.png', 128);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (151, '43411761-016c-416c-bf18-a282ff85fcffgfg.png', 129);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (152, '1d6f1853-5a3a-41d8-a4d2-0d09a055a74egfg.png', 130);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (153, 'ff20bff3-051d-4141-956c-a1670ad74879gfg.png', 131);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (154, '5986a1d8-05da-417c-9e49-59034e3e7e93gfg.png', 132);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (155, 'c0ae0f04-c660-4820-aded-2aa04bbc18dagfg.png', 133);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (156, '14e3f31b-5ae4-4605-87b7-75c99205b97cgfg.png', 134);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (157, 'bf589037-d182-42f5-b3bd-04d9c4d2aad3gfg.png', 135);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (198, '720319fd-196a-4b07-bbe6-45a75c2911deblazer-mid-77-vintage-shoe-dNWPTj.jpg', 34);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (313, 'eb05410b-f289-4e25-a079-1d7cd2a28e32img-1.jpg', 137);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (314, '6a8ebe87-8ff8-4c0b-b299-3da9a4b0b435img-1.jpg', 137);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (315, 'e98d606c-fd77-4844-a397-7c0596767808Speedcat-LS-Men''s-Motorsport-Shoes.jpg', 137);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (316, '0e71d47e-c9cb-494e-bbb5-85cc2effb746react-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2 (1).png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (317, '5bb1273e-0420-4f35-8c89-b621f86adc78react-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2 (2).png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (318, '985ee665-5851-4b84-8d79-0f25358622e1react-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2 (3).png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (319, '677082d1-a4b1-4245-b2ed-4d35213ba6cfreact-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2 (4).png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (320, 'f9a00f95-c19f-4c0a-ba87-91e072b2a556react-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2 (5).png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (321, '41edfc8b-a313-4c64-be51-e7e5288c6be7react-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2.jpg', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (322, 'e6b7c6bd-de08-45f0-a840-2a6d88fd104ereact-infinity-run-flyknit-2-mens-road-running-shoes-DttDF2.png', 148);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (328, 'defea344-bcca-425c-8e93-3e9269395a9aimg-1.jpg', 149);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (329, 'e2582312-33aa-44b1-81cc-3d104a1da7d2img-1.jpg', 149);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (330, 'abbb64f1-2d5a-49cc-a491-4db58f1d9262img-1.jpg', 149);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (331, 'c49199e4-fe4d-41e8-9b34-afdd751b5acaimg-1.jpg', 149);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (332, '32dbf651-a0db-4086-8d12-aced897c0cecair-huarache-mens-shoes-JppwBb (1).jpg', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (333, 'ac4459ff-91ce-4df3-a07f-dcb83640b396air-huarache-mens-shoes-JppwBb (1).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (334, 'd9b6a5bd-dbbc-4ac7-96cd-67a52c0082d0air-huarache-mens-shoes-JppwBb (2).jpg', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (335, 'af6146e1-bbae-405a-be17-e721632c5710air-huarache-mens-shoes-JppwBb (2).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (336, 'ba6c7de4-7015-4b09-8505-431d9b78f460air-huarache-mens-shoes-JppwBb (3).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (337, '4d865db8-b0cd-4691-8c43-b3a178798140air-huarache-mens-shoes-JppwBb (4).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (338, '2c7eae1c-914e-4c23-bc92-19be00bcd559air-huarache-mens-shoes-JppwBb (5).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (339, 'a84787bf-c5cf-43ae-8b82-1828be40db63air-huarache-mens-shoes-JppwBb (6).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (340, '09403f74-9d5c-4368-b494-2210559abc98air-huarache-mens-shoes-JppwBb (7).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (341, 'e0b1e1a8-be31-4fb3-9481-822a58ed588cair-huarache-mens-shoes-JppwBb (8).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (342, '369da95e-8e8c-4aed-ac02-9cf115a1abfcair-huarache-mens-shoes-JppwBb (9).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (343, '83db01af-7d3b-4d06-a7a5-0f31f2bf8e32air-huarache-mens-shoes-JppwBb (10).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (344, '2813d11f-f322-4f15-a03e-f9f22ca9b9c6air-huarache-mens-shoes-JppwBb (11).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (345, '232d81d7-f175-4967-9c09-63dcf3edd285air-huarache-mens-shoes-JppwBb (12).png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (291, 'f21c99b4-597b-490f-af98-64194891b28bimg-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (292, 'cf6c9435-5496-486d-8efe-0e273ced0059img-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (293, '2993873d-6fc9-4710-8328-76442e5ecc8eimg-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (294, 'f90a39f1-eb5f-467e-97f0-f588bc459ae6img-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (295, '74d6236c-c1a6-44f5-83dd-870a2ff1db9dimg-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (296, 'd59572ee-05a8-4592-8b1d-f1ac8a4c4136img-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (297, '88d8be37-0d58-45dd-9023-1949f7cab4b5img-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (298, '3ba21f16-2186-4641-a0e5-d75470fa93d3img-1.jpg', 1);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (346, 'b28bfc12-1bd2-4b24-ab83-5a2f6b8a2d58air-huarache-mens-shoes-JppwBb.jpg', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (347, '48f8df6f-15dd-41b8-b2cd-a0526d793091air-huarache-mens-shoes-JppwBb.png', 150);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (348, '68122695-2ac4-43f6-b37a-11cd7efa0960women’s-air-huarache-koromogae-release-date (1).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (349, 'c9c23835-e4c7-4d1b-a1c3-08334393f8dfwomen’s-air-huarache-koromogae-release-date (2).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (350, 'ae2f148c-91bc-4c3f-bd67-f6a47c2002dawomen’s-air-huarache-koromogae-release-date (3).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (351, '8e93af39-9338-4f8c-a6ef-7635e4047e34women’s-air-huarache-koromogae-release-date (4).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (352, 'e943bd39-3574-49ee-a824-42b781bcd181women’s-air-huarache-koromogae-release-date (5).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (353, '0b704178-2822-472e-956f-3ec1d346b95awomen’s-air-huarache-koromogae-release-date (6).jpg', 18);
INSERT INTO public.api_productimage (id, image, product_id) VALUES (354, 'c53f01f9-c693-40a3-a09b-c803b7d36e65women’s-air-huarache-koromogae-release-date.jpg', 18);


--
-- TOC entry 2622 (class 0 OID 34697)
-- Dependencies: 202
-- Data for Name: api_productvariant; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (4, 20, '2021-11-06 14:07:35.449377+07', '2021-11-06 14:07:35.449412+07', 1, 1, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (5, 20, '2021-11-06 14:07:39.651693+07', '2021-11-06 14:07:39.651751+07', 1, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (8, 20, '2021-11-06 14:07:48.419112+07', '2021-11-06 14:07:48.419175+07', 1, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (2, 20, '2021-11-06 13:55:47.159327+07', '2021-11-24 20:45:28.183+07', 39, 1, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (9, 7, '2021-11-07 12:36:52.72233+07', '2021-11-25 22:44:28.049+07', 26, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (3, 3, '2021-11-06 13:56:01.856631+07', '2021-11-25 22:44:28.049+07', 39, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (38, 10, '2021-12-12 01:24:46.42+07', '2021-12-12 01:24:48.603+07', 70, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (39, 7, '2021-12-12 01:24:48.357+07', '2021-12-12 01:24:50.539+07', 32, 6, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (40, 6, '2021-12-12 01:24:50.296+07', '2021-12-12 01:24:52.522+07', 29, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (41, 5, '2021-12-12 01:24:52.276+07', '2021-12-12 01:24:54.474+07', 115, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (42, 4, '2021-12-12 01:24:54.221+07', '2021-12-12 01:24:56.512+07', 100, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (43, 6, '2021-12-12 01:24:56.269+07', '2021-12-12 01:24:58.456+07', 62, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (7, 13, '2021-11-06 14:07:45.619664+07', '2021-12-11 04:21:39.505+07', 1, 4, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (10, 7, '2021-12-09 21:39:43.183+07', '2021-12-11 20:15:26.47+07', 2, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (12, 12, '2021-12-11 02:09:33.636+07', '2021-12-12 01:11:21.357+07', 1, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (14, 10, '2021-12-12 01:11:46.845+07', '2021-12-12 01:11:48.815+07', 34, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (36, 17, '2021-12-12 01:24:42.525+07', '2021-12-12 01:30:39.573+07', 74, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (67, 16, '2021-12-12 01:26:32.575+07', '2021-12-12 01:31:02.462+07', 82, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (69, 8, '2021-12-12 01:26:36.439+07', '2021-12-12 01:31:05.855+07', 13, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (82, 15, '2021-12-12 01:27:01.674+07', '2021-12-12 01:31:30.694+07', 128, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (13, 9, '2021-12-12 01:11:45.337+07', '2021-12-13 15:30:39.183+07', 34, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (31, 7, '2021-12-12 01:24:33.052+07', '2021-12-12 01:24:35.009+07', 130, 6, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (32, 3, '2021-12-12 01:24:34.764+07', '2021-12-12 01:24:36.941+07', 66, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (33, 9, '2021-12-12 01:24:36.7+07', '2021-12-12 01:24:38.886+07', 90, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (34, 4, '2021-12-12 01:24:38.641+07', '2021-12-12 01:24:40.825+07', 86, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (35, 9, '2021-12-12 01:24:40.579+07', '2021-12-12 01:24:42.769+07', 120, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (44, 3, '2021-12-12 01:24:58.209+07', '2021-12-12 01:25:00.408+07', 30, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (45, 7, '2021-12-12 01:25:00.164+07', '2021-12-12 01:25:02.334+07', 6, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (46, 10, '2021-12-12 01:25:02.094+07', '2021-12-12 01:25:04.279+07', 2, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (47, 8, '2021-12-12 01:25:04.033+07', '2021-12-12 01:25:06.226+07', 11, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (48, 5, '2021-12-12 01:25:05.973+07', '2021-12-12 01:25:08.161+07', 82, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (49, 9, '2021-12-12 01:25:07.916+07', '2021-12-12 01:25:10.101+07', 3, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (50, 7, '2021-12-12 01:25:09.858+07', '2021-12-12 01:25:16.144+07', 13, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (51, 3, '2021-12-12 01:26:00.409+07', '2021-12-12 01:26:02.343+07', 25, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (52, 8, '2021-12-12 01:26:02.102+07', '2021-12-12 01:26:04.268+07', 66, 4, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (53, 10, '2021-12-12 01:26:04.025+07', '2021-12-12 01:26:06.219+07', 90, 6, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (54, 6, '2021-12-12 01:26:05.975+07', '2021-12-12 01:26:08.151+07', 86, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (55, 9, '2021-12-12 01:26:07.907+07', '2021-12-12 01:26:10.079+07', 120, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (56, 3, '2021-12-12 01:26:09.838+07', '2021-12-12 01:26:13.227+07', 74, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (37, 11, '2021-12-12 01:24:44.463+07', '2021-12-12 01:26:13.227+07', 133, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (57, 7, '2021-12-12 01:26:12.986+07', '2021-12-12 01:26:15.414+07', 70, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (58, 8, '2021-12-12 01:26:15.168+07', '2021-12-12 01:26:17.35+07', 32, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (59, 10, '2021-12-12 01:26:17.109+07', '2021-12-12 01:26:19.283+07', 29, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (60, 6, '2021-12-12 01:26:19.041+07', '2021-12-12 01:26:21.22+07', 115, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (61, 8, '2021-12-12 01:26:20.976+07', '2021-12-12 01:26:23.152+07', 100, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (62, 4, '2021-12-12 01:26:22.91+07', '2021-12-12 01:26:25.091+07', 62, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (63, 8, '2021-12-12 01:26:24.849+07', '2021-12-12 01:26:27.022+07', 6, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (64, 3, '2021-12-12 01:26:26.779+07', '2021-12-12 01:26:28.961+07', 30, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (65, 7, '2021-12-12 01:26:28.716+07', '2021-12-12 01:26:30.887+07', 2, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (66, 9, '2021-12-12 01:26:30.647+07', '2021-12-12 01:26:32.817+07', 11, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (68, 4, '2021-12-12 01:26:34.507+07', '2021-12-12 01:26:36.695+07', 3, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (70, 7, '2021-12-12 01:26:38.383+07', '2021-12-12 01:26:40.558+07', 119, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (71, 5, '2021-12-12 01:26:40.309+07', '2021-12-12 01:26:42.495+07', 5, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (72, 10, '2021-12-12 01:26:42.251+07', '2021-12-12 01:26:44.439+07', 14, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (73, 8, '2021-12-12 01:26:44.196+07', '2021-12-12 01:26:46.38+07', 72, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (74, 6, '2021-12-12 01:26:46.139+07', '2021-12-12 01:26:48.384+07', 78, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (75, 8, '2021-12-12 01:26:48.127+07', '2021-12-12 01:26:50.324+07', 28, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (76, 6, '2021-12-12 01:26:50.081+07', '2021-12-12 01:26:52.263+07', 131, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (77, 5, '2021-12-12 01:26:52.018+07', '2021-12-12 01:26:54.194+07', 122, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (78, 6, '2021-12-12 01:26:53.948+07', '2021-12-12 01:26:56.134+07', 98, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (79, 8, '2021-12-12 01:26:55.891+07', '2021-12-12 01:26:58.064+07', 31, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (80, 9, '2021-12-12 01:26:57.821+07', '2021-12-12 01:26:59.992+07', 129, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (81, 8, '2021-12-12 01:26:59.749+07', '2021-12-12 01:27:01.918+07', 125, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (83, 7, '2021-12-12 01:27:03.604+07', '2021-12-12 01:27:05.8+07', 26, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (84, 3, '2021-12-12 01:27:05.555+07', '2021-12-12 01:27:07.75+07', 132, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (85, 4, '2021-12-12 01:27:07.504+07', '2021-12-12 01:27:09.68+07', 60, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (86, 10, '2021-12-12 01:27:09.436+07', '2021-12-12 01:27:11.614+07', 88, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (87, 9, '2021-12-12 01:27:11.371+07', '2021-12-12 01:27:13.66+07', 96, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (88, 3, '2021-12-12 01:27:13.419+07', '2021-12-12 01:27:15.599+07', 35, 4, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (89, 4, '2021-12-12 01:27:15.355+07', '2021-12-12 01:27:17.523+07', 84, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (90, 5, '2021-12-12 01:27:17.279+07', '2021-12-12 01:27:19.453+07', 36, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (91, 4, '2021-12-12 01:27:19.206+07', '2021-12-12 01:27:21.383+07', 104, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (93, 7, '2021-12-12 01:27:23.077+07', '2021-12-12 01:27:25.261+07', 4, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (15, 20, '2021-12-12 01:14:25.94+07', '2021-12-12 01:29:16.124+07', 12, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (92, 3, '2021-12-12 01:27:21.139+07', '2021-12-12 01:27:23.32+07', 76, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (94, 8, '2021-12-12 01:27:25.018+07', '2021-12-12 01:27:27.194+07', 27, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (95, 3, '2021-12-12 01:27:26.949+07', '2021-12-12 01:27:29.142+07', 68, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (99, 8, '2021-12-12 01:27:34.724+07', '2021-12-12 01:32:04.05+07', 64, 4, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (97, 5, '2021-12-12 01:27:30.837+07', '2021-12-12 01:27:33.013+07', 33, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (98, 9, '2021-12-12 01:27:32.772+07', '2021-12-12 01:27:34.97+07', 92, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (101, 10, '2021-12-12 01:27:38.582+07', '2021-12-12 01:27:40.757+07', 102, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (102, 6, '2021-12-12 01:27:40.514+07', '2021-12-12 01:27:42.694+07', 37, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (103, 9, '2021-12-12 01:27:42.45+07', '2021-12-12 01:27:44.622+07', 43, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (105, 5, '2021-12-12 01:27:46.314+07', '2021-12-12 01:27:48.517+07', 23, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (106, 6, '2021-12-12 01:27:48.274+07', '2021-12-12 01:27:50.449+07', 17, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (107, 6, '2021-12-12 01:27:50.206+07', '2021-12-12 01:27:52.373+07', 118, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (108, 9, '2021-12-12 01:27:52.131+07', '2021-12-12 01:27:54.307+07', 8, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (109, 5, '2021-12-12 01:27:54.066+07', '2021-12-12 01:27:56.233+07', 117, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (110, 3, '2021-12-12 01:27:55.991+07', '2021-12-12 01:27:58.166+07', 21, 6, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (111, 3, '2021-12-12 01:27:57.924+07', '2021-12-12 01:28:00.1+07', 127, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (112, 5, '2021-12-12 01:27:59.853+07', '2021-12-12 01:28:02.025+07', 22, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (113, 4, '2021-12-12 01:28:01.781+07', '2021-12-12 01:28:03.96+07', 20, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (114, 5, '2021-12-12 01:28:03.715+07', '2021-12-12 01:28:05.885+07', 7, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (115, 7, '2021-12-12 01:28:05.641+07', '2021-12-12 01:28:07.825+07', 18, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (6, 1, '2021-11-06 14:07:42.489554+07', '2021-12-19 22:00:31.905+07', 1, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (11, 28, '2021-12-09 21:48:57.953+07', '2021-12-19 22:07:40.218+07', 2, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (116, 10, '2021-12-12 01:28:07.572+07', '2021-12-12 01:28:09.746+07', 19, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (117, 6, '2021-12-12 01:28:09.501+07', '2021-12-12 01:28:11.675+07', 9, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (118, 3, '2021-12-12 01:28:11.432+07', '2021-12-12 01:28:13.615+07', 124, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (119, 9, '2021-12-12 01:28:13.368+07', '2021-12-12 01:28:15.544+07', 134, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (120, 3, '2021-12-12 01:28:15.301+07', '2021-12-12 01:28:17.484+07', 126, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (121, 8, '2021-12-12 01:28:17.239+07', '2021-12-12 01:28:19.433+07', 15, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (122, 4, '2021-12-12 01:28:19.183+07', '2021-12-12 01:28:21.394+07', 16, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (123, 8, '2021-12-12 01:28:21.15+07', '2021-12-12 01:28:40.775+07', 135, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (18, 20, '2021-12-12 01:14:31.056+07', '2021-12-12 01:29:16.125+07', 121, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (19, 20, '2021-12-12 01:14:33.011+07', '2021-12-12 01:29:16.126+07', 94, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (20, 20, '2021-12-12 01:14:34.732+07', '2021-12-12 01:29:16.126+07', 94, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (21, 20, '2021-12-12 01:14:36.674+07', '2021-12-12 01:29:16.126+07', 80, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (22, 20, '2021-12-12 01:14:38.368+07', '2021-12-12 01:29:16.126+07', 80, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (23, 20, '2021-12-12 01:14:40.317+07', '2021-12-12 01:29:16.127+07', 10, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (24, 20, '2021-12-12 01:14:42.009+07', '2021-12-12 01:29:16.127+07', 10, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (25, 20, '2021-12-12 01:14:43.96+07', '2021-12-12 01:29:16.127+07', 24, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (26, 20, '2021-12-12 01:14:45.662+07', '2021-12-12 01:29:16.127+07', 24, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (27, 20, '2021-12-12 01:14:47.653+07', '2021-12-12 01:29:16.128+07', 38, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (28, 20, '2021-12-12 01:14:49.417+07', '2021-12-12 01:29:16.128+07', 38, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (29, 20, '2021-12-12 01:14:51.376+07', '2021-12-12 01:29:16.129+07', 25, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (30, 20, '2021-12-12 01:14:53.153+07', '2021-12-12 01:29:16.13+07', 25, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (124, 3, '2021-12-12 01:30:09.129+07', '2021-12-12 01:30:11.075+07', 1, 6, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (125, 10, '2021-12-12 01:30:10.833+07', '2021-12-12 01:30:13.012+07', 34, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (126, 3, '2021-12-12 01:30:12.768+07', '2021-12-12 01:30:14.945+07', 12, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (127, 8, '2021-12-12 01:30:14.7+07', '2021-12-12 01:30:16.953+07', 94, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (128, 3, '2021-12-12 01:30:16.707+07', '2021-12-12 01:30:18.896+07', 121, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (129, 9, '2021-12-12 01:30:18.651+07', '2021-12-12 01:30:20.842+07', 80, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (130, 5, '2021-12-12 01:30:20.6+07', '2021-12-12 01:30:22.775+07', 10, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (131, 3, '2021-12-12 01:30:22.531+07', '2021-12-12 01:30:24.704+07', 24, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (132, 9, '2021-12-12 01:30:24.462+07', '2021-12-12 01:30:26.656+07', 38, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (133, 9, '2021-12-12 01:30:26.413+07', '2021-12-12 01:30:28.615+07', 130, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (134, 4, '2021-12-12 01:30:28.369+07', '2021-12-12 01:30:30.575+07', 25, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (135, 5, '2021-12-12 01:30:30.32+07', '2021-12-12 01:30:32.517+07', 66, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (136, 10, '2021-12-12 01:30:32.271+07', '2021-12-12 01:30:34.462+07', 90, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (137, 10, '2021-12-12 01:30:34.217+07', '2021-12-12 01:30:36.403+07', 86, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (138, 3, '2021-12-12 01:30:36.157+07', '2021-12-12 01:30:39.572+07', 120, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (139, 8, '2021-12-12 01:30:39.327+07', '2021-12-12 01:30:41.787+07', 133, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (140, 4, '2021-12-12 01:30:41.539+07', '2021-12-12 01:30:43.746+07', 70, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (141, 7, '2021-12-12 01:30:43.503+07', '2021-12-12 01:30:45.787+07', 32, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (142, 8, '2021-12-12 01:30:45.54+07', '2021-12-12 01:30:47.714+07', 29, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (143, 3, '2021-12-12 01:30:47.472+07', '2021-12-12 01:30:49.644+07', 115, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (144, 3, '2021-12-12 01:30:49.402+07', '2021-12-12 01:30:51.577+07', 100, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (145, 7, '2021-12-12 01:30:51.335+07', '2021-12-12 01:30:53.516+07', 62, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (146, 9, '2021-12-12 01:30:53.272+07', '2021-12-12 01:30:55.444+07', 6, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (147, 5, '2021-12-12 01:30:55.202+07', '2021-12-12 01:30:57.39+07', 30, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (148, 9, '2021-12-12 01:30:57.146+07', '2021-12-12 01:30:59.328+07', 2, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (149, 8, '2021-12-12 01:30:59.086+07', '2021-12-12 01:31:02.461+07', 11, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (150, 5, '2021-12-12 01:31:02.22+07', '2021-12-12 01:31:05.854+07', 3, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (151, 9, '2021-12-12 01:31:05.61+07', '2021-12-12 01:31:08.036+07', 119, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (152, 9, '2021-12-12 01:31:07.792+07', '2021-12-12 01:31:09.982+07', 5, 6, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (153, 10, '2021-12-12 01:31:09.736+07', '2021-12-12 01:31:11.93+07', 14, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (154, 8, '2021-12-12 01:31:11.688+07', '2021-12-12 01:31:13.87+07', 72, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (155, 7, '2021-12-12 01:31:13.626+07', '2021-12-12 01:31:15.812+07', 78, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (156, 3, '2021-12-12 01:31:15.569+07', '2021-12-12 01:31:17.772+07', 28, 6, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (157, 8, '2021-12-12 01:31:17.53+07', '2021-12-12 01:31:19.748+07', 131, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (158, 4, '2021-12-12 01:31:19.501+07', '2021-12-12 01:31:21.692+07', 122, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (159, 6, '2021-12-12 01:31:21.45+07', '2021-12-12 01:31:23.635+07', 98, 4, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (160, 9, '2021-12-12 01:31:23.391+07', '2021-12-12 01:31:25.575+07', 31, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (161, 4, '2021-12-12 01:31:25.325+07', '2021-12-12 01:31:27.509+07', 129, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (162, 8, '2021-12-12 01:31:27.267+07', '2021-12-12 01:31:30.694+07', 125, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (163, 5, '2021-12-12 01:31:30.452+07', '2021-12-12 01:31:32.913+07', 26, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (164, 5, '2021-12-12 01:31:32.644+07', '2021-12-12 01:31:34.846+07', 132, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (165, 5, '2021-12-12 01:31:34.602+07', '2021-12-12 01:31:36.792+07', 60, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (166, 10, '2021-12-12 01:31:36.545+07', '2021-12-12 01:31:38.727+07', 88, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (167, 8, '2021-12-12 01:31:38.485+07', '2021-12-12 01:31:40.67+07', 96, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (168, 10, '2021-12-12 01:31:40.424+07', '2021-12-12 01:31:42.605+07', 35, 7, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (170, 6, '2021-12-12 01:31:44.404+07', '2021-12-12 01:31:46.582+07', 36, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (96, 13, '2021-12-12 01:27:28.899+07', '2021-12-12 01:31:57.493+07', 123, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (169, 10, '2021-12-12 01:31:42.363+07', '2021-12-12 01:31:44.646+07', 84, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (171, 6, '2021-12-12 01:31:46.338+07', '2021-12-12 01:31:48.521+07', 104, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (172, 5, '2021-12-12 01:31:48.279+07', '2021-12-12 01:31:50.468+07', 76, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (173, 3, '2021-12-12 01:31:50.223+07', '2021-12-12 01:31:52.411+07', 4, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (174, 10, '2021-12-12 01:31:52.168+07', '2021-12-12 01:31:54.342+07', 27, 6, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (175, 8, '2021-12-12 01:31:54.1+07', '2021-12-12 01:31:57.492+07', 68, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (176, 6, '2021-12-12 01:31:57.25+07', '2021-12-12 01:31:59.697+07', 33, 4, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (177, 10, '2021-12-12 01:31:59.454+07', '2021-12-12 01:32:04.049+07', 92, 5, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (100, 16, '2021-12-12 01:27:36.658+07', '2021-12-12 01:32:04.05+07', 39, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (178, 9, '2021-12-12 01:32:03.801+07', '2021-12-12 01:32:06.466+07', 102, 3, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (179, 4, '2021-12-12 01:32:06.217+07', '2021-12-12 01:32:08.404+07', 37, 5, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (180, 6, '2021-12-12 01:32:08.159+07', '2021-12-12 01:32:10.337+07', 43, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (181, 8, '2021-12-12 01:32:10.091+07', '2021-12-12 01:32:12.277+07', 58, 3, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (182, 7, '2021-12-12 01:32:12.032+07', '2021-12-12 01:32:14.204+07', 23, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (183, 3, '2021-12-12 01:32:13.963+07', '2021-12-12 01:32:16.137+07', 17, 4, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (184, 3, '2021-12-12 01:32:15.894+07', '2021-12-12 01:32:18.073+07', 118, 4, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (185, 6, '2021-12-12 01:32:17.83+07', '2021-12-12 01:32:20.006+07', 8, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (186, 6, '2021-12-12 01:32:19.765+07', '2021-12-12 01:32:21.955+07', 117, 6, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (187, 9, '2021-12-12 01:32:21.711+07', '2021-12-12 01:32:23.892+07', 21, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (188, 10, '2021-12-12 01:32:23.648+07', '2021-12-12 01:32:25.835+07', 127, 5, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (189, 5, '2021-12-12 01:32:25.592+07', '2021-12-12 01:32:27.782+07', 22, 7, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (190, 10, '2021-12-12 01:32:27.538+07', '2021-12-12 01:32:29.71+07', 20, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (191, 10, '2021-12-12 01:32:29.468+07', '2021-12-12 01:32:31.651+07', 7, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (192, 9, '2021-12-12 01:32:31.406+07', '2021-12-12 01:32:33.596+07', 18, 3, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (193, 3, '2021-12-12 01:32:33.351+07', '2021-12-12 01:32:35.531+07', 19, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (194, 9, '2021-12-12 01:32:35.288+07', '2021-12-12 01:32:37.467+07', 9, 2, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (195, 9, '2021-12-12 01:32:37.223+07', '2021-12-12 01:32:39.399+07', 124, 7, 3);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (17, 19, '2021-12-12 01:14:29.347+07', '2021-12-15 13:56:07.208+07', 121, 2, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (196, 5, '2021-12-12 01:32:39.156+07', '2021-12-12 01:32:41.33+07', 134, 7, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (197, 7, '2021-12-12 01:32:41.086+07', '2021-12-12 01:32:43.256+07', 126, 2, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (198, 8, '2021-12-12 01:32:43.015+07', '2021-12-12 01:32:45.184+07', 15, 4, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (199, 10, '2021-12-12 01:32:44.944+07', '2021-12-12 01:32:47.116+07', 16, 2, 7);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (200, 3, '2021-12-12 01:32:46.874+07', '2021-12-12 01:33:08.952+07', 135, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (201, 20, '2021-12-15 11:53:50.141+07', '2021-12-15 12:00:45.441+07', 80, 5, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (202, 800, '2021-12-15 12:12:12.4+07', '2021-12-15 12:15:23.724+07', 34, 6, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (203, 200, '2021-12-15 12:23:27.753+07', '2021-12-15 12:23:27.783+07', 121, 3, 6);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (204, 20, '2021-12-15 12:23:27.78+07', '2021-12-15 12:23:27.802+07', 12, 3, 6);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (205, 20, '2021-12-15 12:24:06.829+07', '2021-12-15 12:24:06.847+07', 94, 1, 5);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (16, 18, '2021-12-12 01:14:27.403+07', '2021-12-15 13:30:36.717+07', 12, 2, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (206, 20, '2021-12-15 13:59:15.623+07', '2021-12-15 13:59:15.637+07', 119, 5, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (207, 20, '2021-12-15 13:59:15.634+07', '2021-12-15 13:59:15.663+07', 3, 1, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (208, 200, '2021-12-16 14:43:19.354+07', '2021-12-16 14:43:19.423+07', 10, 1, 5);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (209, 5, '2021-12-16 14:50:31.305+07', '2021-12-16 14:50:31.365+07', 122, 8, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (210, 10, '2021-12-16 14:50:31.363+07', '2021-12-16 14:50:31.395+07', 72, 1, 4);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (104, 2, '2021-12-12 01:27:44.379+07', '2021-12-16 14:58:34.558+07', 58, 3, 2);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (211, 10, '2021-12-16 23:01:37.91+07', '2021-12-16 23:01:37.991+07', 148, 4, 1);
INSERT INTO public.api_productvariant (id, quantity, created, updated, product_id, size_id, color_id) VALUES (212, 20, '2021-12-19 22:32:23.856+07', '2021-12-19 22:32:23.888+07', 18, 1, 3);


--
-- TOC entry 2624 (class 0 OID 34703)
-- Dependencies: 204
-- Data for Name: api_review; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (1, '2021-12-15 10:34:18.117', '2021-12-15 10:34:18.117', 'fefefef', 'Son Cong', 4, 'Day la review', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (2, '2021-12-15 10:41:22.333', '2021-12-15 10:41:22.333', 'San pham tuyet cu meo', 'Son Cong', 5, 'San Pham Tuyet', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (3, '2021-12-15 10:44:07.203', '2021-12-15 10:44:07.203', 'San pham tuyet cu meo', 'Son Cong', 5, 'San Pham Tuyet', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (4, '2021-12-15 10:44:17.576', '2021-12-15 10:44:17.576', 'San pham tuyet cu meo', 'Son Cong', 5, 'San Pham Tuyet', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (5, '2021-12-15 10:45:19.647', '2021-12-15 10:45:19.647', 'san pham tuyet', 'Son', 5, 'san pham tuyet', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (6, '2021-12-15 10:45:46.146', '2021-12-15 10:45:46.146', 'san pham tuyet', 'Son', 5, 'san pham tuyet', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (7, '2021-12-15 10:46:21.527', '2021-12-15 10:46:21.527', 'pro', 'Son', 5, 'vjp', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (8, '2021-12-15 10:49:56.651', '2021-12-15 10:49:56.651', 'danh gia choi thoi', 'son', 5, 'danh gia choi', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (9, '2021-12-15 10:50:53.851', '2021-12-15 10:50:53.851', 'day la review', 'Son Cong', 5, 'Day la review', 129);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (10, '2021-12-15 14:18:38.625', '2021-12-15 14:18:38.625', 'oce', 'Son Cong', 5, 'san pham tuyet', 126);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (11, '2021-12-17 18:11:18.979', '2021-12-17 18:11:18.979', 'giày wa'' mắc', 'Son Cong', NULL, 'giày j mắc như quỷ', 149);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (12, '2021-12-17 18:11:54.366', '2021-12-17 18:11:54.366', 'Giày cho người giàu', 'Phan Công Sơn', NULL, 'Giày cho người giàu', 149);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (13, '2021-12-17 18:12:57.108', '2021-12-17 18:12:57.108', 'giày quá rẻ', 'Phan Công Sơn', NULL, 'giày rẻ', 149);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (14, '2021-12-19 15:57:35.672', '2021-12-19 15:57:35.672', 'san pham dep', 'Son Cong', NULL, 'Day la review', 16);
INSERT INTO public.api_review (id, created, updated, content, name, rating, title, product_id) VALUES (15, '2021-12-19 15:59:07.05', '2021-12-19 15:59:07.05', 'review', 'Son Cong', NULL, 'Day la review', 16);


--
-- TOC entry 2626 (class 0 OID 34711)
-- Dependencies: 206
-- Data for Name: api_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_role (id, created, updated, tenquyen) VALUES (1, '2021-11-19 15:36:51', '2021-11-19 15:36:52', 'System');
INSERT INTO public.api_role (id, created, updated, tenquyen) VALUES (2, '2021-11-19 22:27:43', '2021-11-19 22:27:45', 'Sale');
INSERT INTO public.api_role (id, created, updated, tenquyen) VALUES (3, '2021-11-24 20:55:57', '2021-11-24 20:56:00', 'Warehouse');


--
-- TOC entry 2628 (class 0 OID 34716)
-- Dependencies: 208
-- Data for Name: api_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_size (id, name, slug) VALUES (1, '35', '35');
INSERT INTO public.api_size (id, name, slug) VALUES (2, '36', '36');
INSERT INTO public.api_size (id, name, slug) VALUES (3, '37', '37');
INSERT INTO public.api_size (id, name, slug) VALUES (4, '38', '38');
INSERT INTO public.api_size (id, name, slug) VALUES (5, '39', '39');
INSERT INTO public.api_size (id, name, slug) VALUES (6, '40', '40');
INSERT INTO public.api_size (id, name, slug) VALUES (7, '41', '41');
INSERT INTO public.api_size (id, name, slug) VALUES (8, '42', '42');


--
-- TOC entry 2630 (class 0 OID 34724)
-- Dependencies: 210
-- Data for Name: api_staff; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (1, '2021-11-01 15:37:09', '2021-11-19 15:37:14', 'System', '$2a$10$m/MSQD4jDn2oyqBq76ZXhu7QnlscdkDio0GxlPy2ClnxxR0pFOXHe', 'system', 1);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (3, '2021-11-20 22:54:47', '2021-12-15 06:15:57.455', 'Nam Nguyen', '$2a$10$9.RG3G5U7mTdvsm9G5cxseUkmNHtTk829y4t9Le80F9zbztGSWHxm', 'namnmp', 3);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (5, '2021-12-15 06:19:22.742', '2021-12-15 06:19:22.742', 'Dat Ho', '$2a$10$ncadRdD80nG9.9M12hkybu9dGivXZiMwtuKlaTFdEjROuAXLOAP5e', 'datsh', 2);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (6, '2021-12-15 06:19:54.594', '2021-12-15 06:19:54.594', 'Huy On', '$2a$10$Oe.r5W7U605TE0P5wejWGODZpQCb8hCZ0.fEImWuOKiALwXAs2YOa', 'huyot', 3);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (7, '2021-12-15 07:03:28.534', '2021-12-15 07:03:28.534', 'test', '$2a$10$Crl3q9dnlWAIHZgzpXrCo.5dIWUgjTn3CnxKb0bMwPT1YqkKJmgCC', 'test', 2);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (8, '2021-12-16 15:27:55.454', '2021-12-16 15:27:55.454', 'Nguyen Van A', '$2a$10$FKmSHatIZnO6wTqe3YyRG.MW6ge/IR570hEO4JLJvGgc5pbsFGrVK', 'saleaccount', 2);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (2, '2021-11-19 22:28:31', '2021-12-19 15:18:18.391', 'Ha', '$2a$10$PgVPdZDSqEMLAMYFCHM7vubV8QUrB0.S90Dl4sSI.fMae5Fu97vr.', 'hapc', 3);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (4, '2021-12-09 21:27:05', '2021-12-19 15:25:24.629', 'Son Phan', '$2a$10$0ICKlrkdZohY/fKut2C/ruoo1ZS9ILm/zo5HlF4jvPWkRpnIU98V6', 'sonpc', 2);
INSERT INTO public.api_staff (id, created, updated, full_name, password, username, role_id) VALUES (9, '2021-12-16 15:57:15.465', '2021-12-19 15:25:52.212', 'Nguyen Van B', '$2a$10$77Ob8Nxw0aESopFNvZaYw.ODW8ciEIXxWCzSAP1ctstPDHaCL2.YW', 'wareahouseaccount', 2);


--
-- TOC entry 2632 (class 0 OID 34732)
-- Dependencies: 212
-- Data for Name: api_supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (1, 'supplier@nike.com', 'Nike', '0929315514');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (2, 'supplier@adidas.com', 'Adidas', '0123456789');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (3, 'supplier@converse.com', 'Converse', '092941124');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (4, 'supplier@converse.com', 'Converse', '092941124');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (5, 'supplier@converse.com', 'Converse', '092941124');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (6, 'daylanhacungcap@gmail.com', 'Nha cung cap b', '0929314415');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (9, 'daylanhacungcap@gmail.com', 'Nha cung cap d', '0929314415');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (7, 'daylanhacungcap@gmail.com', 'Nha cung cap a', '0929314415');
INSERT INTO public.api_supplier (id, email, name, phonenumber) VALUES (8, 'daylanhacungcap@gmail.com', 'Nha cung cap c', '0929314415');


--
-- TOC entry 2634 (class 0 OID 34740)
-- Dependencies: 214
-- Data for Name: api_supply; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (1, '2021-12-09 14:48:59.435', '2021-12-09 14:48:59.435', 6000000, 4, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (2, '2021-12-10 19:16:17.005', '2021-12-10 19:16:17.005', 6000000, 4, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (3, '2021-12-11 18:11:20.597', '2021-12-11 18:11:20.597', 6000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (4, '2021-12-11 18:11:48.08', '2021-12-11 18:11:48.08', 4000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (5, '2021-12-11 18:14:54.372', '2021-12-11 18:14:54.372', 32000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (6, '2021-12-11 18:25:11.066', '2021-12-11 18:25:11.066', 26800000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (7, '2021-12-11 18:28:22.375', '2021-12-11 18:28:22.375', 92000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (8, '2021-12-11 18:29:11.926', '2021-12-11 18:29:11.926', 32000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (9, '2021-12-11 18:32:48.085', '2021-12-11 18:32:48.085', 114200000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (10, '2021-12-15 04:53:50.246', '2021-12-15 04:53:50.246', 15000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (11, '2021-12-15 05:00:45.434', '2021-12-15 05:00:45.434', 15000000, 1, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (12, '2021-12-15 05:12:12.417', '2021-12-15 05:12:12.417', 400000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (13, '2021-12-15 05:14:02.356', '2021-12-15 05:14:02.356', 400000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (14, '2021-12-15 05:14:58.038', '2021-12-15 05:14:58.038', 400000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (15, '2021-12-15 05:15:23.72', '2021-12-15 05:15:23.72', 400000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (16, '2021-12-15 05:23:27.791', '2021-12-15 05:23:27.791', 342000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (17, '2021-12-15 05:24:06.843', '2021-12-15 05:24:06.843', 24000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (18, '2021-12-15 06:59:15.649', '2021-12-15 06:59:15.649', 60000000, 1, 2);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (19, '2021-12-16 07:43:19.414', '2021-12-16 07:43:19.414', 100000000, 1, 6);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (20, '2021-12-16 07:50:31.381', '2021-12-16 07:50:31.381', 1500000, 1, 6);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (21, '2021-12-16 16:01:37.983', '2021-12-16 16:01:37.983', 5000000, 9, 1);
INSERT INTO public.api_supply (id, created, updated, phonenumber, staff_id, supplier_id) VALUES (22, '2021-12-19 15:32:23.877', '2021-12-19 15:32:23.877', 40000000, 1, 1);


--
-- TOC entry 2636 (class 0 OID 34745)
-- Dependencies: 216
-- Data for Name: api_supply_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (1, 300000, 10, 10, 1);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (2, 300000, 10, 11, 1);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (3, 300000, 10, 12, 2);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (4, 300000, 10, 11, 2);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (5, 300000, 10, 12, 3);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (6, 300000, 10, 11, 3);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (7, 200000, 10, 13, 4);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (8, 200000, 10, 14, 4);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (9, 200000, 10, 15, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (10, 200000, 10, 16, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (11, 200000, 10, 17, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (12, 200000, 10, 18, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (13, 200000, 10, 19, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (14, 200000, 10, 20, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (15, 200000, 10, 21, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (16, 200000, 10, 22, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (17, 200000, 10, 23, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (18, 200000, 10, 24, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (19, 200000, 10, 25, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (20, 200000, 10, 26, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (21, 200000, 10, 27, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (22, 200000, 10, 28, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (23, 200000, 10, 29, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (24, 200000, 10, 30, 5);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (25, 200000, 7, 31, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (26, 200000, 3, 32, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (27, 200000, 9, 33, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (28, 200000, 4, 34, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (29, 200000, 9, 35, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (30, 200000, 10, 36, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (31, 200000, 5, 37, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (32, 200000, 10, 38, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (33, 200000, 7, 39, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (34, 200000, 6, 40, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (35, 200000, 5, 41, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (36, 200000, 4, 42, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (37, 200000, 6, 43, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (38, 200000, 3, 44, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (39, 200000, 7, 45, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (40, 200000, 10, 46, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (41, 200000, 8, 47, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (42, 200000, 5, 48, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (43, 200000, 9, 49, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (44, 200000, 7, 50, 6);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (45, 200000, 3, 51, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (46, 200000, 8, 52, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (47, 200000, 10, 53, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (48, 200000, 6, 54, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (49, 200000, 9, 55, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (50, 200000, 3, 56, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (51, 200000, 6, 37, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (52, 200000, 7, 57, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (53, 200000, 8, 58, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (54, 200000, 10, 59, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (55, 200000, 6, 60, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (56, 200000, 8, 61, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (57, 200000, 4, 62, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (58, 200000, 8, 63, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (59, 200000, 3, 64, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (60, 200000, 7, 65, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (61, 200000, 9, 66, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (62, 200000, 6, 67, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (63, 200000, 4, 68, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (64, 200000, 5, 69, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (65, 200000, 7, 70, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (66, 200000, 5, 71, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (67, 200000, 10, 72, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (68, 200000, 8, 73, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (69, 200000, 6, 74, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (70, 200000, 8, 75, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (71, 200000, 6, 76, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (72, 200000, 5, 77, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (73, 200000, 6, 78, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (74, 200000, 8, 79, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (75, 200000, 9, 80, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (76, 200000, 8, 81, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (77, 200000, 5, 82, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (78, 200000, 7, 83, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (79, 200000, 3, 84, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (80, 200000, 4, 85, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (81, 200000, 10, 86, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (82, 200000, 9, 87, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (83, 200000, 3, 88, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (84, 200000, 4, 89, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (85, 200000, 5, 90, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (86, 200000, 4, 91, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (87, 200000, 3, 92, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (88, 200000, 7, 93, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (89, 200000, 8, 94, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (90, 200000, 3, 95, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (91, 200000, 7, 96, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (92, 200000, 5, 97, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (93, 200000, 9, 98, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (94, 200000, 4, 99, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (95, 200000, 6, 100, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (96, 200000, 10, 101, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (97, 200000, 6, 102, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (98, 200000, 9, 103, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (99, 200000, 4, 104, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (100, 200000, 5, 105, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (101, 200000, 6, 106, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (102, 200000, 6, 107, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (103, 200000, 9, 108, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (104, 200000, 5, 109, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (105, 200000, 3, 110, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (106, 200000, 3, 111, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (107, 200000, 5, 112, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (108, 200000, 4, 113, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (109, 200000, 5, 114, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (110, 200000, 7, 115, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (111, 200000, 10, 116, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (112, 200000, 6, 117, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (113, 200000, 3, 118, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (114, 200000, 9, 119, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (115, 200000, 3, 120, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (116, 200000, 8, 121, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (117, 200000, 4, 122, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (118, 200000, 8, 123, 7);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (119, 200000, 10, 15, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (120, 200000, 10, 16, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (121, 200000, 10, 17, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (122, 200000, 10, 18, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (123, 200000, 10, 19, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (124, 200000, 10, 20, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (125, 200000, 10, 21, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (126, 200000, 10, 22, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (127, 200000, 10, 23, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (128, 200000, 10, 24, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (129, 200000, 10, 25, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (130, 200000, 10, 26, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (131, 200000, 10, 27, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (132, 200000, 10, 28, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (133, 200000, 10, 29, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (134, 200000, 10, 30, 8);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (135, 200000, 3, 124, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (136, 200000, 10, 125, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (137, 200000, 3, 126, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (138, 200000, 8, 127, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (139, 200000, 3, 128, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (140, 200000, 9, 129, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (141, 200000, 5, 130, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (142, 200000, 3, 131, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (143, 200000, 9, 132, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (144, 200000, 9, 133, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (145, 200000, 4, 134, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (146, 200000, 5, 135, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (147, 200000, 10, 136, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (148, 200000, 10, 137, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (149, 200000, 3, 138, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (150, 200000, 7, 36, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (151, 200000, 8, 139, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (152, 200000, 4, 140, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (153, 200000, 7, 141, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (154, 200000, 8, 142, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (155, 200000, 3, 143, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (156, 200000, 3, 144, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (157, 200000, 7, 145, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (158, 200000, 9, 146, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (159, 200000, 5, 147, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (160, 200000, 9, 148, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (161, 200000, 8, 149, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (162, 200000, 10, 67, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (163, 200000, 5, 150, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (164, 200000, 3, 69, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (165, 200000, 9, 151, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (166, 200000, 9, 152, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (167, 200000, 10, 153, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (168, 200000, 8, 154, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (169, 200000, 7, 155, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (170, 200000, 3, 156, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (171, 200000, 8, 157, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (172, 200000, 4, 158, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (173, 200000, 6, 159, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (174, 200000, 9, 160, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (175, 200000, 4, 161, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (176, 200000, 8, 162, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (177, 200000, 10, 82, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (178, 200000, 5, 163, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (179, 200000, 5, 164, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (180, 200000, 5, 165, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (181, 200000, 10, 166, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (182, 200000, 8, 167, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (183, 200000, 10, 168, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (184, 200000, 10, 169, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (185, 200000, 6, 170, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (186, 200000, 6, 171, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (187, 200000, 5, 172, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (188, 200000, 3, 173, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (189, 200000, 10, 174, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (190, 200000, 8, 175, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (191, 200000, 6, 96, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (192, 200000, 6, 176, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (193, 200000, 10, 177, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (194, 200000, 4, 99, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (195, 200000, 10, 100, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (196, 200000, 9, 178, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (197, 200000, 4, 179, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (198, 200000, 6, 180, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (199, 200000, 8, 181, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (200, 200000, 7, 182, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (201, 200000, 3, 183, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (202, 200000, 3, 184, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (203, 200000, 6, 185, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (204, 200000, 6, 186, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (205, 200000, 9, 187, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (206, 200000, 10, 188, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (207, 200000, 5, 189, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (208, 200000, 10, 190, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (209, 200000, 10, 191, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (210, 200000, 9, 192, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (211, 200000, 3, 193, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (212, 200000, 9, 194, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (213, 200000, 9, 195, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (214, 200000, 5, 196, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (215, 200000, 7, 197, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (216, 200000, 8, 198, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (217, 200000, 10, 199, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (218, 200000, 3, 200, 9);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (219, 1500000, 10, 201, 10);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (220, 1500000, 10, 201, 11);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (221, 2000000, 200, 202, 12);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (222, 2000000, 200, 202, 13);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (223, 2000000, 200, 202, 14);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (224, 2000000, 200, 202, 15);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (225, 1500000, 200, 203, 16);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (226, 2100000, 20, 204, 16);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (227, 1200000, 20, 205, 17);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (228, 1500000, 20, 206, 18);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (229, 1500000, 20, 207, 18);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (230, 500000, 200, 208, 19);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (231, 100000, 5, 209, 20);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (232, 100000, 10, 210, 20);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (233, 500000, 10, 211, 21);
INSERT INTO public.api_supply_item (id, price, quantity, product_variant_id, supply_id) VALUES (234, 2000000, 20, 212, 22);


--
-- TOC entry 2641 (class 0 OID 34935)
-- Dependencies: 221
-- Data for Name: api_used_voucher; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (1, '2021-12-16 16:52:20.248', '2021-12-16 16:52:20.248', 2, 9);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (2, '2021-12-16 17:03:22.753', '2021-12-16 17:03:22.753', 2, 10);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (3, '2021-12-16 17:11:00.222', '2021-12-16 17:11:00.222', 1, 11);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (4, '2021-12-17 11:42:51.369', '2021-12-17 11:42:51.369', 1, 10);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (5, '2021-12-17 17:35:09.479', '2021-12-17 17:35:09.479', 1, 12);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (6, '2021-12-17 17:52:41.132', '2021-12-17 17:52:41.132', 3, 12);
INSERT INTO public.api_used_voucher (id, created, updated, user_id, voucher_id) VALUES (7, '2021-12-20 07:07:09.995', '2021-12-20 07:07:09.995', 3, 15);


--
-- TOC entry 2638 (class 0 OID 34750)
-- Dependencies: 218
-- Data for Name: api_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_user (id, created, updated, email, fullname, password, phonenumber) VALUES (2, '2021-11-19 17:03:18.099', '2021-11-19 17:03:18.099', 'phancongson24@gmail.com', 'Phan Cong Son', '$2a$10$v0bUpQgausW/6LjQEBLDi.QO74wJIP4UTcC8L8M.z9V4V/sTpqfjK', '0929315514');
INSERT INTO public.api_user (id, created, updated, email, fullname, password, phonenumber) VALUES (3, '2021-11-19 17:43:36.927', '2021-11-19 17:43:36.927', 'phuongnam2810@gmail.com', 'Nguyen Minh Phuong Nam', '$2a$10$m/MSQD4jDn2oyqBq76ZXhu7QnlscdkDio0GxlPy2ClnxxR0pFOXHe', '0929315555');
INSERT INTO public.api_user (id, created, updated, email, fullname, password, phonenumber) VALUES (1, '2021-11-19 15:45:23', '2021-11-19 15:45:23', 'phancongha24@gmail.com', 'Ha Phan', '$2a$10$m/MSQD4jDn2oyqBq76ZXhu7QnlscdkDio0GxlPy2ClnxxR0pFOXHe', '0929315514');
INSERT INTO public.api_user (id, created, updated, email, fullname, password, phonenumber) VALUES (4, '2021-12-15 06:37:47.021', '2021-12-15 06:37:47.021', 'hosydat@gmail.com', 'Ho Sy Dat', '$2a$10$ye7ainGeFbHuRDVyOHAeFuJ.2ov5tgwm3LnHgriQ1riLaABPsJew6', '0929315514');


--
-- TOC entry 2643 (class 0 OID 34943)
-- Dependencies: 223
-- Data for Name: api_voucher; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (9, '2021-12-16 15:36:01.255', '2021-12-16 16:52:20.276', 'sago1', 'Mã giảm giá 100%, tối đa 150k', 100, '2021-12-16 15:30:00', 'Mã giảm giá 100%, tối đa 150k', 14, 150000, 15);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (10, '2021-12-16 17:03:13.639', '2021-12-17 11:42:51.405', 'sago2', 'Mã giảm giá 100%, tối đa 250k', 100, '2021-12-17 15:30:00', 'Mã giảm giá 100%, tối đa 250k', 8, 250000, 10);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (11, '2021-12-16 17:05:53.635', '2021-12-16 17:11:00.225', 'sago3', 'Mã giảm giá 100%, tối đa 350k', 100, '2021-12-17 15:30:00', 'Mã giảm giá 100%, tối đa 350k', 0, 300000, 1);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (12, '2021-12-17 15:30:09.246', '2021-12-17 17:52:41.147', 'sago4', 'Mã giảm giá 100%, tối đa 400k', 100, '2021-12-23 16:59:59', 'Mã giảm giá 100%, tối đa 400k', 18, 400000, 20);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (19, '2021-12-19 13:33:55.756', '2021-12-19 13:38:23.153', 'newyear', 'Discount', 100, '2022-01-31 13:38:19', 'New year', 20, 300000, 20);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (14, '2021-12-18 08:25:48.045', '2021-12-18 08:25:48.045', 'specialcode', 'Giảm tận 300k, ngại ngần gì không mua ngay hả thí chủ.', 100, '2021-12-31 08:01:07', 'Sago Special Code', 200, 300000, 200);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (17, '2021-12-18 08:32:06.74', '2021-12-18 19:19:46.977', 'supersale', 'Super sale ', 100, '2022-11-29 17:00:00', 'Super sale', 5, 300000, 10);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (13, '2021-12-18 08:05:18.12', '2021-12-18 08:05:18.12', 'benlau', 'Giảm tận 300k, ngại ngần gì không mua ngay hả thí chủ.', 100, '2021-12-31 08:01:07', 'Khuyến mãi 25', 200, 300000, 200);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (16, '2021-12-18 08:31:07.748', '2021-12-20 07:04:06.391', 'christmas', 'Christmas is coming to town', 100, '2021-12-24 17:00:00', 'Christmas is coming to town', 9, 200000, 150);
INSERT INTO public.api_voucher (id, created, updated, code, description, discount_amount, expired_date, name, quantity, max_discount_amount, total) VALUES (15, '2021-12-18 08:26:48.223', '2021-12-20 07:07:10.007', 'shibainu', 'Giảm tận 400k, ngại ngần gì không mua ngay hả thí chủ.', 100, '2022-03-14 08:26:45', 'Shibainu', 199, 400000, 200);


--
-- TOC entry 2669 (class 0 OID 0)
-- Dependencies: 186
-- Name: api_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_brand_id_seq', 19, true);


--
-- TOC entry 2670 (class 0 OID 0)
-- Dependencies: 188
-- Name: api_color_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_color_id_seq', 10, true);


--
-- TOC entry 2671 (class 0 OID 0)
-- Dependencies: 190
-- Name: api_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_order_id_seq', 134, true);


--
-- TOC entry 2672 (class 0 OID 0)
-- Dependencies: 192
-- Name: api_order_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_order_item_id_seq', 183, true);


--
-- TOC entry 2673 (class 0 OID 0)
-- Dependencies: 194
-- Name: api_order_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_order_status_id_seq', 5, true);


--
-- TOC entry 2674 (class 0 OID 0)
-- Dependencies: 196
-- Name: api_orderhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_orderhistory_id_seq', 232, true);


--
-- TOC entry 2675 (class 0 OID 0)
-- Dependencies: 199
-- Name: api_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_product_id_seq', 150, true);


--
-- TOC entry 2676 (class 0 OID 0)
-- Dependencies: 201
-- Name: api_productimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_productimage_id_seq', 354, true);


--
-- TOC entry 2677 (class 0 OID 0)
-- Dependencies: 203
-- Name: api_productvariant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_productvariant_id_seq', 212, true);


--
-- TOC entry 2678 (class 0 OID 0)
-- Dependencies: 205
-- Name: api_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_review_id_seq', 15, true);


--
-- TOC entry 2679 (class 0 OID 0)
-- Dependencies: 207
-- Name: api_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_role_id_seq', 1, false);


--
-- TOC entry 2680 (class 0 OID 0)
-- Dependencies: 209
-- Name: api_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_size_id_seq', 8, true);


--
-- TOC entry 2681 (class 0 OID 0)
-- Dependencies: 211
-- Name: api_staff_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_staff_id_seq', 9, true);


--
-- TOC entry 2682 (class 0 OID 0)
-- Dependencies: 213
-- Name: api_supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_supplier_id_seq', 14, true);


--
-- TOC entry 2683 (class 0 OID 0)
-- Dependencies: 215
-- Name: api_supply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_supply_id_seq', 22, true);


--
-- TOC entry 2684 (class 0 OID 0)
-- Dependencies: 217
-- Name: api_supply_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_supply_item_id_seq', 234, true);


--
-- TOC entry 2685 (class 0 OID 0)
-- Dependencies: 220
-- Name: api_used_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_used_voucher_id_seq', 7, true);


--
-- TOC entry 2686 (class 0 OID 0)
-- Dependencies: 219
-- Name: api_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_user_id_seq', 4, true);


--
-- TOC entry 2687 (class 0 OID 0)
-- Dependencies: 222
-- Name: api_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.api_voucher_id_seq', 22, true);


--
-- TOC entry 2415 (class 2606 OID 34776)
-- Name: api_brand api_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_brand
    ADD CONSTRAINT api_brand_pkey PRIMARY KEY (id);


--
-- TOC entry 2418 (class 2606 OID 34778)
-- Name: api_brand api_brand_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_brand
    ADD CONSTRAINT api_brand_slug_key UNIQUE (slug);


--
-- TOC entry 2420 (class 2606 OID 34780)
-- Name: api_color api_color_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_color
    ADD CONSTRAINT api_color_pkey PRIMARY KEY (id);


--
-- TOC entry 2424 (class 2606 OID 34782)
-- Name: api_order_item api_order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_item
    ADD CONSTRAINT api_order_item_pkey PRIMARY KEY (id);


--
-- TOC entry 2422 (class 2606 OID 34784)
-- Name: api_order api_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order
    ADD CONSTRAINT api_order_pkey PRIMARY KEY (id);


--
-- TOC entry 2426 (class 2606 OID 34786)
-- Name: api_order_status api_order_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_status
    ADD CONSTRAINT api_order_status_pkey PRIMARY KEY (id);


--
-- TOC entry 2428 (class 2606 OID 34788)
-- Name: api_orderhistory api_orderhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_orderhistory
    ADD CONSTRAINT api_orderhistory_pkey PRIMARY KEY (id);


--
-- TOC entry 2430 (class 2606 OID 34790)
-- Name: api_payment api_payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_payment
    ADD CONSTRAINT api_payment_pkey PRIMARY KEY (id);


--
-- TOC entry 2433 (class 2606 OID 34792)
-- Name: api_product api_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_product
    ADD CONSTRAINT api_product_pkey PRIMARY KEY (id);


--
-- TOC entry 2436 (class 2606 OID 34794)
-- Name: api_product api_product_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_product
    ADD CONSTRAINT api_product_slug_key UNIQUE (slug);


--
-- TOC entry 2438 (class 2606 OID 34796)
-- Name: api_productimage api_productimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productimage
    ADD CONSTRAINT api_productimage_pkey PRIMARY KEY (id);


--
-- TOC entry 2441 (class 2606 OID 34798)
-- Name: api_productvariant api_productvariant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productvariant
    ADD CONSTRAINT api_productvariant_pkey PRIMARY KEY (id);


--
-- TOC entry 2445 (class 2606 OID 34800)
-- Name: api_review api_review_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_review
    ADD CONSTRAINT api_review_pkey PRIMARY KEY (id);


--
-- TOC entry 2447 (class 2606 OID 34802)
-- Name: api_role api_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_role
    ADD CONSTRAINT api_role_pkey PRIMARY KEY (id);


--
-- TOC entry 2449 (class 2606 OID 34804)
-- Name: api_size api_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_size
    ADD CONSTRAINT api_size_pkey PRIMARY KEY (id);


--
-- TOC entry 2451 (class 2606 OID 34806)
-- Name: api_staff api_staff_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_staff
    ADD CONSTRAINT api_staff_pkey PRIMARY KEY (id);


--
-- TOC entry 2455 (class 2606 OID 34808)
-- Name: api_supplier api_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supplier
    ADD CONSTRAINT api_supplier_pkey PRIMARY KEY (id);


--
-- TOC entry 2459 (class 2606 OID 34810)
-- Name: api_supply_item api_supply_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply_item
    ADD CONSTRAINT api_supply_item_pkey PRIMARY KEY (id);


--
-- TOC entry 2457 (class 2606 OID 34812)
-- Name: api_supply api_supply_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply
    ADD CONSTRAINT api_supply_pkey PRIMARY KEY (id);


--
-- TOC entry 2465 (class 2606 OID 34940)
-- Name: api_used_voucher api_used_voucher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_used_voucher
    ADD CONSTRAINT api_used_voucher_pkey PRIMARY KEY (id);


--
-- TOC entry 2461 (class 2606 OID 34814)
-- Name: api_user api_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_user
    ADD CONSTRAINT api_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2467 (class 2606 OID 34951)
-- Name: api_voucher api_voucher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_voucher
    ADD CONSTRAINT api_voucher_pkey PRIMARY KEY (id);


--
-- TOC entry 2463 (class 2606 OID 34816)
-- Name: api_user uk5x3eommwi436cv1hemmxd8twm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_user
    ADD CONSTRAINT uk5x3eommwi436cv1hemmxd8twm UNIQUE (email);


--
-- TOC entry 2453 (class 2606 OID 34818)
-- Name: api_staff ukdbfcapvlafku32p7mmc2oqcvb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_staff
    ADD CONSTRAINT ukdbfcapvlafku32p7mmc2oqcvb UNIQUE (username);


--
-- TOC entry 2416 (class 1259 OID 34819)
-- Name: api_brand_slug_e368d547_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_brand_slug_e368d547_like ON public.api_brand USING btree (slug varchar_pattern_ops);


--
-- TOC entry 2431 (class 1259 OID 34820)
-- Name: api_product_brand_id_id_62fbb9a3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_product_brand_id_id_62fbb9a3 ON public.api_product USING btree (brand_id);


--
-- TOC entry 2434 (class 1259 OID 34821)
-- Name: api_product_slug_45947488_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_product_slug_45947488_like ON public.api_product USING btree (slug varchar_pattern_ops);


--
-- TOC entry 2439 (class 1259 OID 34822)
-- Name: api_productimage_product_id_id_f467deaf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_productimage_product_id_id_f467deaf ON public.api_productimage USING btree (product_id);


--
-- TOC entry 2442 (class 1259 OID 34823)
-- Name: api_productvariant_product_id_id_bb2f33a0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_productvariant_product_id_id_bb2f33a0 ON public.api_productvariant USING btree (product_id);


--
-- TOC entry 2443 (class 1259 OID 34824)
-- Name: api_productvariant_size_id_id_efc16dec; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_productvariant_size_id_id_efc16dec ON public.api_productvariant USING btree (size_id);


--
-- TOC entry 2475 (class 2606 OID 34825)
-- Name: api_product api_product_brand_id_c4e9b7a1_fk_api_brand_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_product
    ADD CONSTRAINT api_product_brand_id_c4e9b7a1_fk_api_brand_id FOREIGN KEY (brand_id) REFERENCES public.api_brand(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2476 (class 2606 OID 34830)
-- Name: api_productimage api_productimage_product_id_5020b937_fk_api_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productimage
    ADD CONSTRAINT api_productimage_product_id_5020b937_fk_api_product_id FOREIGN KEY (product_id) REFERENCES public.api_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2477 (class 2606 OID 34835)
-- Name: api_productvariant api_productvariant_product_id_bd663ca3_fk_api_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productvariant
    ADD CONSTRAINT api_productvariant_product_id_bd663ca3_fk_api_product_id FOREIGN KEY (product_id) REFERENCES public.api_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2478 (class 2606 OID 34840)
-- Name: api_productvariant api_productvariant_size_id_d2b82143_fk_api_size_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productvariant
    ADD CONSTRAINT api_productvariant_size_id_d2b82143_fk_api_size_id FOREIGN KEY (size_id) REFERENCES public.api_size(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2474 (class 2606 OID 34845)
-- Name: api_payment fk2ab4qwh0iatq4cdx13qp82f95; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_payment
    ADD CONSTRAINT fk2ab4qwh0iatq4cdx13qp82f95 FOREIGN KEY (order_id) REFERENCES public.api_order(id);


--
-- TOC entry 2471 (class 2606 OID 34850)
-- Name: api_orderhistory fkaocod4lh6xq9b3prlg0hcqxw5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_orderhistory
    ADD CONSTRAINT fkaocod4lh6xq9b3prlg0hcqxw5 FOREIGN KEY (staff_id) REFERENCES public.api_staff(id);


--
-- TOC entry 2484 (class 2606 OID 34855)
-- Name: api_supply_item fkaplkwlcsgnmyfucc0iqtx950e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply_item
    ADD CONSTRAINT fkaplkwlcsgnmyfucc0iqtx950e FOREIGN KEY (supply_id) REFERENCES public.api_supply(id);


--
-- TOC entry 2481 (class 2606 OID 34860)
-- Name: api_staff fkb94ih605bcxus9rkeps7f6ehh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_staff
    ADD CONSTRAINT fkb94ih605bcxus9rkeps7f6ehh FOREIGN KEY (role_id) REFERENCES public.api_role(id);


--
-- TOC entry 2487 (class 2606 OID 34957)
-- Name: api_used_voucher fkd9ifh1nsq3blgbm0u3vde97wc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_used_voucher
    ADD CONSTRAINT fkd9ifh1nsq3blgbm0u3vde97wc FOREIGN KEY (voucher_id) REFERENCES public.api_voucher(id);


--
-- TOC entry 2480 (class 2606 OID 34865)
-- Name: api_review fkenl8p0il7frb1x2ggw6wv28sc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_review
    ADD CONSTRAINT fkenl8p0il7frb1x2ggw6wv28sc FOREIGN KEY (product_id) REFERENCES public.api_product(id);


--
-- TOC entry 2469 (class 2606 OID 34870)
-- Name: api_order_item fkfpy88t8lg9bj45f5ct558pyii; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_item
    ADD CONSTRAINT fkfpy88t8lg9bj45f5ct558pyii FOREIGN KEY (product_variant_id) REFERENCES public.api_productvariant(id);


--
-- TOC entry 2472 (class 2606 OID 34875)
-- Name: api_orderhistory fkg0s13rb0wug4o9sctct7l2wvy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_orderhistory
    ADD CONSTRAINT fkg0s13rb0wug4o9sctct7l2wvy FOREIGN KEY (orderstatus_id) REFERENCES public.api_order_status(id);


--
-- TOC entry 2479 (class 2606 OID 34880)
-- Name: api_productvariant fki57pv2m4nu38ywdpy4dws40hp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_productvariant
    ADD CONSTRAINT fki57pv2m4nu38ywdpy4dws40hp FOREIGN KEY (color_id) REFERENCES public.api_color(id);


--
-- TOC entry 2485 (class 2606 OID 34885)
-- Name: api_supply_item fkkbutc6jq6trc0l7dkjoppcst1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply_item
    ADD CONSTRAINT fkkbutc6jq6trc0l7dkjoppcst1 FOREIGN KEY (product_variant_id) REFERENCES public.api_productvariant(id);


--
-- TOC entry 2486 (class 2606 OID 34952)
-- Name: api_used_voucher fkke5hekj88238hr9mim4c0b7j1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_used_voucher
    ADD CONSTRAINT fkke5hekj88238hr9mim4c0b7j1 FOREIGN KEY (user_id) REFERENCES public.api_user(id);


--
-- TOC entry 2482 (class 2606 OID 34890)
-- Name: api_supply fkmwr4h8ob942uqjfaffukdmra6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply
    ADD CONSTRAINT fkmwr4h8ob942uqjfaffukdmra6 FOREIGN KEY (supplier_id) REFERENCES public.api_supplier(id);


--
-- TOC entry 2468 (class 2606 OID 34895)
-- Name: api_order fkoaksyrc34rcwb79ownui1myo7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order
    ADD CONSTRAINT fkoaksyrc34rcwb79ownui1myo7 FOREIGN KEY (user_id) REFERENCES public.api_user(id);


--
-- TOC entry 2473 (class 2606 OID 34900)
-- Name: api_orderhistory fkqugm5pallng9cn569253fqjnc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_orderhistory
    ADD CONSTRAINT fkqugm5pallng9cn569253fqjnc FOREIGN KEY (order_id) REFERENCES public.api_order(id);


--
-- TOC entry 2483 (class 2606 OID 34905)
-- Name: api_supply fkrod9yuliq2ih7upca2k9gbgdx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_supply
    ADD CONSTRAINT fkrod9yuliq2ih7upca2k9gbgdx FOREIGN KEY (staff_id) REFERENCES public.api_staff(id);


--
-- TOC entry 2470 (class 2606 OID 34910)
-- Name: api_order_item fktmkilvutm5rhpdvr57dd5kbd5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.api_order_item
    ADD CONSTRAINT fktmkilvutm5rhpdvr57dd5kbd5 FOREIGN KEY (order_id) REFERENCES public.api_order(id);


-- Completed on 2021-12-21 13:51:17 +07

--
-- PostgreSQL database dump complete
--

