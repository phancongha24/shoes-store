import { configureStore } from "@reduxjs/toolkit";
import { cartSlice } from "../pages/CartPage/cartSlice";
import { wishlistSlice } from "../pages/WishlistPage/wishlistSlice";

export const store = configureStore({
  reducer: {
    cart: cartSlice.reducer,
    wishlist: wishlistSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
