import axios, { AxiosRequestConfig } from 'axios';
let options: AxiosRequestConfig = {}
let apiUrl = process.env.REACT_APP_API_ENDPOINT;
const userToken = localStorage.getItem("access_token");
const axiosClient = {
  get: (path: string) => {
    options = {
      method: 'GET',
      url: `${apiUrl}${path}`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`,
        'Content-Type': 'application/json',
      },
    };
    return axios.request(options).then((response) => {
      const { data } = response;
      return data;
    }).catch(function (error) {
      console.error(error);
    });
  },
  post: (path: string, formData: object) => {
    options = {
      method: 'POST',
      url: `${apiUrl}${path}`,
      data: formData,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`,
        'Content-Type': 'application/json',
      },
    };
    return axios(options).then((res) => {
      return res;
    })
      .catch((err) => {
        return err.response;
      })
  },
  getAreas: (path: string) => {
    options = {
      method: 'GET',
      url: `https://api.mysupership.vn/v1/partner/areas${path}`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    return axios.request(options).then((response) => {
      const { data } = response;
      return data;
    })
  },
  put: (path: string, formData: any) => {
    let headers = {
      'Authorization': `Bearer ${userToken}`
    }
    return axios.put(`${apiUrl}${path}`, formData, {
      headers: headers
    }).then((res) => {
      return res;
    })
      .catch((err) => {
        if (axios.isCancel(err)) {
          console.log('Request canceled', err.message);
        } else {
          return err.response
        }
      })
  },

}

export default axiosClient;