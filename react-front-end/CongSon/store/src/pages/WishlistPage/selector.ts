import { createSelector } from "@reduxjs/toolkit";

const wishlistItemSelector = (state: any) => state.wishlist.wishList;

export const wishlistItemsCountSelector = createSelector(
  wishlistItemSelector,
  (wishlistItems) => wishlistItems.length
);
