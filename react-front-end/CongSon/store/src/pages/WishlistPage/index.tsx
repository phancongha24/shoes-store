import * as React from 'react';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';
import { Wishlist } from './Wishlist';

export interface TrackOrderPageProps {
}

export function WishlistPage(props: TrackOrderPageProps) {
    return (
        <>
            <Header />
            <Wishlist />
            <Footer />
        </>
    );
}
