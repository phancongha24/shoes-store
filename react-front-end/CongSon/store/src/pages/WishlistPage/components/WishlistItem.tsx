import * as React from 'react';
import { Link } from 'react-router-dom';


interface IProduct{
    id:number,
    name:string,
    price:number,
    image:string

}

export interface WishlistItemProps {
    product:IProduct,
    handleProductRemove: Function
}

export function WishlistItem({product, handleProductRemove}: WishlistItemProps) {
    const handleDeleteClick = (id:number) => {
        handleProductRemove(id);
    }
    return (
        <div className='flex flex-col mb-10' key={product.id}>
            <div className="relative">
                <img className='w-full h-96' src={product.image} alt="bag" />
                <button onClick={() => handleDeleteClick(product.id)} aria-label="close" className="top-4 right-4 absolute  p-1.5 bg-gray-800 text-white">
                    <svg className="fil-current" width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 1L1 13" stroke="currentColor" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M1 1L13 13" stroke="currentColor" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </button>
            </div>
            <div className="mt-6 flex justify-between items-center">
                <div className="flex justify-center items-center">
                    <Link to={`/products/${product.id}`} className="tracking-tight text-xl font-semibold leading-6 text-gray-800">{product.name}</Link>
                </div>
            </div>
            <div className="mt-6">
                <p className="tracking-tight text-base font-medium leading-4 text-gray-800">{product.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ")}</p>
            </div>
        </div>
    );
}
