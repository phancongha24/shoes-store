import * as React from 'react';
import { useHistory } from 'react-router-dom';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';

export interface SuccessPageProps {
}

export function SuccessPage(props: SuccessPageProps) {
    const history = useHistory();
    const handleShoppingClick = () => {
        history.push("/products");
    }
    const handleTrackYourOrderClick = () => {
        history.push("/orders");
    }
    return (
        <>
            <Header />
            <div className="flex flex-col items-center">
                <div className="w-1/2">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-13 w-13 text-green-600" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clipRule="evenodd" />
                    </svg>
                </div>
                <h2 className="text-7xl text-green-600 mb-32"> Success! </h2>
                <div className="flex justify-between w-1/2">
                    <button onClick={handleShoppingClick} className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded-full">
                        Continue Shopping
                    </button>
                    <button onClick={handleTrackYourOrderClick} className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded-full">
                        Track Your Order
                    </button>
                </div>
            </div>
            <Footer />
        </>

    );
}
