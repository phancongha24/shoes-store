import * as React from 'react';
import { Accordion } from '../../components/Common/Accordion';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';

export interface FaqPageProps {
}

export function FaqPage(props: FaqPageProps) {
    const [showDiscription, setShowDiscription] = React.useState(false);
    const [showPolicy, setShowPolicy] = React.useState(false);
    const [showReviews, setShowReviews] = React.useState(false);
    const handleDiscriptionToggle = (visible: boolean) => {
        setShowDiscription(visible);
    }
    const handleReviewsToggle = (visible: boolean) => {
        setShowReviews(visible);
    }
    const handlePolicyToggle = (visible: boolean) => {
        setShowPolicy(visible);
    }
    return (
        <>

            <Header />
            <div className='mx-auto md:w-1/2 px-4 md:px-8 2xl:px-16'>
                <Accordion isShow={showDiscription} handleToggleChange={handleDiscriptionToggle} title="How to contact with Customer Service?" content="Our Customer Experience Team is available 7 days a week and we offer 2 ways to get in contact.Email and Chat . We try to reply quickly, so you need not to wait too long for a response!." />
                <Accordion isShow={showPolicy} handleToggleChange={handlePolicyToggle} title="How do I create an account?" content="If you want to open an account for personal use you can do it over the phone or online. Opening an account online should only take a few minutes." />
                <Accordion isShow={showReviews} handleToggleChange={handleReviewsToggle} title="How Do I Get Free Shipping On Sago Orders?" content="If you're not already a Member, join Sago today for free. Sign in to your Member profile when you shop on sago.com and we'll automatically apply free standard shipping at checkout on every order (no promo code required)." />

            </div>
            <Footer />
        </>
    );
}
