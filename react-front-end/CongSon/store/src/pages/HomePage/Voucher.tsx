import * as React from 'react';
import { Link } from 'react-router-dom';

export interface VoucherProps {
}

export function Voucher(props: VoucherProps) {
    return (
        <section className="text-white bg-gray-900" role="banner">
            <div className="max-w-screen-xl px-4 py-24 mx-auto sm:px-6 lg:px-8 sm:py-36 lg:h-80 lg:flex lg:items-center">
                <div className="max-w-3xl mx-auto text-center">
                    <h1
                        className="text-3xl font-extrabold text-transparent sm:text-6xl bg-clip-text bg-gradient-to-r from-purple-500 to-red-400"
                    >
                        Buy for less!
                    </h1>

                    <p className="max-w-xl mx-auto mt-6 text-lg">
                        Find the latest Sago promo codes and discount vouchers here.
                    </p>

                    <div className="mt-8 sm:justify-center sm:flex">
                        <Link to="/vouchers" className="block px-5 py-3 mt-3 font-medium border border-purple-600 rounded-lg sm:ml-3 sm:mt-0 hover:border-purple-500">
                            Get voucher
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    );
}
