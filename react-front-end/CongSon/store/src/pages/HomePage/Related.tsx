import * as React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { toast } from 'react-toastify';
import axiosClient from '../../api/axios-client';
import { ProductCard } from '../ProductPage/components/ProductCard';
import { data } from '../ProductPage/data';
import './css/Related.css';
export interface RelatedProductProps {
}


export function RelatedProduct(props: RelatedProductProps) {


    const [ bestSellerList, setBestSeller ] = React.useState([]);
    const [ arrivalProductList, setArrivalProductList ] = React.useState([]);
    const [ productList, setProductList ] = React.useState([]);


    const fetchData = async () => {
        try {
            const result:any = await axiosClient.get("/products?limit=40");
            let  newProductList = result;
            newProductList.forEach((product:any) => product.slug = `/products/${product.slug}`);
            let newBestSellerList = newProductList.filter((product:any) => product.sold > 2);
            let newArrivalProductList = newProductList.sort((a:any,b:any) => b.inStock - a.inStock);
            setBestSeller(newBestSellerList);
            setArrivalProductList(newArrivalProductList);
            setProductList(newProductList);
        } catch (error) {
            toast.error("PLEASE TRY AGAIN");
        }
    }

    React.useEffect(() => {
        fetchData();
    },[])
    return (
        <div className="mt-20 max-w-screen-xl mx-auto">
            <h2 className="font-bold text-3xl ml-4 my-5 text-my-color uppercase tracking-wider">Featured Products</h2>
            <Tabs>
                <TabList className="flex md:justify-end">
                    <Tab>ALL</Tab>
                    <Tab>BEST SELLERS</Tab>
                    <Tab>THE GREATEST</Tab>
                </TabList>

                <TabPanel>
                    <div className="grid grid-flow-row grid-cols-1 md:grid-cols-4 md:gap-x-5">
                        {productList.map(item => <ProductCard product={item} />)}
                    </div>
                </TabPanel>
                <TabPanel>
                    <div className="grid grid-flow-row grid-cols-1 md:grid-cols-4 md:gap-x-5">
                        {bestSellerList.map(item => <ProductCard product={item} />)}
                    </div>
                </TabPanel>
                <TabPanel>
                    <div className="grid grid-flow-row grid-cols-1 md:grid-cols-4 md:gap-x-5">
                        {arrivalProductList.map(item => <ProductCard product={item} />)}
                    </div>
                </TabPanel>
            </Tabs>
        </div>
    );
}
