import * as React from 'react';
import { useHistory } from 'react-router-dom';

export interface CategoryProps {
}

export function Category(props: CategoryProps) {
    const history = useHistory();
    const handleCateClick = (params:any) => {
        history.push({
            pathname: `/products`,
            search: params
        });
    }
    return (
        <div className="pb-16 max-w-screen-xl mx-auto">
            <div className="flex justify-center items-center">
                <div className="2xl:mx-auto 2xl:container py-12 px-4 sm:px-6 xl:px-20 2xl:px-0 w-full">
                    <div className="flex flex-col jusitfy-center items-center space-y-10">
                        <div className="flex flex-col justify-center items-center space-y-2">
                            <p className="text-xl leading-5 text-gray-600">2021 Trendsetters</p>
                            <h1 className="text-3xl xl:text-4xl font-semibold leading-7 xl:leading-9 text-gray-800">Shop By Brands</h1>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 md:gap-x-8 w-full">
                            <div className="relative group flex justify-center items-center h-full w-full">
                                <img className="object-center object-cover h-full w-full" src="https://cdn.shopify.com/s/files/1/0255/3220/3107/products/basket-puma-suede-classic-bboy-fabulousflame-scarlet-50-ans-65362-02-4.jpg" alt="girl-image" />
                                <button onClick={() => handleCateClick('?brands=puma')} className="bottom-4 z-10 absolute text-base font-medium leading-none text-gray-50 py-3 w-36 bg-black">Puma</button>

                            </div>
                            <div className="flex flex-col space-y-4 md:space-y-8 mt-4 md:mt-0">
                                <div className="relative group flex justify-center items-center h-full w-full">
                                    <img className="object-center object-cover h-full w-full" src="https://static.nike.com/a/images/f_auto/dpr_1.3,cs_srgb/w_696,c_limit/d3cc9625-d91e-4ca4-983d-097e4b9022f7/men-s-shoes-clothing-accessories.jpg" alt="shoe-image" />
                                    <button onClick={() => handleCateClick('?brands=nike')} className="bottom-4 z-10 absolute text-base font-medium leading-none text-gray-50 py-3 w-36 bg-black">Nike</button>

                                </div>
                                <div className="relative group flex justify-center items-center h-full w-full">
                                    <img className="object-center object-cover h-full w-full" src="https://assets.adidas.com/images/w_600,f_auto,q_auto/b7beee7c32d4438aaba3acb6001c2e7b_9366/Giay_Co_Thap_Forum_trang_FY7757_01_standard.jpg" alt="watch-image" />
                                    <button onClick={() => handleCateClick('?brands=adidas')} className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-400 bottom-4 z-10 absolute text-base font-medium leading-none text-gray-50 py-3 w-36 bg-black">Adidas</button>
                                </div>
                            </div>
                            <div className="relative group flex justify-center items-center h-full w-full">
                                <img className="object-center object-cover h-full w-full opacity-90" src="https://giaysecondhand.com/wp-content/uploads/2019/10/nhung-mau-giay-converse-den-cuc-chat.png" alt="girl-image" />
                                <button onClick={() => handleCateClick('?brands=converse')} className="bottom-4 z-10 absolute text-base font-medium leading-none text-gray-50 py-3 w-36 bg-black">Converse</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
