import * as React from 'react';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import axiosClient from '../../api/axios-client';

export interface SignInProps {
}

export function SignIn(props: SignInProps) {
    let history = useHistory();
    const [isValid, setValid] = React.useState({
        email: false,
        phoneNumber: false,
        repassword: false
    })
    const [isSignIn, setSignIn] = React.useState(true);
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [fullName, setFullName] = React.useState("");
    const [phoneNumber, setPhoneNumber] = React.useState("");
    const [rePassword, setRePassword] = React.useState("");

    let fetchData = async(path:string, formData:object) => {
        try{
            let result = await axiosClient.post(path,formData);
            return result;
        }catch(error){

        }
    }
    const handleButtonClick = async () => {
        let formData;
        if (isSignIn && isValid.email) {
            formData = {
                email:email,
                password:password
            }
            let result:any = await fetchData("/auth/users/sign-in",formData);
            if(result.data.token){
                localStorage.setItem("access_token",result.data.token);
                localStorage.setItem("user_info",email);
                toast.success("Sign in success");
                history.push("/");
                return;

            }
            if(!result.data.content){
                toast.warning(result.data);
                return;
            }
            toast.warning(result.data.content);
            return;
        }
        formData = {
            email:email,
            password:password,
            phoneNumber:phoneNumber,
            fullName:fullName
        }
        let result:any = await fetchData("/auth/users/sign-up",formData);
        
        if(result.content){
            toast.warning(result.content);
            return;
        }
        toast.success("Sign up success");
        setSignIn(true);
        return;

    }

    const handleEmailChange = (e: any) => {
        if (e.target.value.match(/^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/gm)) {
            setValid((prevValid: any) => ({
                ...prevValid,
                email: true
            }));
        }
        else {
            setValid((prevFilters: any) => ({
                ...prevFilters,
                email: false
            }));
        }
        setEmail(e.target.value);
    }
    const handlePasswordChange = (e: any) => {
        setPassword(e.target.value);
    }
    const handleRePasswordChange = (e: any) => {
        if (e.target.value === password) {
            setValid((prevValid: any) => ({
                ...prevValid,
                repassword: true
            }))
        }
        else {
            setValid((prevValid: any) => ({
                ...prevValid,
                repassword: false
            }))
        }
        setRePassword(e.target.value);
    }
    const handleFullNameChange = (e: any) => {
        setFullName(e.target.value);
    }
    const handlePhoneNumberChange = (e: any) => {
        if (e.target.value.match(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/)) {
            setValid((prevValid: any) => ({
                ...prevValid,
                phoneNumber: true
            }))
        }
        else {
            setValid((prevValid: any) => ({
                ...prevValid,
                phoneNumber: false
            }))
        }
        setPhoneNumber(e.target.value);
    }
    return (
        <div className="flex items-center min-h-screen bg-white dark:bg-gray-900">
            <div className="container mx-auto">
                <div className="max-w-md mx-auto my-10">
                    <div className="text-center">
                        <h1 className="my-3 text-3xl font-semibold text-gray-700 ">{isSignIn ? "Sign in" : "Sign up"}</h1>
                    </div>
                    <div className="m-7">
                        <div className="mb-6">
                            <label className="block mb-2 text-sm text-gray-600">Email Address</label>
                            <input defaultValue={email} onBlur={handleEmailChange} type="email" name="email" id="email" placeholder="Your Email" className={`w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md  ${isValid.email ? "border-green-500" : "border-red-500"}`} />
                            {isValid.email ? null :
                                <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                    Invalid Email
                                </span>
                            }
                        </div>
                        <div className="mb-6">
                            <div className="flex justify-between mb-2">
                                <label className="text-sm text-gray-600 dark:text-gray-400">Password</label>

                            </div>
                            <input defaultValue={password} onBlur={handlePasswordChange} type="password" name="password" id="password" placeholder="Your Password" className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        </div>
                        {isSignIn ? null :
                            <>
                                <div className="mb-6">
                                    <div className="flex justify-between mb-2">
                                        <label className="text-sm text-gray-600 dark:text-gray-400">Repassword</label>
                                    </div>
                                    <input defaultValue={rePassword} onBlur={handleRePasswordChange} type="password" name="repassword" id="repassword" placeholder="Your Re-Password" className={`w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md  ${isValid.repassword ? "border-green-500" : "border-red-500"}`}/>
                                    {isValid.repassword ? null :
                                        <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                            Invalid Re-password
                                        </span>
                                    }
                                </div>
                                <div className="mb-6">
                                    <div className="flex justify-between mb-2">
                                        <label className="text-sm text-gray-600 dark:text-gray-400">Full name</label>
                                    </div>
                                    <input defaultValue={fullName} onBlur={handleFullNameChange} type="text" name="fullname" id="fullname" placeholder="Your Full Name" className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md" />
                                </div>
                                <div className="mb-6">
                                    <div className="flex justify-between mb-2">
                                        <label className="text-sm text-gray-600 dark:text-gray-400">Phone number</label>
                                    </div>
                                    <input type="text" defaultValue={phoneNumber} onBlur={handlePhoneNumberChange} name="phonenumber" id="phonenumber" placeholder="Your Phone Number" className={`w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md  ${isValid.phoneNumber ? "border-green-500" : "border-red-500"}`} />
                                    {isValid.phoneNumber ? null :
                                        <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                            Invalid Phone Number
                                        </span>
                                    }
                                </div>
                            </>
                        }

                        <div className="mb-6">
                            <button type="button" onClick={handleButtonClick} className="w-full px-3 py-4 text-white bg-purple-500 rounded-md focus:bg-purple-600 focus:outline-none">{isSignIn ? "Sign in" : "Sign up"}</button>
                        </div>
                        {
                            isSignIn ?
                                <p className="text-sm text-center text-gray-400">Don&#x27;t have an account yet? <button onClick={() => setSignIn(false)} className="text-purple-400 focus:outline-none focus:underline focus:text-purple-500 dark:focus:border-indigo-800">Sign up</button>.</p>
                                :
                                <p className="text-sm text-center text-gray-400">Have an account ? <button onClick={() => setSignIn(true)} className="text-purple-400 focus:outline-none focus:underline focus:text-purple-500 dark:focus:border-indigo-800">Sign in</button>.</p>
                        }

                    </div>
                </div>
            </div>
        </div>
    );
}
