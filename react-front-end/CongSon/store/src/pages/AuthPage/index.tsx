import * as React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';
import { SignIn } from './SignIn';



export function SignInPage() {
    const match = useRouteMatch();
    return (
        <div>
            <Header />
            <Switch>
                <Route path={match.url} component={SignIn} exact />
            </Switch>
            <Footer />
        </div>

    )
}
