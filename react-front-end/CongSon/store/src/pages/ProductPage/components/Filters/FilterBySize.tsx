import * as React from 'react';

interface FilterBySizeProps {
    sizeList: Array<any>,
    filters: any,
    onChange: Function,
}

export function FilterBySize({ sizeList, filters, onChange }: FilterBySizeProps) {
    const [isVisible, setVisible] = React.useState(false);
    const [checked, setChecked] = React.useState(() => {
       return new Array((sizeList.length)).fill(false)
    }
    )

    React.useEffect(() => {
        if(!filters.sizes){
            return;
        }
        
        let newChecked = [...checked];
        let sizeParams = filters.sizes;
        sizeList.forEach((item,index) => {
            if(sizeParams.indexOf(Number.parseInt(item.name)) !== -1 ){
                newChecked[index] = true;
            } 
        })
        setChecked(newChecked);

    },[])

    const handleVisibleChange = () => {
        setVisible(!isVisible);
    }

    const handleChecked = (position: any) => {
        
        let newChecked = [...checked];
        console.log(checked);
        const updatedCheckedState = newChecked.map((item, index) =>
            index === position ? !item : item
        );

        let checkedSize = sizeList.filter((item, index) => updatedCheckedState[index] === true);
        
        let sizeString = checkedSize.map((item) => item.name).join(",");
       
        if (sizeString) {
            let newFilter = {
                'sizes': sizeString
            }
            onChange(newFilter);
            setChecked(updatedCheckedState);
            return;
        }
        delete filters.sizes;
        onChange(filters);
        setChecked(updatedCheckedState);


    }

    return (
        <div className="border-b border-gray-200 py-6">
            <h3 className="-my-3 flow-root">

                <button type="button" className="py-3 bg-white w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500" aria-controls="filter-section-2" aria-expanded="false">
                    <span className="font-medium text-gray-900">
                        Size
                    </span>
                    <span className="ml-6 flex items-center" onClick={handleVisibleChange}>
                        {
                            isVisible ?
                                <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                </svg>
                                :
                                <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clipRule="evenodd" />
                                </svg>
                        }

                    </span>
                </button>
            </h3>

            {
                isVisible ? null :
                    <div className="pt-6" id="filter-section-2">

                        <ul className="flex flex-wrap">
                            {sizeList.map((item, index) => {
                                return (
                                    <li key={(index)}>
                                        <label className="inline-flex items-center mt-2">
                                            <input value={item.name} checked={checked[index]} onChange={() => handleChecked(index)} type="checkbox" className="form-checkbox cursor-pointer hidden" /><span className={`text-xs border border-gray-200 rounded-sm h-10 w-10 mx-1 flex items-center justify-center cursor-pointer shadow-sm ease-out transform hover:scale-110 transition duration-300 ${checked[index] ? `bg-black text-gray-50 ` : `hover:bg-black hover:text-gray-50`}`}>{item.name}</span>
                                        </label>
                                    </li>
                                )
                            })}

                        </ul>
                    </div>
            }
        </div>

    )
}