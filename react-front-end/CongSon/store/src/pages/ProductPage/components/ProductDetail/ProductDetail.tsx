import * as React from "react";
import { Link } from "react-router-dom";
import { ImageSlide } from "../ImageSlider/ImageSlide";
import { ProductPolicy } from "./Policy";
import { Rating } from "./Rating";
import { Size } from "./Size";
import { useParams } from "react-router-dom";
import { ProductReview } from "./ProductReview";
import { RelatedProducts } from "./RelatedProducts";
import { Accordion } from "./components/Accordion";
import { useDispatch } from "react-redux";
import { addToCart } from "../../../CartPage/cartSlice";
import { toast } from "react-toastify";
import { Color } from "./Color";
import axiosClient from "../../../../api/axios-client";
import { addToWishList } from "../../../WishlistPage/wishlistSlice";
import Parser from 'html-react-parser';


interface IProductVariant {
  size: number;
  colors: Array<any>;
}

interface IProduct {
  id: number;
  name: string;
  slug: string;
  price: number;
  productVariants: IProductVariant[];
  brand: string;
  image: Array<any>;
  reviews: any;
  description: any;
}

export interface ItemDetailProps { }


const data = [
  {
    image:
      "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/037c035b-14e3-4cb2-a9c5-65f377f7d1ed/air-zoom-pegasus-38-running-shoes-Hmsj6Q.png",
  },
  {
    image:
      "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/1510a451-1b8b-441f-b424-f02d54cd6001/air-zoom-pegasus-38-flyease-womens-running-shoe-0gpVcx.png",
  },
  {
    image:
      "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/1433cc5c-692b-42dc-8bfe-f61775f74112/air-zoom-pegasus-38-road-running-shoes-mPchsD.png",
  },
  {
    image:
      "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/1510a451-1b8b-441f-b424-f02d54cd6001/air-zoom-pegasus-38-flyease-womens-running-shoe-0gpVcx.png",
  },
  {
    image:
      "https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/1510a451-1b8b-441f-b424-f02d54cd6001/air-zoom-pegasus-38-flyease-womens-running-shoe-0gpVcx.png",
  },
];

export function ProductDetail(props: ItemDetailProps) {
  const dispatch = useDispatch();

  let { slug }: any = useParams();
  const [product, setProduct] = React.useState<IProduct>();
  const [imageList, setImageList] = React.useState<any>();
  const [showDiscription, setShowDiscription] = React.useState(false);
  const [showPolicy, setShowPolicy] = React.useState(false);
  const [showReviews, setShowReviews] = React.useState(false);
  const [selectedSize, setSelectedSize] = React.useState();
  const [selectedColor, setSelectedColor] = React.useState();
  const [colorList, setColorList] = React.useState<string[]>([]);
  const [outOfStock, setOutOfStock] = React.useState(false);
  const [sizeList, setSizeList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const [loved, setLoved] = React.useState(false);

  const fetchData = async () => {
    try {
      let result: any = await axiosClient.get(`/products/${slug}`);
      if (!result) {
        return;
      }
      let newWishList = JSON.parse(localStorage.getItem("wishlist") || "[]");
      if (newWishList.length > 0) {
        const index = newWishList.findIndex(
          (item: any) => item.id === result?.id
        );
        if (index >= 0) setLoved(true);
      }
      let newSizeList = result.productVariants.map((item: any) => item.size);
      if (newSizeList.length === 0) {
        setOutOfStock(true);
      }
      let newImageList: any = [];
      setSizeList(newSizeList);
      setProduct(result);
      if (result.images.length > 0) {
        result.images.forEach((image: any) => {
          newImageList.push({
            image: `${process.env.REACT_APP_API_ENDPOINT}/products${image}`,
          });
        });
        setImageList(newImageList);
      } else {
        setImageList(data);
      }

      setLoading(false);
    } catch (error) { }
  };
  React.useEffect(() => {
    fetchData();
  }, []);
  React.useEffect(() => {
    let colorsOfSize: any = product?.productVariants.find((item: any) => {
      if (item.size === selectedSize) {
        return item;
      }
    });
    if (!colorsOfSize) {
      return;
    }
    let newColorList: string[] = Object.keys(colorsOfSize.colors);
    setColorList(newColorList);
  }, [selectedSize]);

  const handleDiscriptionToggle = (visible: boolean) => {
    setShowDiscription(visible);
  };
  const handleReviewsToggle = (visible: boolean) => {
    setShowReviews(visible);
  };
  const handlePolicyToggle = (visible: boolean) => {
    setShowPolicy(visible);
  };

  const handleAddReview = async (value: any) => {
    try {
      let result = await axiosClient.post(
        `/products/${product?.id}/reviews`,
        value
      );
      if (product?.reviews) {
        setProduct((prevState: any) => ({
          ...prevState,
          reviews: [...product.reviews, result.data],
        }));
        toast.success("Thanks for your review");
      }
    } catch (error) {
      toast.error("Something went wrong");
    }
  };

  const handleAddToWishlist = () => {
    const action = addToWishList({
      id: product?.id,
      name: product?.name,
      price: product?.price,
      image: imageList[0].image
    });
    dispatch(action);
    if (loved) {
      toast.success('Unliked');
    }
    else {
      toast.success('Liked');
    }
    setLoved(!loved);
  };

  const handleAddToCart = () => {
    if (!selectedColor) {
      toast.warn("Pick a color");
      return;
    }
    let newProductName = `${product?.slug} ${selectedColor} ${selectedSize}`;
    const actions = addToCart({
      id: product?.id,
      name: product?.name,
      price: product?.price,
      quantity: 1,
      productName: newProductName,
      image: imageList[0].image
    });
    dispatch(actions);
    toast.success("Added");
  };

  const handleSizeChange = (newSize: any) => {
    if (selectedSize === newSize) {
      return;
    }
    setSelectedSize(newSize);
  };

  const handleColorChange = (newColor: any) => {
    if (selectedColor === newColor) {
      return;
    }
    setSelectedColor(newColor);
  };

  return (
    <>
      {isLoading ? (
        <div className="flex h-screen md:justify-center items-center">
          <div
            className="
          animate-spin
          rounded-full
          h-32
          w-32
          border-t-2 border-b-2 border-purple-500
      "
          ></div>
        </div>
      ) : (
        <div className="max-w-screen-xl mx-auto">
          <section className="text-gray-700 body-font overflow-hidden bg-white">
            <div className="container flex flex-col md:flex-row px-5 py-24 mx-auto">
              <div className="w-full mx-auto mr-5">
                <div
                  className="text-sm text-left text-gray-600 bg-opacity-10 h-12 flex items-center p-4 rounded-md"
                  role="alert"
                >
                  <ol className="list-reset flex text-grey-dark">
                    <li>
                      <Link to="/" className="font-bold">
                        Home
                      </Link>
                    </li>
                    <li>
                      <span className="mx-2">/</span>
                    </li>
                    <li>{product?.name}</li>
                  </ol>
                </div>
                <ImageSlide images={imageList} />
                {/* <img alt="ecommerce" className="w-full object-cover object-center rounded border border-gray-200" src="https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/037c035b-14e3-4cb2-a9c5-65f377f7d1ed/air-zoom-pegasus-38-running-shoes-Hmsj6Q.png" /> */}
              </div>
              <div className="w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
                <h2 className="text-sm title-font text-gray-500 tracking-widest">
                  {product?.brand}
                </h2>
                <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
                  {product?.name}
                </h1>
                <span className="title-font font-medium text-2xl mb-2 block text-red-600">
                  {product?.price
                    .toLocaleString("it-IT", {
                      style: "currency",
                      currency: "VND",
                    })
                    .replace("VND", "đ")}
                </span>
                <div className="flex mt-5 justify-between">
                  <Rating
                    rating={product?.reviews.rating}
                    reviews={product?.reviews.length}
                  />
                  <button onClick={handleAddToWishlist}>
                    {loved ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 text-red-500"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                          clipRule="evenodd"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                        />
                      </svg>
                    )}
                  </button>
                </div>

                {outOfStock ? <h2 className="text-red-500 text-2xl">Out of stock</h2> :
                  <>
                    <Size
                      handleChange={handleSizeChange}
                      sizeList={sizeList}
                      selectedSize={selectedSize}
                    />
                    {colorList.length > 0 && (
                      <Color
                        handleChange={handleColorChange}
                        colorList={colorList}
                        selectedColor={selectedColor}
                      />
                    )}
                    <button
                      className="
              text-base
              flex
              items-center
              justify-center
              leading-none
              text-white
              bg-black
              w-full
              py-4
              mt-6
              hover:bg-opacity-80
              "
                      onClick={handleAddToCart}
                    >
                      Add to Cart
                    </button>
                  </>
                }

                <Accordion
                  isShow={showDiscription}
                  handleToggleChange={handleDiscriptionToggle}
                  title="Description"
                  content={Parser(product?.description)}
                />
                <Accordion
                  isShow={showPolicy}
                  handleToggleChange={handlePolicyToggle}
                  title="Free Delivery and Returns"
                  content={<ProductPolicy />}
                />
                <Accordion
                  isShow={showReviews}
                  handleToggleChange={handleReviewsToggle}
                  title={`Reviews (${product?.reviews.length})`}
                  content={
                    <ProductReview
                      handleAddReview={handleAddReview}
                      reviews={product?.reviews}
                    />
                  }
                />
              </div>
            </div>
          </section>
          <RelatedProducts />
        </div>
      )}
    </>
  );
}
