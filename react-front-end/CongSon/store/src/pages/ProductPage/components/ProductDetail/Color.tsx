import * as React from 'react';
import { Link } from 'react-router-dom';

export interface ColorProps {
    colorList: any,
    selectedColor?: string,
    handleChange: Function
}

export function Color(props: ColorProps) {
    const { handleChange, colorList, selectedColor } = props;

    const handleColorChange = (e: any) => {
        handleChange(e.target.id);
    }
    return (
        <div className="mt-10 items-center">
            <h3 className="font-medium">Select Color</h3>
            <div className="flex flex-wrap my-3" >
                {colorList.map((color:any, index:any) => {
                    return (
                        <button className="flex flex-col h-full mx-3 items-center min-h-68 ease-out transform hover:scale-110 transition duration-500" value={color} key={index} onClick={handleColorChange}>
                            <div className={`h-7 w-7 rounded-full bg-${color.toLowerCase()}-600 flex justify-center items-center`} id={color}>
                                {
                                    selectedColor === color ?
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                        </svg>
                                        : null
                                }
                            </div>
                            <span className="capitalize inline-block whitespace-normal break-words">{color}</span>
                        </button>
                    )
                })}
            </div>
        </div>
    );
}
