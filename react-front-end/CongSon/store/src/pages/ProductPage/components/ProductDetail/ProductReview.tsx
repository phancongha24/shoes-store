import * as React from 'react';
import { toast } from 'react-toastify';
import { Comment } from './Comment';
export interface ProductReviewProps {
    reviews: Array<any> | undefined,
    handleAddReview: Function
}

export function ProductReview({ reviews, handleAddReview }: ProductReviewProps) {
    const [review, setReview] = React.useState({
        name: "",
        title: "",
        content: "",
        rating: 5

    })
    const handleSubmit = () => {
        if(!review.name || !review.title || !review.content){
            toast.warn("Please fill it out completely");
            return;
        }
        
        handleAddReview(review);
        setReview({
            name:'',
            title:'',
            content:'',
            rating:5
        })
    }
    const handleReviewContentChange = (e: any) => {
        setReview((prevState) => ({
            ...prevState,
            content: e.target.value
        }))
    }
    const handleReviewTitleChange = (e: any) => {
        setReview((prevState) => ({
            ...prevState,
            title: e.target.value
        }))
    }
    const handleReviewNameChange = (e: any) => {
        setReview((prevState) => ({
            ...prevState,
            name: e.target.value
        }))
    }

    return (
        <div>
            <div className="flex items-center rounded-xl max-w-lg">
                <div className="w-full max-w-xl bg-white rounded-lg pt-2">
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <h2 className="px-4 pt-3 pb-2 text-my-color text-lg font-semibold">Write review</h2>
                        <div className="w-full md:w-full px-3 mb-2 mt-2">
                            <div className='my-2'>
                                <label className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                >Your name</label>
                                <input value={review.name} onChange={handleReviewNameChange} type="text" placeholder="Your name" name="name" id="name" className=" border border-gray-300 px-3 py-3 rounded text-sm w-full"
                                />
                            </div>
                            <div className='my-2'>
                                <label className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                >Review title</label>
                                <input value={review.title} onChange={handleReviewTitleChange} type="text" placeholder="Headline or summary for you review" name="title" id="title" className=" border border-gray-300 px-3 py-3 rounded text-sm w-full"
                                />
                            </div>
                            <textarea value={review.content} onChange={handleReviewContentChange} className="h-28 border border-gray-300 px-3 py-3 rounded text-sm w-full" name="body" placeholder='Write your review here. Consider whether you would recommend this product and what you like or dislike about it' required></textarea>
                        </div>
                        <button className="bg-black text-white font-bold py-2 px-3 mx-3 rounded uppercase w-full" onClick={handleSubmit}>
                            Submit
                        </button>
                    </div>
                </div>
            </div>
            <div className="max-h-500 overflow-y-scroll">
                {reviews?.map((review) => <Comment title={review.title} content={review.content} createdAt={review.createdAt} name={review.name}/>)}
            </div>
        </div>
    );
}
