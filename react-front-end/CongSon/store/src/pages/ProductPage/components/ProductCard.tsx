import * as React from 'react';
import { Link } from 'react-router-dom';


export interface ProductCardProps {
  product?: any,
}

export function ProductCard(props: ProductCardProps) {

  const { product } = props;
  return (
    <div className="relative p-4 mt-5 cursor-pointer">
      <div className="relative group" key={product.id}>
        <div className="transition-all delay-75 flex justify-center items-center opacity-0 bg-gradient-to-t from-gray-400 via-gray-300 to-opacity-30 "></div>
        <Link to={`${product.slug}`}><img className="w-full h-96 object-cover md:object-fill" src={`${product.images[0] ? `${process.env.REACT_APP_API_ENDPOINT}/products${product.images[0]}` : "https://cdn.shopify.com/s/files/1/0145/5613/5478/products/product-1.jpg?v=1569556553"}`} /></Link>
      </div>
      <Link to={`${product.slug}`}><p className=" font-normal text-xl leading-5 text-gray-800 md:mt-6 mt-4 cursor-pointer hover:text-purple-600">{product.name}</p> </Link>
      <p className=" font-semibold text-xl leading-5 text-gray-800 mt-4">{product.price.toLocaleString('it-IT', { style: 'currency', currency: 'VND' })}</p>
    </div>
  );
}
