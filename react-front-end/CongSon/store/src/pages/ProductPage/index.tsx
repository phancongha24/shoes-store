import * as React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';
import { ProductDetail } from './components/ProductDetail/ProductDetail';
import { ListPage } from './ListPage';


export function ProductPage() {
  const match = useRouteMatch();
  return (
    <div>
      <Header />
      <Switch>
        <Route path={match.url} exact>
          <ListPage />
        </Route>
      </Switch>
      <Switch>
        <Route exact path={`${match.url}/:slug`} render={props => <ProductDetail key={Date.now()} />} />
      </Switch>

      <Footer />
    </div>

  )
}
