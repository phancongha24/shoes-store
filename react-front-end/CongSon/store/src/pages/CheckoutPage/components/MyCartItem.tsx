import * as React from 'react';

export interface MyCartItemProps {
    product:any;
}

export function MyCartItem(props: MyCartItemProps) {
    const { product } = props;
    return (
        <li className="grid grid-cols-6 gap-2 border-b-1">
            <div className="col-span-1 self-center">
                <img src={product.image} alt="Product" className="rounded w-full" />
            </div>
            <div className="flex flex-col col-span-3 pt-2">
                <span className="text-gray-600 text-md font-semi-bold">{product.name}</span>
                <span className="text-my-color hidden md:inline-block">{product.quantity}x{product.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ")}</span>
            </div>
            <div className="col-span-2 pt-3">
                <div className="flex items-center justify-end space-x-2">
                    
                    <span className="text-red-500 font-semibold inline-block">{(product.quantity*product.price).toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ")}</span>
                </div>
            </div>
        </li>
    );
}
