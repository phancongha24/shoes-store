import * as React from 'react';
import { MyCartItem } from './components/MyCartItem';

export interface MyCartProps {
    cartList: Array<object>,
    cartTotal: number,
    handlePaymentClick: Function,
    discountPrice: number
}

export function MyCart({ cartList, cartTotal = 0, handlePaymentClick, discountPrice }: MyCartProps) {

    const handlePayment = () => {
        handlePaymentClick()
    }

    return (
        <div className="bg-white lg:block">
            <h1 className="py-6 border-b-2 text-xl text-gray-600 px-8">Order Summary</h1>
            <ul className="py-6 border-b space-y-6">
                {cartList.map(product => <MyCartItem product={product}/>)}
            </ul>
            <div className="border-b">
                <div className="flex justify-between py-4 text-gray-600">
                    <span>Subtotal</span>
                    <span className="font-semibold text-red-500">{(cartTotal+discountPrice).toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ")}</span>
                </div>
                <div className="flex justify-between py-4 text-gray-600">
                    <span>Shipping</span>
                    <span className="font-semibold text-red-600">FREE</span>
                </div>
                <div className="flex justify-between py-4 text-gray-600">
                    <span>Discount</span>
                    <span className="font-semibold text-red-500">{discountPrice.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ")}</span>
                </div>
            </div>
            <div className="font-semibold text-xl flex justify-between py-8 text-my-color">
                <span>Total</span>
                <span className="font-medium text-red-500">{cartTotal > 0 ? cartTotal.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}).replace("VND","đ") : '0đ'}</span>
            </div>
            <button onClick={handlePayment} className="submit-button px-4 py-5  bg-purple-500 text-white w-full text-xl font-semibold transition-colors">
                Make Payment
            </button>
        </div>
    );
}
