import * as React from 'react';
import axiosClient from '../../api/axios-client';
import { MyCart } from './MyCart';
import { AreasSelect } from './components/AreasSelect';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { clearCart } from '../CartPage/cartSlice';
import { useDispatch } from 'react-redux';
interface CheckoutProps {
    cartList: Array<object>,
    cartTotal: number,
}

const paymentMethodList = [
    {
        value: 'online'
    },
    {
        value: 'cod'
    },
]

export function Checkout({ cartList, cartTotal }: CheckoutProps) {

    const dispatch = useDispatch();
    const history = useHistory();
    const [total, setTotal] = React.useState(cartTotal);
    const [discountPrice, setDiscountPrice] = React.useState(0);
    const [isValid, setValid] = React.useState({
        validCustomer: false,
        validPhoneNumber: false,
        validNote: false,
        validAddress: false,
        validPayment: false,
        validDiscount: false,
    })
    const [provinceList, setProvinceList] = React.useState([]);
    const [selectedProvince, setSelectedProvince] = React.useState("79");
    const [districtList, setDistrictList] = React.useState([]);
    const [selectedDistrict, setSelectedDistrict] = React.useState("774");
    const [paymentMethod, setPaymentMethod] = React.useState(paymentMethodList);
    const [customer, setCustomer] = React.useState("");
    const [phoneNumber, setPhoneNumber] = React.useState("");
    const [note, setNote] = React.useState("");
    const [address, setAddress] = React.useState("");
    const [province, setProvince] = React.useState("");
    const [district, setDistrict] = React.useState("");
    const [discountCode, setDiscountCode] = React.useState(() => {
        const initDiscount = localStorage.getItem('voucher') || '';
        return initDiscount;
    });

    React.useEffect(() => {
        initDistrictList();
    }, [selectedProvince]);

    React.useEffect(() => {
        initProvinceList();
    }, [])

    const handlePaymentClick = async () => {
        if (isValid.validCustomer && isValid.validPhoneNumber
            && isValid.validPayment && isValid.validAddress && isValid.validNote) {
            let orderItems = cartList.map((item: any) => {
                return {
                    productName: item.productName,
                    quantity: item.quantity
                }
            })
            let newAddress = `${address}, ${district}, ${province}`;

            let newData: any = {
                address: newAddress,
                note: note,
                paymentMethod: paymentMethod,
                customer: customer,
                phoneNumber: phoneNumber,
                orderItems: orderItems,
            }
            if (discountCode.length > 0) {
                newData.code = discountCode;
            }
            let result: any = await axiosClient.post('/orders', newData);
            if (result.status === 400) {
                toast.error(result.data.message);
                return;
            }
            if (result.status === 201) {
                if (result.data.fullfilled === false) {
                    localStorage.removeItem('voucher');
                    dispatch(clearCart([]));
                    history.push('/success');
                    return;
                }
                window.open(result.data);
                localStorage.removeItem('voucher');
                dispatch(clearCart([]));
                history.push('/success');
                return;
            }

        }
        toast.warn("Please fill full");
        return;

    }

    const handleCustomerChange = (e: any) => {
        setCustomer(e.target.value);
        setValid((prevValid) => ({
            ...prevValid,
            validCustomer: true
        }))
    }
    const handlePhoneNumberChange = (e: any) => {
        if (e.target.value.match(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/)) {
            setValid((prevValid: any) => ({
                ...prevValid,
                validPhoneNumber: true
            }))
        }
        else {
            setValid((prevValid: any) => ({
                ...prevValid,
                validPhoneNumber: false
            }))
        }
        setPhoneNumber(e.target.value);
    }
    const handleDiscountChange = (e: any) => {
        setDiscountCode(e.target.value);
    }
    const handleNoteChange = (e: any) => {
        setNote(e.target.value);
        setValid((prevValid) => ({
            ...prevValid,
            validNote: true
        }))
    }
    const handleAddressChange = (e: any) => {
        setAddress(e.target.value.replace(/\,/g, ''));
        setValid((prevValid) => ({
            ...prevValid,
            validAddress: true
        }))
    }

    const fetchData = async (path: string) => {
        let response: any = await axiosClient.getAreas(path);
        return response.results;
    }

    const handlePaymentChange = (e: any) => {
        setPaymentMethod(e.target.value);
        setValid((prevValid) => ({
            ...prevValid,
            validPayment: true
        }))
    }

    const handleDiscountClick = async () => {
        try {
            let result: any = await axiosClient.get(`/vouchers/validate?code=${discountCode}`);
            if (!result.code) {
                toast.error(result);
                setTotal(cartTotal);
                setValid((prevValid) => ({
                    ...prevValid,
                    validDiscount: false
                }))
                return;
            }
            if (isValid.validDiscount) {
                toast.warning('discount code has been added');
                return;
            }
            setValid((prevValid) => ({
                ...prevValid,
                validDiscount: true
            }))
            let newPrice = total - result.maxDiscountAmount;
            let newDiscountPrice = result.maxDiscountAmount;
            setDiscountPrice(newDiscountPrice);
            setTotal(newPrice);
        } catch (error) {
            console.log(error);
        }
    }
    //"273 An Dương Vương, Quận 5, thành phố Hồ Chí Minh",
    const handleProvinceChange = (code: string, name: string) => {
        setSelectedProvince(code);
        setProvince(name);
    }
    const handleDistrictChange = (code: string, name: string) => {
        setSelectedDistrict(code);
        setDistrict(name);
    }
    const initProvinceList = async () => {
        try {
            let results = await fetchData('/province');
            setProvinceList(results);
            let provinceName = '';
            results.forEach((province: any) => {
                if (province.code === selectedProvince) {
                    provinceName = province.name;
                }
            })
            setProvince(provinceName);
        }
        catch (error) {
            console.log(error)
        }
    }
    const initDistrictList = async () => {
        try {
            let results = await fetchData(`/district?province=${selectedProvince}`);
            setDistrictList(results);
            setSelectedDistrict(results[0].code);
            setDistrict(results[0].name);
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="grid grid-cols-1 mx-5 md:grid-cols-2 gap-12 max-w-screen-xl xl:mx-auto">
            <div className="space-y-8">
                <div className="mt-8 p-4 relative flex flex-col sm:flex-row sm:items-center bg-white shadow rounded-md">
                    <div className="flex flex-row items-center border-b sm:border-b-0 w-full sm:w-auto pb-4 sm:pb-0">
                        <div className="text-yellow-500">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-6 sm:w-5 h-6 sm:h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                        <div className="text-sm font-medium ml-3">Payment</div>
                    </div>
                    <div className="text-sm tracking-wide text-gray-500 mt-4 sm:mt-0 sm:ml-4">Complete your shipping and payment details below.</div>
                </div>

                <div className="w-full">
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Full name
                            </label>
                            <input defaultValue={customer} onBlur={handleCustomerChange} className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Your fullname" />
                            {isValid.validCustomer ? null : <p className="text-red-500 text-xs italic">please complete here.</p>}
                        </div>
                        <div className="w-full md:w-1/2 px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Phone number
                            </label>
                            <input defaultValue={phoneNumber} onBlur={handlePhoneNumberChange} className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Your phone number" />
                            {isValid.validPhoneNumber ? null : <p className="text-red-500 text-xs italic">please complete here.</p>}
                        </div>
                    </div>

                    <div className="flex flex-wrap -mx-3 mb-6">
                        <AreasSelect title="City" data={provinceList} selected={selectedProvince} onChange={handleProvinceChange} />
                        <AreasSelect title="District" data={districtList} selected={selectedDistrict} onChange={handleDistrictChange} />
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Address
                            </label>
                            <input defaultValue={address} onBlur={handleAddressChange} className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="273 An Dương Vương" />
                            {isValid.validAddress ? null : <p className="text-red-500 text-xs italic">please complete here.</p>}
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Note
                            </label>
                            <input defaultValue={note} onBlur={handleNoteChange} className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Note" />
                            {isValid.validNote ? null : <p className="text-red-500 text-xs italic">please complete here.</p>}
                        </div>
                        <div className="lg:w-2/4">
                            <div className="px-3 py-3 rounded-md">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                    Discount
                                </label>
                                <input type="text" onChange={handleDiscountChange} placeholder="coupon code" value={discountCode} className="
                                        w-full
                                        px-2
                                        py-2
                                        border border-blue-600
                                        rounded-md
                                        outline-none
                                    "/>
                                <span className={`block ${isValid.validDiscount ? `text-green-600` : `text-red-500`}`}>{isValid.validDiscount ? 'Success' : 'Failure'}</span>
                                <button
                                    onClick={handleDiscountClick}
                                    className="
                                        px-6
                                        py-2
                                        mt-2
                                        text-sm text-gray-50
                                        bg-purple-500
                                        rounded-md
                                        hover:bg-purple-600
                                    ">
                                    Apply
                                </button>
                            </div>
                        </div>
                        <div className="px-3 py-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Payment Method
                            </label>
                            {isValid.validPayment ? null : <p className="text-red-500 text-xs italic">please complete here.</p>}
                            {paymentMethodList.map((item, index) =>
                                <label className="inline-flex items-center mr-3" key={index}>
                                    <input type="radio" className="form-radio" name="payment" value={item.value} onChange={handlePaymentChange} />
                                    <span className="ml-2">{item.value}</span>
                                </label>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            <MyCart cartList={cartList} cartTotal={total} discountPrice={discountPrice} handlePaymentClick={handlePaymentClick} />
        </div>
    );
}
