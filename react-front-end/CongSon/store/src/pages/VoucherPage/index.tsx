import * as React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router';
import { Footer } from '../../components/Common/Footer';
import { Header } from '../../components/Common/Header';
import { Voucher } from './Voucher';

export interface TrackOrderPageProps {
}

export function VoucherPage(props: TrackOrderPageProps) {
    const match = useRouteMatch();
    return (
        <>
            <Header />
            <Switch>
                <Route path={match.url} exact component={Voucher}/>
            </Switch>
            <Footer />
        </>
    );
}
