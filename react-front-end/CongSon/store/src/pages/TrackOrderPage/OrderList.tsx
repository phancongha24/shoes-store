import * as React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { toast } from 'react-toastify';
import axiosClient from '../../api/axios-client';
import { OrderTable } from './components/OrderTable';

export interface OrderListProps {
}

export function OrderList(props: OrderListProps) {

    const [isLoading, setLoading] = React.useState(true);
    const [pendingOrders, setPendingOrders] = React.useState([]);
    const [packedOrders, setPackedOrders] = React.useState([]);
    const [shippingOrders, setShippingOrders] = React.useState([]);
    const [completeOrders, setCompleteOrders] = React.useState([]);
    const [cancelOrders, setCancelOrders] = React.useState([]);
    const fetchData = async () => {
        try {
            let result: any = await axiosClient.get(`/orders/tracking?tracked_by=${localStorage.getItem("user_info")}`);
            if (result.length !== 0) {
                let newPendingOrders = result.filter((order: any) => order.status === "Pending");
                let newPackedOrders = result.filter((order: any) => order.status === "Awaiting Shipment");
                let newShippingOrders = result.filter((order: any) => order.status === "Shipping");
                let newCompleteOrders = result.filter((order: any) => order.status === "Completed");
                let newCancelOrders = result.filter((order: any) => order.status === "Cancelled");
                setPendingOrders(newPendingOrders);
                setPackedOrders(newPackedOrders);
                setShippingOrders(newShippingOrders);
                setCompleteOrders(newCompleteOrders);
                setCancelOrders(newCancelOrders);
                setLoading(false);
            }
            setLoading(false);
        }
        catch (error) {
            console.log(error);
        }
    }

    const handleOrderListChange = (orderList: any, orderStatus: string, orderItem: any) => {

        switch (orderStatus) {
            case "Pending":
                setPendingOrders(orderList);
                break;
            case "Awaiting Shipment":
                setPackedOrders(orderList);
                break;
        }
        let newCancelledOrders:any = [...cancelOrders];
        orderItem.status="Cancelled";
        newCancelledOrders.push(orderItem);
        setCancelOrders(newCancelledOrders);
        toast.success(`Order ${orderItem.id} has been canceled`);
    }
    React.useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
            {
                isLoading ?
                    <div className="flex h-screen md:justify-center items-center">
                        <div
                            className="
                            animate-spin
                            rounded-full
                            h-32
                            w-32
                            border-t-2 border-b-2 border-purple-500
                        "
                        ></div>
                    </div>
                    :
                    < div className="sm:px-6 w-full" >
                        <Tabs>
                            <div className="bg-white py-4 md:py-7 md:px-8 xl:px-10">
                                <div className="sm:flex md:justify-center">
                                    <TabList className="flex md:justify-center">
                                        <Tab>
                                            <div className="flex flex-col items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4" />
                                                </svg>
                                                Pending
                                            </div>
                                        </Tab>
                                        <Tab>
                                            <div className="flex flex-col items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4" />
                                                </svg>
                                                Packed
                                            </div>
                                        </Tab>

                                        <Tab>
                                            <div className="flex flex-col items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0" />
                                                </svg>
                                                Shipping
                                            </div>
                                        </Tab>
                                        <Tab>
                                            <div className="flex flex-col items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                                Complete
                                            </div>
                                        </Tab>
                                        <Tab>
                                            <div className="flex flex-col items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                                Cancel
                                            </div>
                                        </Tab>
                                    </TabList>
                                </div>
                                <TabPanel>
                                    <OrderTable orderList={pendingOrders} handleOrderListChange={handleOrderListChange} />
                                </TabPanel>
                                <TabPanel>
                                    <OrderTable orderList={packedOrders} handleOrderListChange={handleOrderListChange} />
                                </TabPanel>
                                <TabPanel>
                                    <OrderTable orderList={shippingOrders} handleOrderListChange={handleOrderListChange} />
                                </TabPanel>
                                <TabPanel>
                                    <OrderTable orderList={completeOrders} handleOrderListChange={handleOrderListChange} />
                                </TabPanel>
                                <TabPanel>
                                    <OrderTable orderList={cancelOrders} handleOrderListChange={handleOrderListChange} />
                                </TabPanel>
                            </div>
                        </Tabs>
                    </div >
            }
        </>


    );
}
