import * as React from 'react';

import { useHistory, useRouteMatch } from 'react-router';
import axiosClient from '../../../api/axios-client';
import { Modal } from './Modal';
export interface OrderTableProps {
    orderList?: any,
    handleOrderListChange:Function
}

export function OrderTable({ orderList, handleOrderListChange }: OrderTableProps) {
    const history = useHistory();
    const match = useRouteMatch();
    const [showModal, setShowModal] = React.useState(false);
    const [reason, setReason] = React.useState("");
    const [selectedOrder, setSelectedOrder] = React.useState<number>();

    const renderCancelButton = (status: string, orderId:number) => {
        switch (status) {
            case "Cancelled":
                return;
            case "Completed":
                return;
            case "Shipping":
                return;
            default:
                return (
                    <button onClick={() => handleModalClick(orderId)} className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded">
                        Cancel
                    </button>
                )
        }
    }

    const handleModalClick = (orderId:number) => {
        setShowModal(true);
        setSelectedOrder(orderId);
    }

    const handleCancelClick = async () => {
        let status = {
            status: "Cancelled",
            reason: reason
        }
        try {
            let result:any = await axiosClient.put(`/orders/${selectedOrder}`, status);
            if(result.data.status==="Cancelled"){
                let orderStatus = orderList[0].status;
                let orderItem = orderList.find((order:any) => order.id === selectedOrder);
                orderList = orderList.filter((order:any) => order.id !== selectedOrder);
                handleOrderListChange(orderList,orderStatus,orderItem);
                setReason("");
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handleDetailClick = (order: any) => {
        history.push({
            pathname: `${match.url}/${order.id}`,
            state: { order: order }
        });
    }
    return (
        <>
            {
                orderList.length === 0 ?
                    <div className="flex container mx-auto justify-center items-center px-4 md:px-10 py-20">
                        <div className="bg-white px-3 md:px-4 py-12 flex flex-col justify-center items-center">
                            <h1 className="mt-8 md:mt-12 text-3xl lg:text-4xl font-semibold leading-10 text-gray-800 text-center md:w-9/12 lg:w-7/12">There are no orders here.</h1>
                        </div>
                    </div>
                    :
                    <div className="mt-7 overflow-x-auto xl:overflow-hidden">
                        <Modal setShowModal={setShowModal} reason={reason} setReason={setReason} showModal={showModal} handleCancelClick={handleCancelClick} />
                        <table className="w-full whitespace-nowrap mx-auto">
                            <thead >
                                <tr>
                                    <th>ID</th>
                                    <th className='text-right'>Customer Name</th>
                                    <th>Address</th>
                                    <th className='text-right'>Order Date</th>
                                    <th className='text-right'>Payment</th>
                                    <th>Total</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {orderList.map((order: any) => {
                                    return (
                                        <tr className="focus:outline-none h-16 border border-gray-100 rounded" key={order.id}>
                                            <td>
                                                <div className="flex justify-center pl-5">
                                                    <p className="text-base font-medium leading-none text-gray-700 mr-2 cursor-pointer">{order.id}</p>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                                        <path d="M6.66669 9.33342C6.88394 9.55515 7.14325 9.73131 7.42944 9.85156C7.71562 9.97182 8.02293 10.0338 8.33335 10.0338C8.64378 10.0338 8.95108 9.97182 9.23727 9.85156C9.52345 9.73131 9.78277 9.55515 10 9.33342L12.6667 6.66676C13.1087 6.22473 13.357 5.62521 13.357 5.00009C13.357 4.37497 13.1087 3.77545 12.6667 3.33342C12.2247 2.89139 11.6251 2.64307 11 2.64307C10.3749 2.64307 9.77538 2.89139 9.33335 3.33342L9.00002 3.66676" stroke="#3B82F6" strokeLinecap="round" strokeLinejoin="round"></path>
                                                        <path d="M9.33336 6.66665C9.11611 6.44492 8.8568 6.26876 8.57061 6.14851C8.28442 6.02825 7.97712 5.96631 7.66669 5.96631C7.35627 5.96631 7.04897 6.02825 6.76278 6.14851C6.47659 6.26876 6.21728 6.44492 6.00003 6.66665L3.33336 9.33332C2.89133 9.77534 2.64301 10.3749 2.64301 11C2.64301 11.6251 2.89133 12.2246 3.33336 12.6666C3.77539 13.1087 4.37491 13.357 5.00003 13.357C5.62515 13.357 6.22467 13.1087 6.66669 12.6666L7.00003 12.3333" stroke="#3B82F6" strokeLinecap="round" strokeLinejoin="round"></path>
                                                    </svg>
                                                </div>
                                            </td>
                                            <td className="pl-5">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{order.customer}</p>
                                                </div>
                                            </td>
                                            <td className="pl-5">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{order.address}</p>
                                                </div>
                                            </td>
                                            <td className="pl-5">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{order.createdAt.match(/(\d{2,4}\-\d{1,2}\-\d{1,2})/)[1].replace(/(\d{4})\-(\d{2})\-(\d{2})/, '$3/$2/$1')}</p>
                                                </div>
                                            </td>
                                            <td className="pl-5">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{order.paymentMethod}</p>
                                                </div>
                                            </td>
                                            <td className="pl-5">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{Number.parseFloat(order.total).toLocaleString('it-IT', { style: 'currency', currency: 'VND' }).replace("VND", "đ")}</p>
                                                </div>

                                            </td>
                                            <td className="pl-4">
                                                <div className="flex justify-center">
                                                    <p className="text-sm leading-none text-gray-600 ml-2">{order.note}</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="px-5 pt-2 flex justify-center">
                                                    <button onClick={() => handleDetailClick(order)} className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-1 px-3 rounded mr-3">
                                                        Detail
                                                    </button>
                                                    {renderCancelButton(order.status, order.id)}
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                                <tr className="h-3"></tr>
                            </tbody>
                        </table>
                    </div>}
        </>
    );
}
