import * as React from 'react';
import { Hero } from './components/Hero';
import { useHistory, useRouteMatch } from 'react-router';
import { toast } from 'react-toastify';

export interface TrackOrderProps {
}

export function TrackOrder(props: TrackOrderProps) {
    const match = useRouteMatch();
    const history = useHistory();
    const [email, setEmail] = React.useState('');
    const [valid, setValid] = React.useState(false);
    React.useEffect(() => {
        if (localStorage.getItem("access_token")) {
            history.push(`${match.url}/list`);
        }
    }, [])
    const handleTrackingOrder = () => {
        if (valid) {
            localStorage.setItem("user_info", email);
            history.push(`${match.url}/list`);
            return;
        }
        toast.warn("Email is invalid");

    }
    const handleEmailChange = (e: any) => {
        if (e.target.value.match(/^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/gm)) {
            setEmail(e.target.value);
            setValid(true);
        }

    }
    return (
        <>
            <div className="flex-col flex md:flex-row justify-around my-28">
                <div>
                    <h1 className="font-semibold text-5xl text-my-color">Track Your Orders Easily</h1>
                    <blockquote className="text-2xl py-5 text-gray-600 font-medium">Just enter your email & it’s done.</blockquote>
                    <img className="w-full" src="https://image.freepik.com/free-vector/delivery-service-with-masks-concept_23-2148535315.jpg" />
                </div>
                <div className="w-full md:w-2/6 flex items-center">
                    <div className="rounded-3xl shadow-md p-10">
                        <h2 className="font-bold text-xl text-my-color my-5">Enter Details</h2>
                        <input defaultValue={email} onBlur={handleEmailChange} className={`${valid ? null : 'border border-red-500'} border bg-gray-50 border-gray-100 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline`} type="text" placeholder="Enter your email" />
                        <button onClick={handleTrackingOrder} className="w-full bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">
                            Track Now
                        </button>
                    </div>
                </div>
            </div>
            <Hero />
        </>
    );
}
