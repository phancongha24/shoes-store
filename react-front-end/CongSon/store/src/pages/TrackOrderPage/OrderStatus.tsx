import * as React from 'react';
import { useHistory, useLocation } from 'react-router';

export interface OrderDetailProps {
}

let data = [
    {
        title: 'Chờ xác nhận',
        bgColor: 'bg-yellow-500',
        description: 'Đơn hàng của bạn đang chờ được xác nhận'
    },
    {
        title: 'Đã hủy đơn hàng',
        bgColor: 'bg-red-500',
        description: 'Thích thì bơm thui'
    }


]

export function OrderStatus(props: OrderDetailProps) {
    const history = useHistory();
    const location = useLocation();
    const { status, id }: any = location.state;

    const handleBackClick = () => {
        history.goBack();
    }

    const renderStatus = (item: any) => {
        let bgColor = 'bg-green-500';
        switch(item.status){
            case "Cancelled":
            bgColor = 'bg-red-500';
            break;
        }
        return (
            <>
                <div className="col-start-2 col-end-4 mr-10 md:mx-auto relative">
                    <div className="h-full w-6 flex items-center justify-center">
                        <div className={`h-full w-1 pointer-events-none ${bgColor}`}></div>
                    </div>
                    <div className={`w-6 h-6 absolute top-1/2 -mt-3 rounded-full shadow text-center ${bgColor}`}>
                        <i className="fas fa-exclamation-circle text-gray-50"></i>
                    </div>
                </div>
                <div className={`${bgColor} col-start-4 col-end-12 p-4 rounded-xl my-4 mr-auto shadow-md w-full`}>
                    <h3 className="font-semibold text-lg mb-1 text-gray-50">{item.status} <span className='ml-5 text-white'>{item.createdAt.match(/(\d{2,4}\-\d{1,2}\-\d{1,2}T(\d{2})\:(\d{2}))/)[1].replace(/(\d{4})\-(\d{2})\-(\d{2})T/, '$3/$2/$1 ')}</span></h3>
                    <p className="leading-tight text-justify">
                        {item.reason}
                    </p>
                </div>
            </>
        )
    }

    return (

        <div className="p-4">
            <div>
                <button onClick={handleBackClick} className="bg-gray-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">
                    Go Back
                </button>
            </div>

            <div className="mt-4">

                <h1 className="text-4xl text-center font-semibold mb-6 text-gray-500">#{id} Order Status Detail</h1>
                <div className="container">
                    <div className="flex flex-col md:grid grid-cols-12 text-gray-50">
                        {status.map((item: any) => {
                            return (
                                <div className="flex md:contents">
                                    {renderStatus(item)}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
}
