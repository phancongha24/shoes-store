import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Route, Switch, useLocation, Redirect } from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';
import 'react-tabs/style/react-tabs.css';
import './App.css';
import { NotFound } from './components/Common';
import { CartPage } from './pages/CartPage';
import { CheckoutPage } from './pages/CheckoutPage';
import { HomePage } from './pages/HomePage';
import { ProductPage } from './pages/ProductPage';
import { TrackOrderPage } from './pages/TrackOrderPage';
import { ToastContainer } from 'react-toastify';
import { SignInPage } from './pages/AuthPage';
import { SuccessPage } from './pages/SuccessPage';
import { VoucherPage } from './pages/VoucherPage';
import { FaqPage } from './pages/FaqPage';
import { WishlistPage } from './pages/WishlistPage';


function useQuery() {
  return new URLSearchParams(useLocation().search);
}
function App() {
  let query = useQuery();
  const userToken = localStorage.getItem("access_token");
  return (
    <div>


      <Switch>
        <Route path="/" exact component={HomePage} />

        <Route path="/products" component={ProductPage} />
        <Route path="/vouchers" component={VoucherPage} />
        <Route path="/faq" component={FaqPage} />
        <Route path="/wishlist" component={WishlistPage} />
        <Route path="/checkout" >
          {userToken ? <CheckoutPage /> : <Redirect to="/login" />}
        </Route>

        <Route path="/login" component={SignInPage} />

        <Route path="/cart" component={CartPage} />

        <Route path="/orders" component={TrackOrderPage} />
        <Route path="/success" component={SuccessPage} />
        <Route component={NotFound} />

      </Switch>
      <ToastContainer newestOnTop={true}
        position="bottom-right"
        autoClose={3000}
        hideProgressBar={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover />
    </div>
  );
}

export default App;
